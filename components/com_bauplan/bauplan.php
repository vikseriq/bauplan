<?php
require_once (JPATH_BASE.'/administrator/components/com_bauplan/core.php');	

$section = 'category';

/* Hierarchy:
	component
		section
			controller
			model
			view_1
			view_2
	etc.
*/
require_once (JPATH_COMPONENT.DS.$section.DS.'controller.php');	
require_once (JPATH_COMPONENT.DS.$section.DS.'model.php');

$controller = $section.'Controller'; 
$controller = new $controller( array('default_task' => 'show') );

$task = JRequest::getVar('task');
if (!$task)
	$task = JRequest::getVar('view');
//$_REQUEST['task'] = $task;

$controller->execute($task);
