<?php
	// BauPlan - вспомогательные функции
	//
define('BP_ICON_PATH', JURI::base(true).'/templates/'.JFactory::getApplication()->getTemplate().'/images/');

// вывод select в код
function select_r($label = '', $name = '', $array = array(), $selected = 0, $default = "Любой"){
	$array[0] = $default;    // даже если массив будет пуст, у нас не будет проблем
	$html = '<label>'.$label.'</label><select class="ss" id='.$name.' name='.$name.'>';
	foreach ($array as $i => $j){
		if ($i != $selected)
			$html .= '<option value="'.$i.'">'.$j.'</option>';
		else
			$html .= '<option value="'.$i.'" selected="selected">'.$j.'</option>';
	}
	$html .= '</select>';

	return $html;
}

// чекбоксы — выбор нескольких значений
function checkbox_multi($label = '', $name = '', $array = array(), $selected = 0){
	if (!is_array($selected))
		$selected = [$selected];
	$html = '<li><div class="b-chkhead">'.$label.'</div></li>';
	foreach ($array as $i => $j){
		if ($i === 0) continue;
		$html .= '<li><input name="'.$name.'[]" value="'.$i.'" type="checkbox" id="'.$name.'['.$i.']" '
			.(in_array($i, $selected, true) ? 'checked' : '').' ><label for="'.$name.'['.$i.']">'.$j.'</label></li>';
	}

	return $html;
}

function object_card($i){
	$html = '<li class="b-item">';

	$html .= sprintf('<div class="b-pic"><a href="%s"><img src="%s" alt="%s"></a></div>',
		$i->url, BauCore::img($i->image_main, 135, 85), BauCore::alt($i->title));
	$html .= sprintf('<div class="b-pname"><a href="%s">%s</a></div>',
		$i->url, $i->title);
	if ($i->area)
		$html .= '<div class="b-char">Общ.площадь: %s</div>'.BauCore::f($i->area, 'areas');

	$html .= "</li>\n";
	return $html;
}

function object_param_html_li($value, $icon = null){
	return '<li class="b-item"><div class="b-tb">'
	.($icon ? '<div class="b-ihsp-ico"><img src="'.BP_ICON_PATH.$icon.'.png" alt=""></div>' : '&nbsp;')
	.'<div class="b-ihsp">'.$value.'</div></div></li>';
}

function object_param_html($item){    // возвращает оформленный список параметров
	$p = $item->params;
	$x = '';

	if ($p['floors'])
		$x .= object_param_html_li('Этажность: <i>'.($p['floors']).'</i>', 'ip2');
	$x .= object_param_html_li('Терраса <i>'.($p['floor_out'] ? 'есть' : 'нет').'</i>', 'ip3');
	$x .= object_param_html_li('Сауна <i>'.($p['swim'] ? 'есть' : 'нет').'</i>', 'ip4');
	$x .= object_param_html_li('Балконы: <i>'.($p['floor_b'] ? $p['floor_b'] : 'нет').'</i>', 'ip5');
	$x .= object_param_html_li('Гараж <i>'.($p['car'] ? 'есть' : 'нет').'</i>');
	$x .= object_param_html_li('Цокольный этаж <i>'.($p['floor_0'] ? 'есть' : 'нет').'</i>');

	return $x;
}

function list_param_html_li($icon, $val, $hidden){
	return '<li class="b-item '.($hidden ? 'b-item-hidden' : '').'"><div class="b-tb">'
		.'<div class="b-ihsp-ico"><img src="'.BP_ICON_PATH.$icon.'.png" alt=""></div><div class="b-ihsp">'.$val.'</div>'
		.'</div></li>';
}

function list_param_html( $params ){	// возвращает оформленный список параметров
	$p = BauCore::str2params($params);	// получаем массив параметров
	$vn = 2;
	$i = 0;
	$x = '';
	if ($p['floors'])	$x.= list_param_html_li('ip2', 'Этажность: '.$p['floors'], (++$i > $vn));
	if ($p['floor_out'])$x.= list_param_html_li('ip3', 'Терраса', (++$i > $vn));
	if ($p['swim'])		$x.= list_param_html_li('ip4', 'Сауна', (++$i > $vn));
	if ($p['floor_b'])	$x.= list_param_html_li('ip5', 'Балконы: '.$p['floor_b'], (++$i > $vn));
	if ($p['car'])		$x.= list_param_html_li('ip1', 'Гараж', (++$i > $vn));
	if ($p['floor_0'])	$x.= list_param_html_li('ip1', 'Цокольный этаж', (++$i > $vn));

	if ($i > $vn){
		$x .= '<li class="b-item"><div class="b-more"><a href="">еще</a></div></li>';
	}
	return $x;
}

function is_plan(&$name){
	return (!(strpos($name, 'plan_') === false));
}

function get_image_list($path){
	$files = bImages::images_in_dir($path);
	$images = array('plan' => array(), 'image' => array());
	if ($files) foreach ($files as $i){
		$x = JUri::base().substr($path, 1).$i;
		if (is_plan($i)) $images['plan'][] = $x;
		else            $images['image'][] = $x;
	}
	return $images;
}