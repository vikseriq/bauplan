<?php
$input = JFactory::getApplication()->input;
$collapsed = !defined('BP_SEARCH');
?>
<?php if ($this->cparam->get('enable_search', 1)): ?>
	<div class="b-pfilter" style="display:<?=($collapsed ? 'none' : 'block')?>">
		<form method="get" action="<?php echo JRoute::_('index.php?option=com_bauplan&view=search') ?>"
			  data-action="<?php echo JRoute::_('index.php?option=com_bauplan&view=search_ajax') ?>"
			  id="SearchForm" name="sf">
			<?php // инициализация
			$area_min = $price_min = 0;
			$type = $material = $floor = $extra = 0;
			$area_max = 750;
			$price_max = 10000 * 1000;
			$title = '';
			if (defined('BP_SEARCH')){    // если это страница поиска
				$area_a = $_GET['area_a'];
				$area_b = $_GET['area_b'];
				$price_a = $_GET['price_a'];
				$price_b = $_GET['price_b'];
				$type = $_GET['type'];
				$material = $_GET['material'];
				$title = $_GET['title'];
				$floor = $_GET['floor'];
				$extra = $_GET['extra'];
			} else {
				// значения по умолчанию
				$area_a = 30;
				$area_b = 630;
				$price_a = 0;
				$price_b = 6500 * 1000;

				// поддержка градации площади из подкатегории
				if (!empty($_GET['from']))
					$area_a = intval($_GET['from']);
				if (!empty($_GET['till']))
					$area_b = intval($_GET['till']);
			}
			$extras = BauCore::getObjectSearchableParams();
			?>
			<div class="b-pft-inner">

				<div class="b-prf-left">

                    <div class="b-param-line">
                        <ul class="b-param-tb">
                            <li class="b-item"><span class="b-param-head">Площадь от</span></li>
                            <li class="b-item"><input class="b-inp-f" type="text" id="lower_sq" name="area_a" data-a value="<?=$area_a?>"></li>
                            <li class="b-item b-td-slider">
                                <div class="b-line-slider b-line-slider c-slider-sq"
                                     data-min="<?=$area_min?>" data-max="<?=$area_max?>" data-step="10"></div>
                            </li>
                            <li class="b-item">до</li>
                            <li class="b-item"><input class="b-inp-f" type="text" id="upper_sq" name="area_b" data-b value="<?=$area_b?>"></li>
                            <li class="b-item">пл.м<sup>2</sup></li>
                        </ul>
                    </div><!-- /.b-param-line -->

					<?php if ($this->cparam->get('search_by_price', 1)): ?>
                    <div class="b-param-line">
                        <ul class="b-param-tb">
                            <li class="b-item"><span class="b-param-head">Цена от</span></li>
                            <li class="b-item"><input class="b-inp-f" type="text" id="lower_sq3" name="price_a" data-a value="<?=$price_a?>"></li>
                            <li class="b-item b-td-slider3">
                                <div class="b-line-slider b-line-slider c-slider-capacity"
                                     data-min="<?=$price_min?>" data-max="<?=$price_max?>" data-step="1000"></div>
                            </li>
                            <li class="b-item">до</li>
                            <li class="b-item"><input class="b-inp-f" type="text" id="upper_sq3" name="price_b" data-b value="<?=$price_b?>"></li>
                            <li class="b-item">руб</li>
                        </ul>
                    </div><!-- /.b-param-line -->
					<? endif ?>

                    <ul class="b-pft-check-group">
	                    <?php echo checkbox_multi('По материалу', 'material', $materials, $material); ?>
                    </ul>

                    <ul class="b-pft-check-group">
	                    <?php echo checkbox_multi('По типу', 'type', $types, $type); ?>
                    </ul>
				</div>
				<!-- /.b-prf-left -->

				<div class="b-prf-right">
					<input type="submit" value="найти" class="b-btn-send">
				</div>
			</div>
		</form>
	</div><!-- /.b-pfilter -->
<?php endif; ?>

<div class="b-filt-sqr">
<? if ($categories && $cid) foreach($categories as $cat):
	if ($cat->category_id != $cid)
		continue;
	// выводим деление категории по площадям
	if ($cat->selector && ($a = explode(';', $cat->selector)) && count($a) && strlen(trim($a[0]))):
?>

	<ul class="b-filt-sqr-list">
		<? foreach ($a as $sq){
			$_val = explode('=', trim($sq));
			if (count($_val) > 1 && strlen(trim($_val[0]))){
				$_sub = explode('-', $_val[1]);
				printf('<li class="b-item"><a href="%s">%s</a></li>',
					JRoute::_($cat->url.'?from='.$_sub[0].'&till='.$_sub[1]),
					$_val[0]);
			}
		}?>
		<li class="b-item-all"><a href="<?=$cat->url?>">все проекты</a></li>
	</ul>

	<?	endif;
endforeach; ?>
    <div class="b-filter-toggler active">Поиск по параметрам</div>
</div>

