<div class="b-project-tnav">
    <ul class="b-project-tnav-list">
	    <?php foreach($cats as $cat): ?>
            <li class="<?php if ($cat->category_id == $current) echo 'active'; ?>"><a href="<?=$cat->url?>"><?=$cat->title?></a></li>
	    <?php endforeach; ?>
    </ul>
</div>