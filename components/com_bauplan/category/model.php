<?php

require_once (JPATH_BASE.'/administrator/components/com_bauplan/core.php');
jimport('joomla.application.component.model');

class categoryModelcategory extends JModelLegacy {

	var $db 			= null;
	var $data			= null;
	var $table_xref		= null;
	var $table_plan		= null;
	var $table_category	= null;
	var $table_price	= null;
	var $_total 		= null;
	var $_pagination	= null;
	var $limit			= null;
	var $limitstart		= null;

	function __construct () {
		global $mainframe, $context;

		parent::__construct();

		$this->db = $this->_db;

		$this->table_category = "#__bp_category";
		$this->table_plan	  = "#__bp_plan";
		$this->table_xref 	  = "#__bp_plan_category_xref";
		$this->table_price 	  = "#__bp_price";

		$this->limit		= 1000;//$mainframe->getUserStateFromRequest( $context.'limit', 'limit', $mainframe->getCfg('list_limit'), 0 );
		$this->limitstart	= 0;//$mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0 );
	}

	function getList( $only_total = false ) {
		$q = "SELECT *, (SELECT COUNT(*) FROM ".$this->table_xref." b WHERE b.category_id = a.category_id) as contain
		FROM ".$this->table_category." a WHERE 1 ORDER BY parent_id, category_id";

		if (!$only_total) {
			if (empty($this->data))
				$this->data = $this->_getList($q, $this->limitstart, $this->limit);
			return $this->data;
		} else {
			if (empty($this->total))
				$this->total = $this->_getListCount($q) + 100;
			return $this->total;
		}
	}

	function hit( $o, $id ) {
		// проверка на вшивость ,)
		$val = null;
		gv($val, 'blade', '', 'cookie', 'string');
		$ext = '_'.$id.',';
		if (strpos($val, $ext)!==false){
			return;		// уже есть печенька, больше не даём
		} else {
			setcookie('blade', $val.$ext, time()+3600*5);
		}

		$q = '';
		if ($o == 'category'){
			$q = 'UPDATE '.$this->table_category.' SET hits=(hits+1) WHERE category_id = '.(int)$id;
		} elseif ($o == 'object') {
			$q = 'UPDATE '.$this->table_plan.' SET hits=(hits+1) WHERE plan_id = '.(int)$id;
		}
		if ($q){
			$this->db->setQuery($q);
			$this->db->query();
		}
	}

	function setObjectCategoryTab( $plan_id = 0, $category_id = 0 ){
		if (!($plan_id && $category_id)) return;
		$q = "UPDATE ".$this->table_plan." SET category_tab = ".(int)$category_id.", mdate = '".date("Y-m-d H:i:s", time()+11)."' WHERE plan_id = ".(int)$plan_id;
		$this->db->setQuery($q);
		$this->db->query();
	}

	function getPagination($prefix = '') {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->_total, $this->limitstart, $this->limit, $prefix);
		}
		return $this->_pagination;
	}

	function getCategory( $cid ) {
		$q = "SELECT * FROM ".$this->table_category." WHERE category_id = ".$cid." AND published = 1 LIMIT 1;";
		$this->db->setQuery($q);
		$a = $this->db->loadObject();
		return ($a) ? $a : null;
	}

	function getCategories( $q = '', $url = 'index.php?option=com_bauplan&view=category&cid=' ) {
		if (!$q)
			$q = 'SELECT * FROM '.$this->table_category.' WHERE published = 1 AND parent_id = 0 ORDER BY parent_id, ordering';
		$this->db->setQuery($q);
		$this->data = $this->db->loadObjectList();
		if ($this->data && strlen($url)) {
			for ($i = count($this->data); $i--;) {
				// build link
				$this->data[$i]->url = JRoute::_($url . $this->data[$i]->category_id);
				// resolve images
				if ($this->data[$i]->image && $this->data[$i]->image == $this->data[$i]->image + 0) {
					$q = 'SELECT image_main FROM ' . $this->table_plan . ' WHERE plan_id = ' . $this->data[$i]->image;
					$this->db->setQuery($q);
					$this->data[$i]->image = $this->db->loadResult();
				}
			}
		}
		return $this->data;
	}

	function getObject( $pid ) {
		$q = "SELECT * FROM ".$this->table_plan." WHERE plan_id = ".$pid." AND published = 1 LIMIT 1;";
		$this->db->setQuery($q);
		$a = $this->db->loadObject();
		if (!$a) return null;
		$a->p = BauCore::str2params($a->params);
		$a->price_tables = BauCore::str2array($a->prices, 3);
		return $a;
	}

	function getPriceTables( $ids ){
		$this->db->setQuery('SELECT * FROM '.$this->table_price.' WHERE ext = 0 AND id IN ('.$ids.')');
		$t = $this->db->loadAssocList();
		for($i = count($t); $i--; ){
			$this->db->setQuery('SELECT * FROM '.$this->table_price.' WHERE ext = '.$t[$i]['id'].' ORDER BY value ASC ');
			$t[$i]['items'] = $this->db->loadAssocList();
		}
		return $t;
	}

	function getCategoryFilter( &$c /*, $page = 0*/ ) {
		$q = 'SELECT * FROM '.$this->table_plan.' WHERE ';
		$q.= ' published = 1 ';
		// поиск по названию приоритетен
		if (strpos($c->use, 'title')!==false){
			$q.= " AND ( title LIKE '%".$c->title."%' ) ";
		} else {
			if (strpos($c->use, 'material')!==false)
				$q.= ' AND material IN ( '.(is_array($c->material) ? implode(', ', $c->material) : $c->material).' ) ';
			if (strpos($c->use, 'type')!==false)
				$q.= ' AND type IN ( '.(is_array($c->type) ? implode(', ', $c->type) : $c->type).' ) ';
			if (strpos($c->use, 'price')!==false)
				$q.= ' AND ( price_min >= '.$c->price_from.' AND price_min <= '.$c->price_till.' ) ';
			if (strpos($c->use, 'area')!==false)
				$q.= ' AND ( area >= '.$c->area_from.' AND area <= '.$c->area_till.' ) ';
			if (strpos($c->use, 'floor')!==false)
				$q.= " AND params LIKE '%floors=".$c->floor."%'";
			if (strpos($c->use, 'extra')!==false){
				// разбираем детальные параметры
				$allowed = BauCore::getObjectSearchableParams();
				foreach($c->extra as $extra){
					if (isset($allowed[$extra]))
						$q.= " AND params LIKE '%".$extra."=1%'";
				}
			}
		}

		$order = $this->getSortColumn($c->sort_column);
		if (strpos($order, 'RAND') === false)
			$order.= ($c->sort_order != 0)? ' ASC ' : ' DESC ';

		$q.= 'ORDER BY '.$order.' ';
		/*if ($c->limit && $page > 0) {
			$limitstart = $c->limit * ($page-1);
			$q.= 'LIMIT '.$limitstart.', '.($c->limit).' ;';
		}*/

		return $q;
	}

	function getObjectsByFilter( $q, $url = '', $pagination = false) {
		if ($pagination) {
			$this->_total	= $this->_getListCount($q);
			$this->data		= $this->_getList($q, $this->limitstart, $this->limit);
		} else {
			$this->db->setQuery($q);
			$this->data = $this->db->loadObjectList();
		}
		if ($this->data && strlen($url)) {
			for (	$i = count($this->data);
					$i--;
					$this->data[$i]->url = JRoute::_($url.$this->data[$i]->plan_id)
				);
		}
		return $this->data;
	}

	private function getSortColumn( $i ){
		switch ($i) {
			case 1: $x = 'area';		break;
			case 2: $x = 'title';		break;
			case 3: $x = 'price_min';	break;
			case 4: $x = 'cdate';		break;
			case 5: $x = 'mdate';		break;
			case 6: $x = 'rate';		break;
			case 7: $x = 'hits';		break;
			case 8: $x = ' RAND() ';	break;
			case 0:
			default: $x = 'plan_id';	break;
		}
		return $x;
	}

	// выборка построек
	function getBuildings(){
		$q = "SELECT a.*, b.title as plan_title,
			b.image_main, b.material, b.type, b.area, b.area_live, b.size_w, b.size_l, b.params as obj_params
			FROM  #__bp_build a
			LEFT JOIN #__bp_plan b ON a.plan_id = b.plan_id
			WHERE a.published = 1;";
		$this->db->setQuery($q);
		$a = $this->db->loadAssocList();
		$data = array();

		if ($a) foreach($a as $x){
			/*if ($x['articles']){
				$this->db->setQuery("SELECT id, title, catid FROM #__content WHERE id IN (".$x['articles'].") AND state = 1");
				$r = $this->db->loadAssocList();
				$x['articles'] = array();
				if ($r) foreach($r as $j){
					$x['articles'][] = $j;
				}
			}*/
			$x['p'] = BauCore::str2params($x['params'], BauCore::getBuildParamKeys());
			$data[] = $x;
		}
		return $data;
	}

	// выборка постройки, связанной с проектом
	function getBuild($build_id, $plan_id){
		$a = null;
		$this->db->setQuery('SELECT * FROM #__bp_build WHERE build_id = '.intval($build_id).' AND plan_id = '.intval($plan_id));
		if ($a = $this->db->loadObject()){
			if ($a->articles){
				$this->db->setQuery("SELECT * FROM #__content WHERE id IN (".$a->articles.") AND state = 1");
				$rows = $this->db->loadAssocList();
				$a->articles = array();
				if ($rows) foreach($rows as $row){
					$a->articles[] = $row;
				}
			}
		}
		return $a;
	}

	// выбор элементов внешней фотогалереи
	function getGallery($gallery_id){
		$path = '/media/com_gallery_wd/uploads/';
		$gallery = array();
		$this->db->setQuery("SELECT * FROM #__bwg_image WHERE gallery_id = ".$gallery_id." AND published = 1 ORDER BY `order` asc");
		$rows = $this->db->loadAssocList();
		if (count($rows)){
			foreach ($rows as $row){
				$gallery[] = array(
					'image' => $path.$row['image_url'],
					'thumb' => $path.$row['thumb_url'],
					'title' => $row['alt']
				);
			}
		}
		return $gallery;
	}

}

?>