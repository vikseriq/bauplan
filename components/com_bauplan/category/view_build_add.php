<?php // bauplan .11 // vik

	$d =& JFactory::getDocument();
	$d->addScript("http://api-maps.yandex.ru/2.0-stable/?load=package.standard,package.geoObjects&lang=ru-RU");

function input_text ( $key, $value, $etc = ' maxlength="300"') {
	return sprintf('<input class="text_in" type="text" name="%s" id="%s" value="%s" %s />', $key, $key, $value, $etc);
}
function check_param($array, $key) {
	return sprintf('<input type="checkbox" class="checkbox" name="params[%s]" id="%s" value="1" %s />',
			$key, $key, (isset($array[$key])&&$array[$key])?'checked':'');
}
	// Вывод строки таблицы. Заголовок, значения, описание
function tt( $title, $field, $desc = '' ) {
	$title = JText::_($title);
	if (strlen($desc))
		$title = '<abbr title="'.$desc.'">'.$title.'</abbr>';

	echo '
	<tr>
		<td class="key"><label>'.$title.'</label></td>
		<td>'.$field.'</td>
	</tr>';
}

function selector ( $name, $selected = 0, $values = array() ) {
	$html = '<select name="'.$name.'">';
	foreach($values as $i => $v) {
		if ($i == $selected)
			$html.= '<option value="'.$i.'" selected="yes">'.$v.'</option>';
		else
			$html.= '<option value="'.$i.'">'.$v.'</option>';
	}
	$html.= '</select>';
	return $html;
}

	$uri = JFactory::getURI()->_uri;
	$editor =& JFactory::getEditor();
	JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES, 'text' );

	define( 'SITE_URL', str_replace('/administrator/', '', JUri::base()));

?>
<style type="text/css">
table.admintable {
	font-family: serif;
	font-size: 14px;
}
table.admintable td.key {
	font-size: 12px;
}
.text_in {
	font-size: 16px;
}
input#title {
	width: 350px;
}
.params .text_in {
	width: 120px;
}
select {
	font-size: 12px;
}
</style>

<form action="<?php echo $uri ?>" method="post" enctype="multipart/form-data" name="adminForm">
	<fieldset class="adminform">
	<legend>Параметры постройки</legend>
	<table class="admintable" style="width:100%">
		<tr><td style="width:45%" valign="top">
			<fieldset class="adminform"><legend>Основное</legend><table class="admintable">
				<?php

				tt('Номер постройки', 	$row->build_id,				"Порядковый номер в базе данных");
				tt('Title', 			input_text('title', $row->title));
				tt('Published', 		JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $row->published));
				tt('Объект',			input_text('plan_id', $row->plan_id, 'style="width: 200px;"'), "");
				tt('Связанные статьи',	input_text('articles', $row->articles, 'style="width: 200px;"'), "ID материалов через запятую");
				tt('Доступно всем', 	JHTML::_('select.booleanlist',  'public', 'class="inputbox"', $row->public), "Управление доступом: полная информация всем или только менеджерам");
				?>
			</table></fieldset>
			<fieldset class="adminform"><legend>Координаты</legend><table class="admintable">
				<?php
				tt('GPS-координаты', 	input_text('coord', $row->coord), "Координаты на карте: long,lat");
				tt('Карта', '<div id="map" style="width: 350px; height: 350px"></div>');
				?>
			</table></fieldset>
		</td><td>
			<fieldset class="adminform"><legend>Описание</legend><table class="admintable" style="width:100%">
			<?php
				tt('Описание', $editor->display(
							'text', htmlspecialchars($row->text, ENT_QUOTES),
							'400', '400', '80', '40', array('pagebreak', 'readmore'))
				) ;
			?>
			</table></fieldset>
		</td></tr>
	</table></fieldset>
	<div class="clr"></div>

	<input type="hidden" name="option" value="com_bauplan" />
	<input type="hidden" name="id" value="<?php echo $row->build_id; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
<script>
var map;
var point;

ymaps.ready(function(){
	map = new ymaps.Map("map", { center: [<?=$row->coord?>], zoom: 11 });
	map.controls.add('typeSelector');
	map.controls.add('smallZoomControl');
	map.events.add('click', function (e) {
		var coords = e.get('coordPosition');
		point.geometry.setCoordinates(coords);
		document.getElementById('coord').value = [coords[0].toPrecision(6),coords[1].toPrecision(6)].join(',');
	});

	point = new ymaps.Placemark([<?=$row->coord?>]);
	map.geoObjects.add(point);

});
</script>