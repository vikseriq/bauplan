<?
	// отображение объектов на карте
	// com_bauplan
	// vikseriq @ 12.08.2013 - 20.08.2013
	$d =& JFactory::getDocument();
	$d->addScript("http://api-maps.yandex.ru/2.0-stable/?load=package.standard,package.geoObjects&lang=ru-RU");

	$twirl_icons = array('', 'twirl#campingIcon', 'twirl#storehouseIcon', 'twirl#keyMasterIcon', 'twirl#factoryIcon');

	$me = JFactory::getUser(); $guest = $me->guest;
?>
<style>
.ymo_left {
    float: left;
    margin: 0 10px 10px;
}
.ymo_right {
    float: right;
    width: 300px;
}
.ymo_clear { clear: both; }
</style>
<div class="category_block">
	<div class="category_content">
		<div id="map" style="width: 890px; height: 500px"></div>
	</div>
</div>
<script type="text/javascript">
function filterObjectMaterial(material){
	map.geoObjects.each(function(e){
		if (e.properties.get('typeid') == 'build'){
			if (material >= 0){
				// only specified
				if (e.properties.get('material') == material){	e.options.set('visible', true);	}
				else { e.options.set('visible', false); }
			} else {
				// show all
				e.options.set('visible', true);
			}
		}
	});
}

var map;
ymaps.ready(function(){
	map = new ymaps.Map ("map", {
		center: [55.770479,37.555873], zoom: 9
	}, {
		maxZoom: <?=$this->cparam->get('map_zoom_max', 15)?>, minZoom: <?=$this->cparam->get('map_zoom_min', 6)?>
	});
	map.controls.add('typeSelector');
	map.controls.add('smallZoomControl');

	var typeList = new ymaps.control.ListBox({
        data: { title: 'Материал' },
        items: [
		<? $mats = BauCore::getObjectMaterials(-1); for($i = 1; $i < count($mats); $i++): ?>
            new ymaps.control.ListBoxItem({data: {id: '<?=$i?>', content: '<?=$mats[$i]?>'}}),
		<? endfor; ?>
            new ymaps.control.ListBoxSeparator(),
            new ymaps.control.ListBoxItem({data: {id: '0', content: 'Все постройки'}})
        ]
    }, { expandOnClick: true, collapseTimeout: 1000, position: {top: 5, left: 5}});
	<? for($i = 0; $i < count($mats)-1; $i++): ?>
	typeList.get(<?=$i?>).events.add('click', function (e) {
		filterObjectMaterial(e.get('target').data.get('id'));
	});
	<? endfor; ?>
	typeList.get(<?=count($mats)?>).events.add('click', function (e) {
		filterObjectMaterial(-1);
	});
	map.controls.add(typeList);

<? if($rows) foreach($rows as $x):
	$is_public = !$guest || $x['public'];

	$p = $x['p'];
	if (!$guest){	// для менеджеров отображаем все поля
		foreach($p as $k => $v)
			$p[$k] = 1;
	}

	$plan_url = $x['plan_id'] ? JRoute::_('index.php?option=com_bauplan&Itemid=14&c=object&pid='.$x['plan_id']) : '#';
	$plan_url_ex = $plan_url.'?build='.$x['build_id'];
	if (!$is_public){
		$plan_url_ex = $plan_url = '#';
	}

	$content = '<div class="ymo">';
	if ($p['a_title'])
		$content.= '<h2><a target="_blank" href="'.$plan_url_ex.'">'.$x['title'].'</a></h2>';

	if ($is_public && $x['image_main'])
		$content.= '<div class="ymo_left"><img src="'.BauCore::img($x['image_main'], 140, 140).'" /></div>';
		
	$content.= '<div class="ymo_right">';
	
	if ($is_public && $x['plan_id']){
		$content.= '<small><a href="'.$plan_url.'">'.$x['plan_title'].'</a></small><br/>';
		if ($x['material'])
			$content.= 'Материал: '.BauCore::getObjectMaterials($x['material']).'<br/>';
		if ($x['type'])
			$content.= 'Тип постройки: '.BauCore::getObjectTypes($x['type']).'<br/>';
		if ($x['area'])
			$content.= 'Общая площадь: '.BauCore::f($x['area'], 'area0').'<br/>';
		if ($x['area_live'])
			$content.= 'Жилая площадь: '.BauCore::f($x['area_live'], 'area0').'<br/>';
		if ($x['size_w'] * $x['size_l'])
			$content.= sprintf("Габариты строения: %.2f x %.2f м<br/>", $x['size_w'], $x['size_l']).'<br/>';
	}
	if ($p['a_text'])
		$content.= '<p>'.$x['text'].'</p>';

	if ($is_public && $x['articles'] && $p['a_articles']){
		$content.= 'Статьи по теме:<br/>';
		foreach($x['articles'] as $ar){
			$content.= '<a href="'.JRoute::_(
				'index.php?option=com_content&Itemid=12&catid='.$ar['catid'].'&id='.$ar['id'].'&view=article'
			).'">'.$ar['title'].'</a><br/>';
		}
	}

	$content.= '</div><br class="ymo_clear"/></div>';

	if ($guest){
		// cra3k c00rds - смещаем координаты
		$xc = explode(',', $x['coord']);
		if (isset($xc[1])){
			$xc[0] += (rand(10, 100) - 55) * 0.0001;
			$xc[1] += (rand(10, 100) - 55) * 0.0001;
			$x['coord'] = implode(',', $xc);
		}
	}

?>
	map.geoObjects.add(new ymaps.Placemark(
		[<?=$x['coord']?>],
		{ typeid: 'build', material: '<?=$x['material']?>', content: '<?=$x['title']?>', <? if(strlen($content) > 80):?>balloonContent: '<?=str_replace("\n", '', $content);?>'<? endif; ?> },
		{ preset: '<?=$x['icon']?>' }
	));
<? endforeach; ?>

	map.setBounds(map.geoObjects.getBounds());
});
</script>