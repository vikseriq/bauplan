<?php

$params = $x->params;

// список изображений
$img_all = get_image_list($x->image_path);
$images = $img_all['image'];
$plans = $img_all['plan'];

//  коэффициент цен объекта
$price_mul = BauCore::price_mul($x->p['multiplier'], $x->p['floors']);

// устанавливаем meta-данные
$document = JFactory::getDocument();
if ($x->meta_title){
	$document->setTitle($x->meta_title);
	$document->addCustomTag('<meta property="og:title" content="'.$x->meta_title.'" />');
}
if ($x->meta_desc){
	$document->setMetaData('description', $x->meta_desc);
	$document->addCustomTag('<meta property="og:description" content="'.$x->meta_desc.'" />');
}
if ($x->meta_keys)
	$document->setMetaData('keywords', $x->meta_keys);
if ($x->image_main){
	$juri = JUri::getInstance();
	$document->addCustomTag('<meta property="og:url" content="'.$juri->toString().'" />');
	$document->addCustomTag('<meta property="og:image" content="'.$juri->root().$x->image_main.'" />');
}


?>

<div class="b-oneit">
    <h1 class="b-page-head"><?php echo $x->title; ?></h1>
    <div class="b-oneit-tb">
		<div class="b-one-it-left">

			<div class="b-proj-slider">

				<div class="b-ps-bigs">
					<? foreach ($images as $i) printf('
					<div><a data-fancybox-group="gallery-photo" class="c-pop-link" href="%s"><img src="%s" alt="" width="515" height="375"></a></div>
					', $i, $i); ?>
				</div>

				<div class="b-wrap-ps-smalls">
					<div class="b-ps-smalls">
						<? foreach ($images as $i) printf('<div><img src="%s" alt=""></div>', BauCore::img($i, 110, 77)); ?>
					</div>
				</div>

			</div>
		</div>
		<div class="b-one-it-right">
			<ul class="b-ihs-meta-sq-list">
				<li class="b-item">
					<div class="b-ico"><img src="<?=BP_ICON_PATH?>im1.png" alt=""></div>
					<div class="b-pn">Общ.пл. / Жил.пл.</div>
					<div class="b-pv"><?=BauCore::f($x->area, 'area1')?> / <?=BauCore::f($x->area_live, 'area1')?></div>
				</li>
				<li class="b-item">
					<div class="b-ico"><img src="<?=BP_ICON_PATH?>im2.png" alt=""></div>
					<div class="b-pn">Размеры</div>
					<div class="b-pv"><?php if ($x->size_w) printf("%.1f x %.1f м", $x->size_w, $x->size_l); else echo '&mdash;'; ?></div>
				</li>
				<? foreach ($plans as $i)
					printf('
<li class="b-item">
	<div class="b-ico">
		<a data-fancybox-group="gallery-photo2" class="c-pop-link" href="%s"><img src="%s" alt=""></a>
	</div><div class="b-pv">%s</div>
</li>',
						$i,
						BauCore::img($i, 110, 110),
						BauCore::planTitle($i)
					);
				?>
			</ul>
			<?php if ($params['price_sale']): ?>
				<div class="b-price"><?=BauCore::f($params['price_sale'], 'pricerub')?></div>
				<div class="b-old-price"><i><? if ($params['containment']) echo $params['containment']; ?></i><span><?=BauCore::f($x->price_min, 'pricerub')?></span></div>
			<? else: ?>
				<div class="b-price"><?=BauCore::f($x->price_min, 'pricerub')?></div>
			<? endif ?>
            <div class="b-includes"><span>В указанную стоимость входит:</span>
                стены наружные и внутренние, нагеля, межвенцовый утеплитель, биообработка, балки перекрытий с
                черновым настилом, стропильная система с временной кровлей, доставка (до 150км от г.Тверь),
                сборка на Вашем участке.
            </div>
        </div>
	</div>
	<!-- /.b-oneit-tb -->
	<ul class="b-ihs-prop-list">
		<li class="b-item">
			<div class="b-tb">
				<div class="b-ihsp-ico"><img src="<?=BP_ICON_PATH?>ip1.png" alt=""></div>
				<div class="b-ihsp">Площадь жилая: <i><?=BauCore::f($x->area_live, 'area1')?></i></div>
			</div>
		</li>
		<?=object_param_html($x)?>
	</ul>

	<div class="b-oneit-desc">
		<? if ($x->description): ?>
			<h2 class="b-head">Описание проекта</h2>
			<?=$x->description?>
		<? endif ?>
    </div>

    <div class="b-all-price-z">* Все цены ориентировочные, для получения подробной сметы, просьба заполнить форму ниже или связаться с нашими менеджерами.</div>
    <ul class="b-btm-links">
        <li><a class="b-link-compmore" href="">подробнее<br>о комплектации</a></li>
        <li><a class="b-link-getfull" href="">получить<br>полную смету</a></li>
    </ul>
</div><!-- /.b-oneit -->


<? if ($x->area){
	foreach ($tables as $i => $table){
		if ($table['title'] == 'Оцилиндрованное бревно') $table['title'] = "Цена на силовую часть из оцилидрованного бревна";
		if ($table['title'] == 'Брус') $table['title'] = "Цена на силовую часть из бруса";    // Такие вот "хаки"
		?>
		<div class="b-pr-desc">
			<div class="b-prdesc-head"><? echo $table['title'] ?></div>
			<table class="b-table">
				<tr>
					<th>Вид материала</th>
					<th>Стоимость за <?php echo BauCore::f($x->area, 'areas') ?></th>
				</tr>
				<? foreach ($table['items'] as $j => $r){
					echo '<tr class="'.($j == 0 ? 'tr-pad' : '').'">
						<td>'.$r['title'].'</td>
						<td>'.BauCore::f($r['value'] * $x->area * $price_mul, 'priceru').'</td>
					</tr>';
				} ?>
			</table>
			<div class="b-prdesc-z">Стоимость вычисляется исходя из площади возводимого объекта, а так же варианта
				исполнения (вида материала) и является ориентировочной.<br>В цену включено: доставка до МКАДа,
				высота
				подъема второго этажа не более 1.20 м., высота 1-го этажа до 2.60 м.
			</div>
		</div>
	<? }
} ?>

<?php
if ($this->cparam->get('enable_comments', 1)){
	$comments = JPATH_BASE.'/components/com_jcomments/jcomments.php';
	if (file_exists($comments)){
		/*
		echo '<br/><script type="text/javascript">
		function toggle_jc(){ jQuery(\'#jc\').slideToggle(); }
		jQuery(document).bind("ready", toggle_jc)</script>
		<div class="b-show-comments"><a href="">Посмотреть комментарии или оставить свой</a></div><br/>'."\n\n";
		*/
		require_once($comments);
		echo JComments::showComments($x->plan_id, 'com_bauplan', $x->title);
	}
}
?>
