<div class="b-item-hs">
    <div class="b-item-hs-inner">
        <div class="b-head"><a id="<?=$x->plan_id?>" href="<?=$x->url?>"><?=$x->title?></a></div>
        <div class="b-ihs-tb">
            <div class="b-ihs-photo">
                <div class="b-photo"><a href="<?=$x->url?>"><img src="<?=$x->image_main?>" width="239" height="165" alt="<?=BauCore::alt($x->title)?>"></a></div>
            </div>
            <div class="b-ihs-prop">
                <div class="b-ihs-prop-head">Особенности</div>
                <ul class="b-ihs-prop-list">
                    <li class="b-item"><div class="b-tb">
                            <div class="b-ihsp-ico"><img src="<?=BP_ICON_PATH?>ip1.png" alt=""></div>
                            <div class="b-ihsp">Площадь жилая: <i><?=BauCore::f($x->area_live, 'area1')?></i></div>
                        </div></li>
                    <li class="b-item"><div class="b-tb">
                            <div class="b-ihsp-ico"><img src="<?=BP_ICON_PATH?>im2.png" alt=""></div>
                            <div class="b-ihsp">Размеры: <i><?php if ($x->size_w) printf("%.1f x %.1f м", $x->size_w, $x->size_l); else echo '&mdash;'; ?></i></div>
                        </div></li>
			        <?php echo list_param_html($x->params); ?>
                </ul>
            </div>
            <div class="b-ihs-meta-sq">
                <ul class="b-ihs-meta-sq-list">
                    <li class="b-item">
                        <div class="b-ico"><img src="<?=BP_ICON_PATH?>im1.png" alt=""></div>
                        <div class="b-pn">Общая площадь</div>
                        <div class="b-pv"><?=BauCore::f($x->area, 'area1');?></div>
                </ul>
            </div>
            <div class="b-ihs-meta-pr">
                <!--<div class="b-add2comp"><a href="">Добавить к сравнению</a></div>-->
                <div class="b-link-more"><a href="<?=$x->url?>">подробнее</a></div>
                <div class="b-label">ЦЕНА</div>
		        <?php if ($params['price_sale']): ?>
                    <div class="b-price"><?=BauCore::f($params['price_sale'], 'pricerub')?></div>
                    <div class="b-old-price"><i><? if ($params['containment']) echo $params['containment']; ?></i><span><?=BauCore::f($x->price_min, 'pricerub')?></span></div>
		        <? else: ?>
                    <div class="b-price"><?=BauCore::f($x->price_min, 'pricerub')?></div>
		        <? endif ?>
            </div>
        </div>
    </div>
</div><!-- /.b-item-hs -->