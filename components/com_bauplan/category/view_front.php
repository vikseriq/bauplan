<h1 class="b-page-head">Проекты</h1>

<ul class="b-ccat-list">
	<? foreach($rows as $i => $x): ?>
        <li class="b-item">
            <div class="b-pic"><a href="<?=$x->url?>"><img src="<?=$x->image?>" width="311" height="141" alt="<?=BauCore::alt($x->title)?>"></a></div>
            <div class="b-inner">
                <div class="b-text">
                    <div class="b-head"><a href="<?=$x->url?>"><?=$x->title?></a></div>
                    <p><?=$x->description?></p>
                </div>
	            <? // перебираем варианты площадей
	            if ($x->selector && ($a = explode(';', $x->selector)) && count($a) && strlen(trim($a[0]))):
	            ?>
                <div class="b-selsq-ss">
                    <div class="b-label">Выбрать площадь</div>
                    <div class="b-pop-list">
                        <ul class="b-list">
	                        <? foreach ($a as $sq){
		                        $_val = explode('=', trim($sq));
		                        if (count($_val) > 1 && strlen(trim($_val[0]))){
			                        $_sub = explode('-', $_val[1]);
			                        printf('<li><a href="%s">%s</a></li>',
				                        JRoute::_($x->url.'?from='.$_sub[0].'&till='.$_sub[1]),
				                        $_val[0]);
		                        }
	                        }?>
                        </ul>
                    </div>
                </div>
                <? endif ?>
            </div>
        </li>
	<? endforeach; ?>
</ul><!-- /.b-ccat-list -->

<article class="b-article b-article-sub">
    <h2>Предлагаем вашему вниманию готовые проекты деревянных домов.</h2>
    <p>Также в этом разделе вы найдете фотографии объектов, построенных специалистами компании «ДомиК». Все проекты деревянных домов разделены на категории, однако такая классификация является условной: любой вариант может быть возведен как из бруса, так и из бревна. При необходимости вы также можете воспользоваться услугой адаптации понравившегося архитектурно-планировочного решения к индивидуальным потребностям.</p>
</article>






