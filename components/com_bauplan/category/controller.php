<?php
	// BauPlan - пользовательская часть
	// vikseriq		2011-08-25 / J!3 2015-02-11

define('VOTE_COOKIE', 'RosPil');
include_once(__DIR__.'/helpers.php');

class categoryController extends JControllerV {

	var $type;
	var $content;
	var $db;
	/**
	 * @var categoryModelcategory data model
	 */
	var $m;
	var $cparam;

	function __construct( $d = array() ) {
		parent::__construct( $d );

		$this->pid = JRequest::getVar('pid', 0);	// plan_id
		$this->cid = JRequest::getVar('cid', 0);	// category_id
		$this->type = JRequest::getVar('view', 'category');
		$this->limitstart = JRequest::getVar('start', 0);
		$this->content = null;
		$this->m = $this->getModel();
		$this->db = JFactory::getDBO();
		$this->cparam = JComponentHelper::getParams('com_bauplan');

		// fixing 'canonical'
		$document = JFactory::getDocument();
		foreach($document->_links as $k => $v)
			if ($v['relation'] == 'canonical'){
				unset($document->_links[$k]);
				break;
			}

		// include assets
		//$document = JFactory::getDocument();
		//$document->addScript('/components/com_bauplan/assets/bp.js');
	}

	function show() {
		if ($this->cparam->get('enable_rating', 0) && isset($_COOKIE[VOTE_COOKIE]))
			$this->vote();		// подсистема голосования

		if ($this->type == 'object' && $this->pid > 0) {
			// страница объекта
			$this->content = $this->loadObject($this->pid);
			$this->display_page();
			return;
		} else
		if ($this->cid > 0) {
			// страница категории
			$this->content = $this->loadCategory($this->cid);
			$this->display_page();
			return;
		} else
		if ($this->type == 'search') {
			// поиск
			$this->content = $this->loadSearch();
			$this->display_page();
			return;
		} else
		if ($this->type == 'search_ajax') {
			// поиск ajax
			$this->ajax = true;
			$this->display_ajax($this->loadSearch());
			return;
		} else
		if ($this->type == 'compare') {
			// сравнение
			$this->displayComparison();
		} else
		if ($this->type == 'map') {
			// карта
			$this->displayMap();
		} else
		if ($this->type == 'add') {
			// добавление точки
			$this->displayAddForm();
		} else {
			// главная страница
			$this->displayFrontPage();
			return;
		}

	}

		// стартовая страница
	function displayFrontPage(){
		$cats = $this->m->getCategories('', 'index.php?option=com_bauplan&view=category&cid=');

		// переходим на первую из категорий с сохранением ссылки
		$shadow_frontpage = false;

		if ($shadow_frontpage){
			if (count($cats)){
				$cat = array_shift($cats);
				$this->cid = $cat->category_id;
				$this->content = $this->loadCategory($this->cid);
				$this->display_page();
			}
		} else {
			$this->view('front', array('rows' => $cats));
		}
	}

		// страница сравнения
	function displayComparison(){
		$this->setTitle("Сравнение объектов");
		$this->addPathway("Сравнение объектов");
		$g = $this->get_bp_list('compare');
		$data = null;
		if ($g)
			$data = $this->m->getObjectsByFilter("SELECT * FROM ".$this->m->table_plan." WHERE ".
				"published = 1 AND plan_id IN (".$g.") LIMIT 0, 20;",
				'index.php?option=com_bauplan&view=object&pid='
			);
		$this->view('comparison', array('rows' => $data));
	}

		// постройки на карте
	function displayMap(){
		if (!$this->cparam->get('enable_map', 1)){
			$this->setRedirect("index.php?option=com_bauplan", "Модуль карт отключен");
			$this->redirect();
		}
		$data = $this->m->getBuildings();
		$this->view('map', array('rows' => $data));
	}

		// добавление точки на карту
	function displayAddForm(){
		if (!$this->cparam->get('enable_map', 1)){
			$this->setRedirect("index.php?option=com_bauplan", "Модуль карт отключен");
			$this->redirect();
		}
		$me = JApplication::getUser();
		if ($me->guest || $me->gid < 23){
			$this->setRedirect("index.php?option=com_user&task=login", "Войдите в систему");
			$this->redirect();
		}
		$data = new stdClass();
		$data->build_id = $data->plan_id = $data->published = $data->public = 0;
		$data->text = "";
		$data->articles = "";
		$data->coord = "55.992013,37.213803";
		$data->title = "Новое строение";
		$this->view('build_add', array('row' => $data));
	}

		// категория
	function loadCategory( $cid ) {

		$x = $this->m->getCategory((int)$cid);
		if (!$x) {
			return "Запрошенная категория отсутствет";
		}
		$this->setTitle($x->title);

		// счетчик просмотров
		$this->m->hit('category', $x->category_id);

		// составляем запрос с учетом фильтров
			// Важно: если у нас в запросе лимиты по площади - модифицируем фильтр
			// признак этого: ненулевые from и till в GET
		$_from = $_till = 0;
		gv($_from, 'from', 0, 'get', 'int');
		gv($_till, 'till', 0, 'get', 'int');
		if ( $_till > 0 && $_from < $_till) {
			// удовлетворяют условиям -> в параметры фильтра
			$x->use.= ',area';
			$x->area_from = $_from;
			$x->area_till = $_till;
		}
			// конец модификации
		$q = $this->m->getCategoryFilter($x);


		// загружаем список объектов
		$data = null;
		$this->m->limit = $x->limit;
		$this->m->limitstart = $this->limitstart;
		if ($x->use) {	// если испольуется фильтр + пагинация
			$data = $this->m->getObjectsByFilter($q, '', true);
		}

		if (!$data) {
			if (!$_till)
				return '<div class="bp_list_empty">Данная категория пуста</div>';
			else
				return '<div class="bp_list_empty">Подкатегория пуста</div>';
		}

		// устанавливаем ссылки
		$i = count($data); while ($i--) {
			//$data[$i]->url = JRoute::_('index.php?option=com_bauplan&c=object&cid='.$x->category_id.'&pid='.$data[$i]->plan_id);
			$data[$i]->url	= JRoute::_('index.php?option=com_bauplan&view=object&pid='.$data[$i]->plan_id);

				// автоматическая подстановка категории
			if ($x->category_id != $data[$i]->category_tab &&
				($data[$i]->category_tab == 0)) {
				$this->m->setObjectCategoryTab( $data[$i]->plan_id, $x->category_id);
			}
			//
		}

		// метатеги
		$app = JFactory::getApplication();
		$menu_item = $app->getMenu()->getActive();
		if ($menu_item){
			$menu_params = $menu_item->params;
			$document = JFactory::getDocument();
			if ($title = $menu_params->get('page_title')){
				$document->setTitle($title);
				$document->addCustomTag('<meta property="og:title" content="'.$title.'" />');
			}
			if ($desc = $menu_params->get('menu-meta_description')){
				$document->setMetaData('description', $desc);
				$document->addCustomTag('<meta property="og:description" content="'.$desc.'" />');
			}
			if ($keywords = $menu_params->get('menu-meta_keywords'))
				$document->setMetaData('keywords', $keywords);
		}

		// постраничная
		$pages = $this->m->getPagination('option=com_bauplan&view=category&cid='.$cid.'&')->getPagesLinks();
		$pages = str_replace('limitstart=0', '', $pages);

		return $this->view('object_list', array('rows' => $data, 'pages' => $pages ), false);
	}

	var $related = null; // похожие проекты
		// объект
	function loadObject( $pid ) {

		// загружаем объектъ
		$x = $this->m->getObject( (int)$pid );

		if (!$x) {	// не найдено
			$this->setRedirect("index.php?option=com_bauplan", "");
			$this->redirect();
		}
		// счетчик просмотров
		$this->m->hit('object', $x->plan_id);

		// загружаем ценовые таблицы
		$tables = $this->m->getPriceTables( $x->p['tables'] );

		// получаем список "похожих проектов"
		$this->related = $this->getObjectRelated( $x->p['related'] );

		// категория по-умолчанию
		$this->cid = $x->category_tab;

		// получаем статьи для внедрения, если это страница просмотра постройки
		if ($build_id = JRequest::getInt('build', 0)){
			$x->build = $this->m->getBuild($build_id, $x->plan_id);
		}
		// дополнительная галерея
		if ($gallery_id = $x->p['gallery_id']){
			$x->gallery = $this->m->getGallery($gallery_id);
		}

		// расскрываем параметры
		$x->params = BauCore::str2params($x->params);

		$this->setTitle($x->title);
		$this->addPathway($x->title);
		return $this->view('object_page', array(
			'x' => $x,
			'tables' => $tables,
			'obj_newest'	=> $this->getObjectNewest(),
			'obj_featured' 	=> $this->getObjectFeatured(),
			'obj_related' 	=> $this->related
		), false);
	}

		// поиск
	function loadSearch() {
		if (!$this->ajax){
			$this->setTitle("Поиск объектов");
			$this->addPathway("Поиск");
		}
		// p - мнимая категория
		$p = new stdClass();
		$p->use = 'area,price';
		$p->sort_column = 1; $p->sort_order = 1; $p->limit = 15;	// по площади; 15 на страницу
		gv($p->area_from, 	'area_a',	0, 'get', 'int');
		gv($p->area_till, 	'area_b', 750, 'get', 'int');
		gv($p->price_from, 	'price_a',  0, 'get', 'int');
		gv($p->price_till, 	'price_b',	10*1000*1000, 'get', 'int');
		gv($p->material, 	'material',	0, 'get', 'int');
		gv($p->type,	 	'type',		0, 'get', 'int');
		gv($p->floor,	 	'floor',	0, 'get', 'int');
		gv($p->extra,	 	'extra',	[], 'get', 'array');
		if ($p->material)	$p->use.= ',material';	// обработка варианта "любой"
		if ($p->type)		$p->use.= ',type';
		if ($p->floor)		$p->use.= ',floor';
		if ($p->extra)		$p->use.= ',extra';

		gv($p->title,		'title',	'', 'get', 'string');
		$p->title = addslashes($p->title);
		if (strlen($p->title) >= 3)
			$p->use.= ',title';

		// а теперь все параметры назад в _GET для верного отображения правого блока
		$_GET['price_a'] = $p->price_from;
		$_GET['price_b'] = $p->price_till;
		$_GET['area_a'] = $p->area_from;
		$_GET['area_b'] = $p->area_till;
		$_GET['material'] = $p->material;
		$_GET['type'] = $p->type;
		$_GET['floor'] = $p->floor;
		$_GET['extra'] = $p->extra;
		$_GET['title'] = $p->title;
		define('BP_SEARCH', true);

		// как видим, по такой заготовке легко фильтровать
		$q = $this->m->getCategoryFilter($p);

		// загружаем список объектов
		$data = null;
		$this->m->limit = $p->limit;
		$this->m->limitstart = $this->limitstart;

		$data = $this->m->getObjectsByFilter($q, '', true);

		if (!$data) {
			return "Поиск не дал результатов";
		}

		// устанавливаем ссылки
		$i = count($data); while ($i--) {
			//$data[$i]->url = JRoute::_('index.php?option=com_bauplan&c=object&cid=0&pid='.$data[$i]->plan_id);
			$data[$i]->url = JRoute::_('index.php?option=com_bauplan&view=object&pid='.$data[$i]->plan_id);
		}

		// постраничная
		$pages = $this->m->getPagination('option=com_bauplan&view=search&')->getPagesLinks();
		$pages = str_replace('limitstart=0', '', $pages);

		return $this->view('object_list', array('rows' => $data, 'pages' => $pages), false);
	}

		// Модульное отображение
	function display_page( &$content = "N/A" ) {

		// получаем верхние вкладки
		$cats = $this->m->getCategories();
		$tabs = $this->view('tabs', array('cats' => $cats, 'current' => $this->cid), false);

		// сайдбар и фильтры
		$sidebar = $this->view('sidebar', array(
			'types' 		=> BauCore::getObjectTypes(),
			'materials' 	=> BauCore::getObjectMaterials(),
			'floors'		=> BauCore::getFloors(),
			'obj_newest'	=> $this->getObjectNewest(),
			'obj_featured' 	=> $this->getObjectFeatured(),
			'obj_related' 	=> $this->related,
			'comparison'	=> $this->getObjectListBP('compare'),
			'faves'			=> $this->getObjectListBP('fave'),
			'categories'	=> $cats,
			'cid'	 		=> $this->cid
			), false);

		// собственно контент передан в $html или в $this->content
		if (!$this->content) $this->content = $content;

		$this->view('layout', array('tabs' => $tabs, 'sidebar' => $sidebar, 'content' => $this->content));

	}

	function display_ajax($content){
		ob_end_clean();

		echo $content;

		exit;
	}

		// последние добавленные
	function getObjectNewest() {
		if (!$this->cparam->get('show_newest', 1)) return null;
		return $this->m->getObjectsByFilter("SELECT * FROM ".$this->m->table_plan." WHERE ".
				"published = 1 ORDER BY cdate DESC LIMIT 0, 5;",
				'index.php?option=com_bauplan&view=object&pid='
			);

	}

		// рекомендованые проекты
	function getObjectFeatured() {
		if (!$this->cparam->get('show_featured', 1)) return null;
		return $this->m->getObjectsByFilter("SELECT * FROM ".$this->m->table_plan." WHERE ".
				"published = 1 AND ((INSTR(params, 'featured=1')) OR (SUBSTR(rate, 1, 1) > '3')) ORDER BY RAND() LIMIT 0, 5;",
				'index.php?option=com_bauplan&view=object&pid='
			);
	}

		// похожие проекты
	function getObjectRelated( $list = null ) {
		if (!$list) return null;
		return $this->m->getObjectsByFilter("SELECT * FROM ".$this->m->table_plan." WHERE ".
				"published = 1 AND plan_id IN ( ".$list." );",
				'index.php?option=com_bauplan&view=object&pid='
			);
	}

		// объекты из списка
	function getObjectListBP($listname) {
		if ($g = $this->get_bp_list($listname)) {
			$a = $this->m->getObjectsByFilter("SELECT plan_id, title FROM ".$this->m->table_plan." WHERE ".
				"published = 1 AND plan_id IN (".$g.") LIMIT 0, 10;",
				'index.php?option=com_bauplan&view=object&pid='
			);
			return $a;
		}
		return null;
	}

		// система голосования
	private function vote(){	// AJAX? Не, не слыхал. Хорошо получилось
		if (!$this->cparam->get('enable_rating', 0)) return;
		$r_cookie = 'votex';
		$val = '';
		gv($val, VOTE_COOKIE, '0;0', 'cookie', 'string');
		$val = explode(';', $val);
		$pid	= (isset($val[0])) ? (int)$val[0] : 0;
		$mark	= (isset($val[1])) ? (int)$val[1] : 0;

		$time = time();
		if ($pid <= 0 || ($mark <= 0 || $mark > 5)) return;
		setcookie(VOTE_COOKIE, '', 0);	// забираем печеньки

		gv($val, $r_cookie, '', 'cookie', 'string');	// были ли тут
		$ext = '_'.$pid.',';
		if (strpos($val, $ext)!==false){
			return;		// уже есть печенька, больше не даём
		}
		$this->db->setQuery("SELECT rate FROM ".$this->m->table_plan." WHERE published = 1 AND plan_id = ".$pid." LIMIT 1;");
		$a = $this->db->loadResult();	// format 'result;total;sum;last_time'

		$a_rate = explode(';', $a);
		if (!isset($a_rate[1])) $a_rate[1] = 1; 	else $a_rate[1]++;	// set total
		if (!isset($a_rate[2])) $a_rate[2] = $mark; else $a_rate[2] += $mark;	// set total
		if (isset($a_rate[3]))
			if ($time - $a_rate[3] < 3*60) return;	// не чаще раза в 3 мин. на объект

		setcookie($r_cookie, $val.$ext, $time+3600*5);	// отмечаем голос

		$a_rate[0] = $a_rate[2]/$a_rate[1];
		$a = sprintf("%.2f;%d;%d;%d", $a_rate[0], $a_rate[1], $a_rate[2], $time);

		$this->db->setQuery("UPDATE ".$this->m->table_plan." SET rate = '".$a."' WHERE published = 1 AND plan_id = ".$pid." LIMIT 1;");
		$this->db->query();

		return;
	}

	private function get_bp_list($listname){
		if (isset($_COOKIE['bp-'.$listname])){
			$g = str_replace(array('\'', '"', 'x', '+', ' ', '%'), '', $_COOKIE['bp-'.$listname]);
			if (strlen($g)) return $g;
		}
		return null;
	}

}

?>