<?php

// список рядов
$trs = array
	(
"title" 		=> "Название проекта",
"image_main" 	=> "Изображение",
"type" 			=> "Тип",
"material" 		=> "Материал",
"area" 			=> "общая площадь",
"area_live" 	=> "Жилая площадь",
"size" 			=> "Размеры",
"floors" 		=> "Этажей",
"floor_0" 		=> "Цокольный этаж",
"floor_b" 		=> "Балконов",
"car" 			=> "Гараж",
"swim" 			=> "Сауна",
"floor_out" 	=> "Терраса",
"price_min" 	=> "Цена: от",
/*"rate" 			=> "Рейтинг",*/
	);

// код, выводящий список - раскоментировать при создании, отсортировать и вставить в $trs
/*if ($y = $rows[0]){
	$x = (array)$y;
	$x = array_merge($x, BauCore::str2params($y->params));
	foreach ($x as $k => $v){ echo '"'.$k.'" => "",</br>'; }
	$x = null; die;
}*/

?>
<style>
	table#tcmp {
		width: 100%;
	}
	table#tcmp .row0 {
		background-color: #EDF6FF;
	}
	table#tcmp tr td {
	    min-width: 250px;
	    padding: 5px 8px;
	}
	table#tcmp td.rkey {
	    color: #91b23d;
	    font-size: 13px;
	    font-weight: bold;
	}
	table#tcmp .title {
		color: #91b23d;
		font-size: 24px;
	}
</style>
<div class="category_block">
	<div class="category_content" style="overflow: auto;">
		<? if ($rows): ?>
<table id="tcmp">
<?php
$r_i = 0;
foreach ($trs as $name => $text){
	echo '<tr class="row'.($r_i++%(2)).'"><td class="rkey">'.$text.'</td>';
	foreach ($rows as $x){
		if (!isset($x->p)) $x->p = BauCore::str2params($x->params);
		echo "\n\t<td>";
		//
	switch($name){
		case 'image_main':	echo '<img src="'.BauCore::img($x->image_main, 200, 150).'" style="width:200px;min-height:100px;" /></a>'; break;
		case 'title':		printf('<a class="title" href="%s" target="_blank">%s</a>', $x->url, $x->title); break;

		case 'area':
		case 'area_live':	echo BauCore::f($x->$name, 'areas');				break;

		case 'price_min':	echo BauCore::f($x->price_min, 'priceru');			break;
		case 'type':		echo BauCore::getObjectTypes($x->type); 			break;
		case 'material':	echo BauCore::getObjectMaterials($x->material);		break;

		case 'size':		printf("%.2f x %.2f м", $x->size_w, $x->size_l);	break;

		case 'car':
		case 'swim':
		case 'floor_0':
		case 'floor_out':	if ($x->p[$name] > 0) echo 'Есть'; else echo 'Нет';	break;

		case 'floor_b':
		case 'floors':		echo $x->p[$name];									break;
	}
		//
		echo "</td>";
	}
	echo "</tr>\n";
}
?>
</table>
		<? else: ?>
		<p>Нет объектов для сравнения</p>
		<? endif ?>
	</div>
</div>