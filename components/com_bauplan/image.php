<?php
/* 	ImageTools	v 0.3
 *	Resize, watermark and cache image "on the fly"
 *	Обработка изображений "на лету": изменение размера, наложение водяного знака; используется кеширование результата
 * 	vikseriq@gmail.com / August 2011 / http://vikseriq.xyz/
 *	GNU GPL	 *
 */
/* @param: GET 'i' = image file name */
/* @param: last six chars in request_uri = image req. size if parselable */
/* @param: OR format filename_WWWHHH.ext */
	define('AGE', 1*24*3600);		// срок кеширования данных на стороне клиента, в сек.
	define('QUALITY', 90);		// качество результирующего jpeg-изображения
	define('WATERMARK', "watermark.png"); // изображения для водяного знака, поддерживается png c прозрачностью
		// в директории с этим файлом; если не задано или отсуствует - водяной знак не накладывается
	define('OFFSET_X',  5); define('OFFSET_Y',  5);	// смещение водяного
	define('BIG_X',   800); define('BIG_Y',   600);	// дублирование водяного
	define('SMALL_X', 255); define('SMALL_Y', 255);	// водяной не накладывается
	define('LARGE_X',1024); define('LARGE_Y', 768);	// максимальные размеры результата
	define('LITE_X',   10); define('LITE_Y',   10);	// минимальные  размеры	результата
	$prjpath = realpath(dirname(__FILE__)."/../../images/projects/");
	define('CACHE', $prjpath."/cache/");	// директория общего кеша

do {
	$e = true;	// контроль над ошибками

// ОБРАБОТКА ЗАПРОСА
	//$file = $prjpath.'/'.$_GET['i'];	// ИСПРАВИТЬ НА ЦЕЛЕВОМ ПРОЕКТЕ!
	$file = $prjpath.'/'.substr($_GET['i'], strlen('/images/projects/') -1);	// ИСПРАВИТЬ НА ЦЕЛЕВОМ ПРОЕКТЕ!

	if ($file[strlen($file)-11] == '_' && $file[strlen($file)-10] < 'f' ) {
		// это формат с размерами изображения в имени
		$rs = substr($file, -10, 6);
		$tmp = substr($file, -4);
		$file = substr($file, 0, strlen($file)-11).$tmp;
	} else {
		// иначе формат - размер после '?'
		$rs =   substr($_SERVER['REQUEST_URI'], -6);
	}
	if (!file_exists($file)) break;	// нет файла - нет дела
	//die($rs.' '.$file);
// ВЫБОРКА ИЗ КЕША и инициализация $cache и $file_cache
	$cache = CACHE.'/'.$rs.'/';
	$file_cache	= $cache.substr(md5($file), 2, 6).'.jpg';
	$from_cache = false;
	$file_time = time();
	if (file_exists($file_cache)){			// в кеше есть	- нужна ли такая же проверка с файлом водяного знака?
		$file_time = filemtime($file_cache);
		$fmt = filemtime($file); 		// время создания файла
		if (file_exists(WATERMARK)){ 	// время создания водяного
			$ftt = filemtime(WATERMARK); 
			if ($fmt < $ftt) $fmt = $ftt;	// наибольшее в fmt
			$ftt = filemtime(__FILE__); 
			if ($fmt < $ftt) $fmt = $ftt;	// наибольшее в fmt
		}
		if ($fmt > $file_time){ // но если оригинальное/водяной создано позднее, чем в кеше
			@unlink($file_cache);	// то удаляем из кеша и обновляем в последующем коде
			$file_time = time(); 	// и ставим дату новую
		} else {		// иначе просто выводим
			$from_cache = true; $e = false;	// сбрасываем флаг ошибки
			break; // ну что ж поделать, коли goto нет
		}
	}
	
// ЗАГРУЗКА ОСНОВНОГО ИЗОБРАЖЕНИЯ в $orig
	$info = getimagesize($file);
	if (!isset($info[2])) break;

	switch($info[2]){
		case 1: 	$orig = imagecreatefromgif ($file); break;
		case 2: 	$orig = imagecreatefromjpeg($file); break;
		case 3: 	$orig = imagecreatefrompng ($file); break;
		default:	$orig = null; break;
	}
	if (!$orig) break;

// ЗАПРОШЕННЫЕ РАЗМЕРЫ		---		2 HEX-числа 3 уровня, позволяют задать до 4095*4095 (0xFFF*0xFFF)
	$ox = imagesx($orig);  $oy = imagesy($orig);	// оригинальный размер, можно взять из info
	
	if (strpos($rs, '.')) {		// если параметры не переданы, а сразу расширение файла - то как оригинальные
		$rx = $ox; $ry = $oy;
	} else {
		$rx = base_convert(substr($rs, 0, 3), 16, 10);
		$ry = base_convert(substr($rs, 3, 3), 16, 10);
		if (!$rx && !$ry) { $rx = $ox; $ry = $oy; }	// одиночные нулевые значения всё же сохраним
	}
		if ($rx > LARGE_X) $rx = LARGE_X;	if ($ry > LARGE_Y) $ry = LARGE_Y;
		if ($rx < LITE_X && $rx ) $rx = LITE_X;	if ($ry < LITE_Y && $ry ) $ry = LITE_Y;

// МАСШТАБИРОВАНИЕ ПО БОЛЬШЕЙ СТОРОНЕ
	$scale = false;
	//printf("original: %d %d :: need: %d %d :: scales: %.3f, %.3f", $ox, $oy, $rx, $ry, $rx/$ox, $ry/$oy);
	if(($rx != $ox && $ry != $oy) || (!$rx || !$ry) ){
		$scale = true;
		if ($rx >= $ry){
			$ry = (int)($rx*$oy/$ox);
		} else {
			$rx = (int)($ry*$ox/$oy);
		}
	}
	//printf(" :: result: %d %d", $rx, $ry);	die;

	if ($scale) {
		$ito = imagecreatetruecolor($rx, $ry);
		$a = imagecopyresampled($ito, $orig, 0, 0, 0, 0, $rx, $ry, $ox, $oy);
		if (!$a) break;
		list($ito, $orig) = array($orig, $ito);
		$ox = $rx; $oy = $ry;
		imagedestroy($ito);
	}

// ЗАГРУЗКА ВОДЯНОГО
  if ($ox > SMALL_X && $oy > SMALL_Y )	// если размеры не меньше обозначенных
  if (strlen(WATERMARK) && file_exists(WATERMARK) && $wmark	= imagecreatefrompng(WATERMARK)){
		// если надо работать с водяным знаком
	$wx = imagesx($wmark); $wy = imagesy($wmark);


// НАЛОЖЕНИЕ ВОДЯНОГО
	imagealphablending ($orig, true);
	$dx = $ox - $wx - OFFSET_X;   $dy = $oy - $wy - OFFSET_Y;	// позиция в нижнем правом
	$a = imagecopy($orig, $wmark, $dx, $dy, 0, 0, $wx, $wy);
	// на большие изображения добавляем метку и в верхнем правом углу
	if ( $ox > BIG_X and $oy > BIG_Y ) {
		$dx = OFFSET_X; $dy = OFFSET_Y;
		$a = imagecopy($orig, $wmark, $dx, $dy, 0, 0, $wx, $wy);
	}
	if (!$a) break;
	imagedestroy($wmark);
  }

// ВЫВОД
// КЕШИРОВАНИЕ на стороне сервера
	do {
		if (!file_exists(CACHE )){ if (!mkdir(CACHE ) &&  !chmod(CACHE, 0777)) break; }		// общая папка кеша
		if (!file_exists($cache)){ if (!mkdir($cache) && !chmod($cache, 0777)) break; }		// локальная папка кеша
		$a = imagejpeg($orig, $file_cache, QUALITY);		// сохраняем
		if ($a) $from_cache = true;
	} while(false); // если не удалось закешировать то что ж поделаешь

	$e = false; 	//ошибок не было
} while(false);
if ($e){ // если произошла ошибка
	header("HTTP/1.0 404 Not Found"); exit;
}

// КЕШИРОВАНИЕ на стороне клиента
	if (AGE > 0) {
	header('Expires: '.gmdate('D, d M Y H:i:s', time()+ AGE).' GMT');
	header('Cache-Control: max-age='.AGE.', must-revalidate');
	//header("Cache-Control: maxage=".AGE.", public");
	//header('Cache-Control: must-revalidate');
	//header('Cache-Control: public');
	/*if ($from_cache){
		header(sprintf('Etag: "%x-%x-%x"', fileinode($file_cache), filesize($file_cache), filemtime($file_cache).'"'));
	}*/
	header('Last-Modified: '.gmdate('D, d M Y H:i:s', $file_time).' GMT');
	//header('Vary: Content-ID');
	//header('X-Powered-By: GD Lib');
	}
	header('Content-Type: image/jpeg');
	header('Content-Disposition: inline; filename='.$_GET['i']);
	if ($from_cache) { //echo "File $file_cache from cache";
		readfile($file_cache);
	} else {
		imagejpeg($orig, null, QUALITY);
	}
	if(isset($orig))  imagedestroy($orig); 
	
exit; // и уходим
?>