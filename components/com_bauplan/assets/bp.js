/**
 * BauPlan
 *
 * Extra scripts
 */

if (typeof jQuery.cookie == 'undefined'){
	jQuery.cookie = function(key, value, options){
		if (arguments.length > 1 && String(value) !== "[object Object]"){
			options = jQuery.extend({
				expires: 1
			}, options);
			if (value === null || value === undefined){
				options.expires = -1;
			}
			if (typeof options.expires === 'number'){
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}
			value = String(value);

			var today = new Date();
			var expires = new Date(today.getTime() + 3600 * 2 * 1000);

			return (document.cookie = [
				encodeURIComponent(key), '=',
				options.raw ? value : encodeURIComponent(value),
				'; expires=' + expires.toGMTString(),
				options.path ? '; path=' + options.path : '; path=/',
				options.domain ? '; domain=' + options.domain : '',
				options.secure ? '; secure' : ''
			].join(''));
		}
		options = value || {};
		var result, decode = options.raw ? function(s){
			return s;
		} : decodeURIComponent;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie))
			? decode(result[1]) : null;
	};
}

if (!Array.indexOf){
	Array.prototype.indexOf = function(obj){
		for (var i = 0; i < this.length; i++){
			if (this[i] == obj){
				return i;
			}
		}
		return -1;
	}
}

/*
var BauPlan = {
	fave: {
		items: [],
		labels: ['добавить в избранное', 'в избранном']
	},
	compare: {
		items: [],
		labels: ['+ к сравнению', 'убрать из сравнения']
	},
	lists: {
		// sync list state and process toggle
		sync: function(list, element, toggle){
			var e = $(element);
			var id = e.attr('data-id');
			var items = BauPlan[list].items;
			var _index = items.indexOf(id);
			var active = _index != -1;

			if (typeof toggle != 'undefined' && toggle == true){
				active = !active;
				active ? items.push(id) : items.splice(_index, 1);
				BauPlan.lists.save(list);
				e.trigger('bp.toggle', active);
			}
			if (active){
				e.attr('data-active', 1);
				e.text(BauPlan[list].labels[1]);
			} else {
				e.attr('data-active', 0);
				e.text(BauPlan[list].labels[0]);
			}
			if (!e.data('list')){
				// setup list and handler
				e.data('list', list);
				e.on('click', BauPlan.lists.handler);
			}
		},
		// handle item click
		handler: function(){
			BauPlan.lists.sync($(this).data('list'), this, true);
			return false;
		},
		// sync listing rows
		syncListing: function(list, newElement){
			var place = $('.js-bp-' + list + '-items');
			if (newElement)
				place.append(newElement);
			// keep only unique and actual rows
			var els = place.find('[data-id]');
			var ids = [];
			els.each(function(){
				var id = $(this).attr('data-id');
				(ids.indexOf(id) != -1 || BauPlan[list].items.indexOf(id) == -1) ? $(this).remove() : ids.push(id);
			});
			$('.js-bp-' + list + '-place').css('display', BauPlan[list].items.length ? 'block' : 'none');
		},
		// load lists from cookies
		init: function(){
			['compare', 'fave'].map(function(list){
				var cookie = $.cookie('bp-' + list);
				BauPlan[list].items = cookie ? cookie.split(',') : [];
			});
		},
		// put list back to cookie
		save: function(list){
			$.cookie('bp-' + list, BauPlan[list].items.join(','));
		}
	},
  // search/filter box
  filter: {
    control: null,
    output: null,
    locked: false,
    init: function(panel, out){
      this.control = panel;
      this.output = out;
    },
    process: function(){
      if (BauPlan.filter.locked)
        return;
      BauPlan.filter.locked = true;
      var form = BauPlan.filter.control.find('form');
      var data = form.serialize();
      var path = form.attr('data-action') || form.attr('action');
      path += (path.indexOf('?') > 0 ? '&' : '?') + data;
      BauPlan.filter.output.css('opacity', 0.5);
      $.get(path)
        .done(function(html){
          BauPlan.filter.output.html(html);
          bpListsHandlers();
          window.history.pushState([], "", path.replace('search_ajax', 'search'));
        })
        .fail(function(html){
          //
        })
        .always(function(){
          BauPlan.filter.output.css('opacity', 1);
          BauPlan.filter.locked = false;
        });
    }
  }
};

var bpListsHandlers = function(){
  BauPlan.lists.init();
  // list add/remove links
  $('.js-bp-compare').each(function(){
    BauPlan.lists.sync('compare', this);
    $(this).on('bp.toggle', function(_, active){
      var item = $(this);
      var line = null;
      if (active)
        line = '<li data-id="' + item.data('id') + '"><a href="' + item.data('url') + '" target="_blank">'
          + item.data('title') + '</a></li>';
      BauPlan.lists.syncListing('compare', line);
    });
  });
  $('.js-bp-fave').each(function(){
    BauPlan.lists.sync('fave', this);
    $(this).on('bp.toggle', function(_, active){
      var item = $(this);
      var line = null;
      if (active)
        line = '<li data-id="' + item.data('id') + '"><a href="' + item.data('url') + '" target="_blank">'
          + item.data('title') + '</a></li>';
      BauPlan.lists.syncListing('fave', line);
    });
  });
};

$(document).ready(function(){
	bpListsHandlers();
	// listing places
	if ($('.js-bp-place')){
		// handle clearing list
		$('.js-bp-list-clear').click(function(){
			var list = $(this).attr('data-list');
			BauPlan[list].items = [];
			$('.js-bp-' + list).each(function(){
				BauPlan.lists.sync(list, this);
			});
			BauPlan.lists.save(list);
			BauPlan.lists.syncListing(list);
			return false;
		});
	}

  var searchPanel = $('.js-bp-filter-panel');
  if (searchPanel.length){
    BauPlan.filter.init(searchPanel, $('.js-bp-filter-out'));
    searchPanel.find('input').on('change slider.change', BauPlan.filter.process);
  }
});
*/