<?php
// BauPaln Router
// vikseriq 	2011-08-25 - 2015-03-06 - 2016-05-08

function bauplanBuildRoute(&$q){
	$s = array();
	$con = "";
	static $menus;
	if (!$menus){
		$menu = JFactory::getApplication()->getMenu();
		$menus = $menu->getItems('component', 'com_bauplan');
	}

	if (isset($q['view'])){
		$con = $q['view'];
		unset($q['view']);
	}

	if (count($menus)){
		if ($con != 'category')
			$q['Itemid'] = $menus[0]->tree[0];
		else
			$base_itemid = $menus[0]->tree[0];
	}

	switch ($con){
		case 'object':  // объекты
			if (isset($q['pid'])){
				$pid = $q['pid'];

				// детальные сведения
				$db = JFactory::getDBO();
				$db->setQuery("SELECT * FROM #__bp_plan WHERE plan_id = ".intval($pid));
				$res = $db->loadAssoc();

				// подбираем категорию
				$cid = $res['category_tab'];
				$cat_alias = '';
				if (count($menus))
					foreach ($menus as $m){
						if (isset($m->query['cid']) && $m->query['cid'] == $cid){
							$cat_alias = $m->alias;
						}
					}
				if ($cat_alias)
					$s[] = $cat_alias;
				else
					$s[] = 'plan';

				// алиас
				$s[] = $res['alias'] ? $res['alias'] : 'plan_'.$q['pid'];
				unset($q['pid']);
			}
			break;
		default:
			if ($con != 'front')
				$s[] = $con;
			break;
		case 'category':
			// ищем категорию в меню
			if (count($menus)){
				$cm = null;
				foreach ($menus as $m){
					if (isset($m->query['cid']) && $m->query['cid'] == $q['cid']){
						// exact menu item
						$cm = $m;
						break;
					}
				}
				if ($cm){
					$q['Itemid'] = $m->id;
					unset($q['cid']);
				} else {
					$q['Itemid'] = $base_itemid;
				}
			}
			break;
	}
	return $s;
}

function bauplanParseRoute($s){
	$q = array();
	$n = count($s);
	if ($n == 2 && $s[0] == 'plan'){
		$s = [$s[1]];
		$n = count($s);
	}
	if ($n == 1){
		// ищем подходящее чпу в объектах
		$alias = str_replace(array(':', "'"), array('-', ''), $s[0]);
		$db = JFactory::getDBO();
		$db->setQuery("SELECT * FROM #__bp_plan WHERE alias = '".$alias."'");
		$object_res = $db->loadAssoc();

		if ($object_res){
			$q['view'] = 'object';
			$q['pid'] = $object_res['plan_id'];
		} else if (intval($s[0]) === $s[0]){
			$q['view'] = 'category';
			$q['cid'] = $s[0];
		} elseif (substr($s[0], 0, 5) == 'plan_') {
			$q['view'] = 'object';
			$q['pid'] = intval(substr($s[0], 5));
		} else {
			$q['view'] = $s[0];
		}
	} elseif ($n >= 2) {
		$q['view'] = 'object';
		$q['pid'] = intval(substr($s[1], 0, 5) == 'plan_' ? substr($s[1], 5) : $s[1]);
	}

	return $q;
}