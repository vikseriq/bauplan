<?php
/**
webpro100r
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
$htm = JFactory::getDocument();
	unset($htm->_scripts['/media/jui/js/jquery.min.js']);
	unset($htm->_scripts['/media/system/js/caption.js']);
	unset($htm->_scripts['/media/jui/js/jquery-migrate.min.js']);//зачем он? поддержка ie6..ie8, т.к. jQuery отказались от динозавров
	unset($htm->_scripts['/media/jui/js/jquery-noconflict.js']);
	unset($htm->_scripts['components/com_k2/js/k2.js?v2.6.9&sitepath=/']);
	unset($htm->_scripts['/media/system/js/core.js']);
	unset($htm->_scripts['/media/system/js/mootools-core.js']);
unset($this->_scripts['/media/system/js/modal.js']);


?><!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, maximum-scale=1.0">
	<jdoc:include type="head" />
	<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/styles/style.css">
	<link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/styles/responsive.css" media="all">
		<!--[if lt IE 9]> 
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
<div class="b-wrap-all">

	<header class="b-header">
		<div class="b-container"><div class="b-container-inner">
			<div class="b-logo"><a href=""><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/logo.png" width="218" height="92" alt="Домик идеальный сервис"></a></div>
			<ul class="b-top-contact">
				<li class="b-item">
					<span class="b-label">Телефон в Твери</span>
					<span class="b-phone">(4822) 61-22-22</span>
				</li>
				<li class="b-item">
					<span class="b-label">Телефон в Москве</span>
					<span class="b-phone">(495) 728-66-85</span>
				</li>
			</ul>
			<ul class="b-top-links">
				<li><a class="b-link-dorder contactus" href="">заявка на встречу</a></li>
				<li><a class="b-link-dcall joomly-callback" href=""><span>заказать звонок</span></a></li>
			</ul>
			<div class="b-mob-nav"></div>
			<nav class="b-top-nav">
			<jdoc:include type="modules" name="position-1" style="none" />
			</nav>
		</div></div><!-- /.b-container -->
	</header><!-- /.b-header -->

	<div class="b-content clearfix">


	
<?//	<!-- Если главная, то выводим -->?>
<? if ($option=='com_content' && $view=='featured') : ?>

<section class="b-section-slider">

			<div class="b-mn-slider c-mn-slider">
<jdoc:include type="modules" name="position-12" style="none" />
				<div class="b-item-slide">
				
					<!--<img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/slide1.jpg" width="1920" height="626" alt="">-->
					<div class="b-slide-text"><div class="b-slide-text-helper"><div class="b-slide-text-helper-cell">
						<div class="b-container"><div class="b-container-inner">
						<div class="b-head"><a href=""><span>Строим </span><span>деревянные дома</span><i>под ключ и под усадку</i></a></div>
						</div></div>
					</div></div></div>
				</div><!-- /.b-item-slide -->

				<div class="b-item-slide">
					<!--<img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/slide2.jpg" width="1920" height="626" alt="">-->
					<div class="b-slide-text"><div class="b-slide-text-helper"><div class="b-slide-text-helper-cell">
						<div class="b-container"><div class="b-container-inner">
							<div class="b-head"><a href=""><span>Строим </span><span>деревянные бани</span><i>под усадку и под ключ</i></a></div>
						</div></div>
					</div></div></div>
				</div><!-- /.b-item-slide -->

			</div><!-- /.b-mn-slider -->
			
			<div class="b-ssl-cnav">
				<div class="b-container"><div class="b-container-inner">
					<ul class="b-list">
						<li><a class="b-link-n1" href="#calculator">Калькулятор</a></li>
						<li><a class="b-link-n2" href="#technologies">Технологии</a></li>
						<li><a class="b-link-n3" href="#guarantees">Гарантии и отзывы</a></li>
						<li><a class="b-link-n4" href="#projects">Проекты и фото</a></li>
						<li><a class="b-link-n5" href="#building">Этапы строительства</a></li>
					</ul>
				</div></div>
			</div>
			
		</section><!-- /.b-section-slider -->

		<section class="b-section-prm"><a name="projects"></a>
			<div class="b-container"><div class="b-container-inner">
				<p><strong>Компания «ДомиК»</strong> оказывает услуги проектирования и возведения деревянных домов и домов из кирпича/камня/монолита. Мы строим качественно, опираясь на традиции русского зодчества и учитывая требования современности. Поэтому главной особенностью возводимых нами домов является сочетание надежности, экологичности, отличного качества и безопасности, а также всегда оригинального дизайна.</p>
				<p>Вы можете убедиться в качестве наших работ, посмотрев <a href="">видеосъемки с реальных объектов</a> или <a href="/portfolio.html">фотогалерею</a>.<br>А так же <a href="">отзывы</a> наших клиентов.</p>
			</div></div>
		</section><!-- /.b-section-prm -->

		<section class="b-section-typecat">
			<div class="b-container"><div class="b-container-inner">
                <jdoc:include type="modules" name="main-categories" style="none" />
                <div class="b-text">
					<div class="b-head">В нашем каталоге типовых проектов </div>
					<p>вы найдёте не только сами проекты, но и реально построенные компанией "Домик" объекты с фотографиями. Основная масса проектов с ценами, цены являются расчетными, для уточнения цен, пожалуйста, <a href="">свяжитесь с нашими менеджерами</a> и вы бесплатно получите предварительную смету.</p>
				</div>
				<div class="b-tbl">
					<div class="b-tbc">
						<p>Все наши проекты содержат фотографии, однако не все построенные нами объекты были отражены в каталоге проектов, поэтому с остальными фотографиями Вы можете ознакомиться в нашей <a href="/portfolio.html">фотогалерее &gt;</a></p>
					</div>
					<div class="b-tbc b-tbc-links">
						<a href="portfolio.html"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/d-link1.png" width="44" height="32" alt=""></a>
						<a href="#"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/d-link2.png" width="44" height="34" alt=""></a>
					</div>
				</div>
			</div></div><!-- /.b-container --><a name="building"></a>
		</section><!-- /.b-section-typecat -->

		<section class="b-buildparam-section">
			<div class="b-container"><div class="b-container-inner">
				<div class="b-pdps">
					<div class="b-head">Определение параметров <span>строительства</span></div>

					<div class="slider_block">
						<table id="icons" class="frame"><tbody><tr>
							<td id="icon-0">
								<img class="gray" alt="" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/0x.png">
								<img alt="Определение параметров строительства" class="color" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/0.png">
							</td>
							<td id="icon-1">
								<img class="gray" alt="" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/1x.png">
								<img alt="Разработка и согласование проекта" class="color" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/1.png">
							</td>
							<td id="icon-2">
								<img class="gray" alt="" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/2x.png"/>
								<img alt="Подготовка и подписание документов" class="color" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/2.png">
							</td>
							<td id="icon-3">
								<img class="gray" alt="" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/3x.png">
								<img alt="Подготовка и поставка материалов" class="color" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/3.png">
							</td>
							<td id="icon-4">
								<img class="gray" alt="" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/4x.png">
								<img alt="Строительство дома" class="color" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/4.png">
							</td>
							<td id="icon-5">
								<img class="gray" alt="" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/5x.png">
								<img alt="Отделочные работы и сдача дома" class="color" <img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/5.png">
							</td>
						</tr></tbody></table>
						<div id="slider"></div>
						<div id="balloons">
							<div id="balloon-wrapper">
								<div class="balloon-0 balloon balloonRight">На данном этапе наши менеджеры бесплатно рассчитают Вам предварительную смету по Вашим параметрам, с возможным последующим бесплатным выездом на место для уточнения и измерения. Для запроса сметы - <a href="" title="Контактная информация ООО ДомиК">свяжитесь с нами</a>.</div>
								<div class="balloon-1 balloon balloonRight">Если Вас не устроил типовой проект из каталога, то наша компания приступает к проектированию. После утверждения проекта мы бесплатно предоставляем Вам полную смету. Данная смета будет приложением к договору.</div>
								<div class="balloon-2 balloon balloonRight">Когда обе стороны готовы к прозрачному и открытому сотрудничеству, наша компания подготавливает для Вас полный комплект документов (договор, график платежей, перечень работ).</div>
								<div class="balloon-3 balloon balloonRight">Наша компания берет на себя всю работу по подбору и доставке материалов. Вам остается выбрать цвет и фактуру.</div>
								<div class="balloon-4 balloon balloonLeft">Наша компания выполняет строительство объекта «под ключ» или «под усадку»: от разметки участка до кровельных работ.</div>
								<div class="balloon-5 balloon balloonLeft">При применении «Сухого профилированного бруса» отделка помещения производятся сразу по окончании черновых работ. При применении других материалов отделка производится с учетом усадки, в определённой последовательности.</div>
							</div>
						</div>
					</div>

					<ul class="b-list-mob-feat">
						<li class="b-item">
							<div class="b-pic"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/0.png" alt=""></div>
							<div class="b-txt">На данном этапе наши менеджеры бесплатно рассчитают Вам предварительную смету по Вашим параметрам, с возможным последующим бесплатным выездом на место для уточнения и измерения. Для запроса сметы - <a href="" title="Контактная информация ООО ДомиК">свяжитесь с нами</a>.</div>
						</li>
						<li class="b-item">
							<div class="b-pic"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/1.png" alt=""></div>
							<div class="b-txt">Если Вас не устроил типовой проект из каталога, то наша компания приступает к проектированию. После утверждения проекта мы бесплатно предоставляем Вам полную смету. Данная смета будет приложением к договору.</div>
						</li>
						<li class="b-item">
							<div class="b-pic"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/2.png" alt=""></div>
							<div class="b-txt">Когда обе стороны готовы к прозрачному и открытому сотрудничеству, наша компания подготавливает для Вас полный комплект документов (договор, график платежей, перечень работ).</div>
						</li>
						<li class="b-item">
							<div class="b-pic"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/3.png" alt=""></div>
							<div class="b-txt">Наша компания берет на себя всю работу по подбору и доставке материалов. Вам остается выбрать цвет и фактуру.</div>
						</li>
						<li class="b-item">
							<div class="b-pic"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/4.png" alt=""></div>
							<div class="b-txt">Наша компания выполняет строительство объекта «под ключ» или «под усадку»: от разметки участка до кровельных работ.</div>
						</li>
						<li class="b-item">
							<div class="b-pic"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/5.png" alt=""></div>
							<div class="b-txt">При применении «Сухого профилированного бруса» отделка помещения производятся сразу по окончании черновых работ. При применении других материалов отделка производится с учетом усадки, в определённой последовательности.</div>
						</li>
					</ul>
	
				</div>
			</div></div><!-- /.b-container -->
		</section><!-- /.b-buildparam-section -->
<a name="calculator"></a>
		<section class="b-section-fbt">
			<div class="b-container"><div class="b-container-inner">
				<div class="b-fbt-left">
					<div class="b-pic"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/eskiz.jpg" width="380" height="220" alt=""></div>
					<div class="b-text">
						<p>Вы можете прислать нам свой эскиз, набросок, проект или ссылку на проект на другом сайте, и  наш специалист составит подробный план работ и рассчитает смету проекта Вашего дома совершенно бесплатно.</p>
											</div>
					<a class="b-btn-send" href="">Отправить проект</a>
				</div>
				<div class="b-fbt-right">
					<div class="b-fbt-calc">
						<div class="b-head"><span>Калькулятор</span> предварительной стоимости</div>
						<div class="b-text">Калькулятор поможет Вам рассчитать предварительную стоимость строительства дома из дерева.</div>
						<jdoc:include type="modules" name="main-calculator" style="none" />
					</div>
				</div>
			</div></div><!-- /.b-container -->
		</section><!-- /.b-section-fbt -->

		<section class="b-section-tech">
			<div class="b-container"><div class="b-container-inner">
			<a name="technologies"></a>
				<div class="b-tech-head"><span>Технологии </span>и гарантии</div>
				<div class="b-tech-item">
					<div class="b-text-bl">
						<div class="b-text-lt">
							<div class="b-head">Профилированный брус</div>
							<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. <a href="">Lorem Ipsum используют потому</a>, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения.</p> 
							<p>За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>
						</div>
						<div class="b-text-rt">
							<div class="b-pic"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/cd1.png" width="254" height="254" alt=""></div>
						</div>
					</div>
				</div>
				<div class="b-tech-item b-tech-item-reverse">
					<div class="b-text-bl">
						<div class="b-text-rt">
							<div class="b-pic"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/cd2.png" width="254" height="254" alt=""></div>
						</div>
						<div class="b-text-lt">
							<div class="b-head">Половая доска</div>
							<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. <a href="">Lorem Ipsum используют потому</a>, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации "Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст.." Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения.</p> 
							<p>За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>
						</div>						
					</div>
				</div>
			</div></div><!-- /.b-container -->
		</section><!-- /.b-section-tech -->
<a name="guarantees"></a>
		<section class="b-section-garant">
			<div class="b-container"><div class="b-container-inner">
			
				<div class="b-sg-head"><span>Ваша гарантия - </span>наша работа</div>
				<div class="b-sg-list">
					<ul class="b-list">
						<li class="b-item">
							<div class="b-pic"><i><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/s1.png" alt=""></i></div>
							<div class="b-txt"><span>Гарантия</span>до 5 лет</div>
							<div class="b-nlink"><a href="files/garant.docx">условия</a></div>
						</li>
						<li class="b-item">
							<div class="b-pic"><i><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/s2.png" alt=""></i></div>
							<a class="b-link" href="files/Dogovor.doc">образец<br>договора</a>
						</li>
						<li class="b-item">
							<div class="b-pic"><i><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/s3.png" alt=""></i></div>
							<a class="b-link" href="">пример<br>сметы</a>
						</li>
						<li class="b-item">
							<div class="b-pic"><i><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/s4.png" alt=""></i></div>
							<a class="b-link" href="">пример<br>проекта</a>
						</li>
						<li class="b-item">
							<div class="b-pic"><i><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/s5.png" alt=""></i></div>
							<a class="b-link" href="/i-servis.html#rekvizity">реквизиты<br>компании</a>
						</li>
						<li class="b-item">
							<div class="b-pic"><i><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/s6.png" alt=""></i></div>
							<a class="b-link" href="">карта<br>построек</a>
						</li>
					</ul>
				</div>
				<div class="b-orews">
					<div class="b-orw-head">Отзывы наших клиентов</div>
					<div class="b-wrap-orw-list">
						<ul class="b-orw-list">
							<li class="b-item">
								<div class="b-meta">Олег <i>Москва</i></div>
								<div class="b-pic"><a href="images/islide1.jpg" class="c-pop-link"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/pp1.png" width="380" height="220" alt=""></a></div>
								<div class="b-text">Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона.</div>
							</li>
							<li class="b-item">
								<div class="b-meta">Александр <i>Ульяновск</i></div>
								<div class="b-pic"><a href="images/islide1.jpg" class="c-pop-link"><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/pp1.png" width="380" height="220" alt=""></a></div>
								<div class="b-text">Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона.</div>
							</li>
						</ul>
						<div class="b-orw-all"><a href="">еще отзывы</a></div>
					</div>
				</div>
			</div></div><!-- /.b-container -->
		</section><!-- /.b-section-garant -->


		<div class="b-container"><div class="b-container-inner">
			<div class="b-last-news">
				<div class="b-title-head">Последние Новости</div>
				<ul class="b-list-last-news">
					<li class="b-item">
						<div class="b-pic"><a href=""><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/n1.png" width="297" height="116" alt=""></a></div>
						<div class="b-head"><a href="">Lorem Ipsum для распечатки образцов</a></div>
						<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.</p>
						<time datetime="2016-09-12">12 / 09 / 2013</time>
					</li>
					<li class="b-item">
						<div class="b-pic"><a href=""><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/n2.png" width="297" height="116" alt=""></a></div>
						<div class="b-head"><a href="">Lorem Ipsum для распечатки образцов</a></div>
						<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.</p>
						<time datetime="2016-09-12">12 / 09 / 2013</time>
					</li>
					<li class="b-item">
						<div class="b-pic"><a href=""><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/n3.png" width="297" height="116" alt=""></a></div>
						<div class="b-head"><a href="">Lorem Ipsum для распечатки образцов</a></div>
						<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.</p>
						<time datetime="2016-09-12">12 / 09 / 2013</time>
					</li>
				</ul>
			</div><!-- /.b-last-news -->
			<article class="b-about">
				<jdoc:include type="component" />
			</article><!-- /.b-about -->
		</div></div><!-- /.b-container -->
		<?php endif; ?>
	<?//	<!-- Конец:Если главная, то выводим -->?>
	
	<?//<!-- Если НЕ главная, то выводим -->?>
<?php if ($view!='featured') : ?>
				
				<div class="b-container"><div class="b-container-inner">
			<div class="b-sub-inner">
				<div class="b-bread"><jdoc:include type="modules" name="breadcrumb" style="none" /></div><!-- /.breadcrumb -->
                <? if ($option != 'com_bauplan'): ?>
                    <article class="b-article clearfix">
                        <jdoc:include type="component" />
                    </article><!-- /.b-article -->
                <? else: ?>
                            <jdoc:include type="component" />
                <? endif ?>
			</div>
		</div></div><!-- /.b-container -->
				
		
	<?php endif; ?>
	<?//	<!-- Конец:Если НЕ главная, то выводим -->?>


	

</div><!-- /.b-content -->

	<div class="b-sub-footer"></div>

	
</div><!-- /.b-wrap-all -->

<footer class="b-footer">
	<div class="b-container"><div class="b-container-inner">
		<div class="b-foot-logo"><a href=""><img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/foot-logo.png" width="136" height="58" alt="Домик идеальный сервис"></a></div>
		<ul class="b-foot-soc">
			<li><a class="b-link-soc-vk" href="https://vk.com/skdomik" target="_blank">&nbsp;</a></li>
			<li><a class="b-link-soc-fb" href="https://www.facebook.com/ooodomik/" target="_blank">&nbsp;</a></li>
			<li><a class="b-link-soc-od" href="https://ok.ru/skdomic" target="_blank">&nbsp;</a></li>
			<li><a class="b-link-soc-yt" href="">&nbsp;</a></li>
		</ul>
		<div class="b-develop"><a href="http://www.webpro100r.ru/" target="_blank">Создание сайта<span>Веб-простор</span></a></div>
		<div class="b-copyright">Данный сайт не является публичной офертой. Для уточнения подробной информации, просьба обращаться к нашим менеджерам. Перепечатка информации возможна только с уведомления и разрешения владельца сайта.</div>
	</div></div><!-- /.b-container -->
</footer><!-- /.b-footer -->

<?php if ($view!='featured') : ?><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script><?php endif; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/plugins.js"></script>
<script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/script.js"></script>

	<?php //echo $itemid?>



</body>
</html>
