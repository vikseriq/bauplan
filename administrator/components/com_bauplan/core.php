<?php defined('_JEXEC') or die;
// BauPlan - Core
// vikseriq # 2011-2015

define ('CORE_PATH', dirname(__FILE__));
define ('ONE_FLOOR', 1.50);

if (!defined('DS'))
	define('DS', '/');

require_once (CORE_PATH.DS.'bimages.php');

function td( $text = "" ){ echo '<td>'.$text.'</td>'; }
function tr( $title, $field, $desc = '' ) {
	$title = JText::_($title);
	if (strlen($desc))
		$title = '<abbr title="'.$desc.'">'.$title.'</abbr>';

	echo '
	<tr>
		<td class="key"><label>'.$title.'</label></td>
		<td>'.$field.'</td>
	</tr>';
}

function pre ( &$var, $hide = true ) {
	if ($hide){
		echo "\n<!--\n".print_r($var, 1)."\n-->\n";
	} else {
		echo "\n<pre>".print_r($var, 1)."</pre>\n";
	}
}

	// получаем переменную из параметров запроса
function gv( &$obj, $key, $default = 0, $method = 'post', $type = 'none', $filter = 0) {
	$obj = JRequest::getVar($key, $default, $method, $type, $filter);
}

// Храним основные перечисления и параметры
class BauCore {

	static function getObjectTypes( $id = -1 ) {
		static $x = array(
			0 => "- не указано -",
			"Дом",
			"Баня",
			"Хоз. постройка",
			"Малая форма",
			"Прочее" );
		if ($id < 0) return $x;
		else if (isset($x[$id])) return $x[$id];
			 else return '';
	}

	static function getObjectMaterials( $id = -1 ) {
		static $x = array(
			0 => "- не указано -",
			"Брус",
			"Бревно",
			"Клееный брус",
			"Блоки",
			"Панели",
			"Прочее" );
		if ($id < 0) return $x;
		else if (isset($x[$id])) return $x[$id];
			 else return '';
	}

	static function getFloors( $id = -1 ) {
		static $x = array(
			0 => "- не указано -",
			"Один",
			"Два",
			"Три",
			"Четыре" );
		if ($id < 0) return $x;
		else if (isset($x[$id])) return $x[$id];
		else return '';
	}

	static function getObjectParamKeys() {
		return array(
			0 => 'floor_0', 'floors',
			'car',
			'swim',
			'floor_b',
			'floor_out',
			'featured',
			'tables',
			'price_sale',
			'price_plan',
			'multiplier',
			'related',
			'gallery_id',
			'containment',
		);
	}

	static function getObjectSearchableParams(){
		return [
			0 => '-',
			'floor_out' => 'терраса',
			'floor_b' => 'балкон',
			'floor_0' => 'цокольный этаж',
			'car' => 'гараж',
			'swim' => 'бассейн',
			'loft' => 'второй свет'
		];
	}

	static function getCategoryParamKeys() {
		return array(
			0 => 'use',
			'type',
			'material',
			'price_from',
			'price_till',
			'area_from',
			'area_till',
			'sort_column',
			'sort_order',
			'limit'
		);
	}

	static function getBuildParamKeys() {
		return array(
			0 => 'a_title',
			'a_articles',
			'a_text',
		);
	}

	static function getPriceTables() {
		$db = JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__bp_price WHERE ext = 0');
		return $db->loadObjectList();
	}

	static function getPriceTable( $ext ){
		$db =& JFactory::getDBO();
		$db->setQuery('SELECT * FROM #__bp_price WHERE ext = '.$ext.' ORDER BY value ASC ');
		return $db->loadObjectList();
	}

	static function str2params( $string, $keys = array() ){
		if (!$keys)	$keys = BauCore::getObjectParamKeys();
		$a = array();
		foreach($keys as $x) $a[$x] = 0;
		if (!strpos($string, '='))
			return $a;
		$l = explode(';', str_replace(array('\n', '\r\n'), '', $string));
		$i = -1; while(++$i < count($l)){
			$x = explode('=', $l[$i]);
			if (!isset($x[1])) continue;	// no value
			$key = trim($x[0]); $val = trim($x[1]);
			if (in_array($key, $keys))
				$a[$key] = str_replace(array('%3B', '%3D'), array(';', '='), $val);
		}
		return $a;
	}

	static function params2str( $array, $keys = array() ){
		if (!$keys)	$keys = BauCore::getObjectParamKeys();
		$param_string = '';
		foreach($keys as $key){
			$val = (isset($array[$key]) && strlen($array[$key])) ? $array[$key] : '0';
			if (strpos($val, ';'))
				$val = str_replace(array(';', '='), array('%3B', '%3D'), $val);
			$param_string .= $key.'='.$val.";\n";
		}
		return $param_string;
	}

	static function str2array( $string, $min_child = 2 ){
		$data = array();
		if (!strlen(trim($string)))
			return $data;
		$x = explode("\n", $string);
		if(count($x)) foreach($x as $y){
			if (trim($y)){
				$d = explode(';', $y);
				while(count($d) < $min_child)
					$d[] = '';
				$data[] = $d;
			}
		}
		return $data;
	}

	/**
	 * вычисление коэффициента цены объекта
	 * @param $mul_base float   базовый коэффициент
	 * @param $floors   int     количество этажей
	 * @return float    итоговый коэфф
	 */
	static function price_mul($mul_base, $floors){
		$k = $mul_base ? $mul_base : 1;
		if ($floors == 1){
			$cp = JComponentHelper::getParams('com_bauplan');
			$k *= $cp->get('floor_one', 1);
		}
		return $k;
	}

	static function rur( $n, $post = '' ){
		return BauCore::f($n, "price", $post);
	}

		// форматирование вывода дробных чисел
	// число ; формат; постфикс; префикс
	static function f( $n = 0, $format = "area", $postfix = "", $prefix = "" ){
		$x = $prefix;
		if ($n == 0){
			return '&mdash;';
		}
		switch( $format ){
			case 'price':	$x.= number_format($n, 2, ',', ' '); break;		// цены
			case 'pricex':	$x.= number_format($n, 0, ',', ' '); break;		// цены
			case 'priceru':	$x.= number_format($n, 0, ',', ' ').' &#8381;'; break;		// цены со знаком рубля
			case 'pricerub':$x.= number_format($n, 0, ',', ' ').' <i>руб</i>'; break;		// цены с подписью
			case 'input':	$x.= number_format($n, 2, '.', '');  break;		// ввод дробей
			case 'areas':	$x.= number_format(ceil($n), 0, ',', ' ').' м<sup>2</sup>'; break;	// площадь с html
			case 'area1':	$x.= number_format(ceil($n * 10) / 10, 1, ',', ' ').' м<sup>2</sup>'; break;	// площадь с html, 0.0
			case 'area0':	$x.= number_format(ceil($n), 0, '.', ' ').' м<sup>2</sup>'; break;	// площадь без нулей [2]
			case 'area':
			default:		$x.= number_format($n, 3, ',', ' '); break;		// площадь
		}
		return $x.$postfix;
	}

	static function rus2lat( $in ){	// что ни говорите, но iconv жутко сосёт. Решаем в лоб, десу
		$map = array(
			"А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
			"Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
			"Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
			"О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
			"У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
			"Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
			"Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
			"в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
			"з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
			"м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
			"с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
			"ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
			"ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
			"ё"=>"yo","Ё"=>"YO"
		);
		return strtr($in,$map);
	}

	static function esc($q){
		return str_replace("'", '&quot;', $q);
	}

	// удаляем из текста кавычки - для использования в alt-тегах
	static function alt($text) {
		return str_replace(array('"', '\'', '>', '<'), '', $text);
	}

	// возвращаем путь к изображению вида filename_WWWHHH.ext 	|	 WWW - ширина в 16 с/с, HHH - высота
	static function img( $img, $w = 256, $h = 256 ) {
		$ext = substr($img, -3);
		$img = sprintf("%s_%03x%03x.%s", substr($img, 0, strlen($img)-4), $w, $h, $ext);
		return $img;
	}

	static function planTitle($src){
		static $titles = array(
			'2' => 'План',
			'20' => 'Цокольный этаж',
			'21' => 'Первый этаж',
			'22' => 'Второй этаж',
			'23' => 'Третий этаж',
		);
		if (preg_match('|plan_(\d+)|', $src, $match)){
			if (isset($titles[$match[1]]))
				return $titles[$match[1]];
			else return 'План';
		}
		return '';
	}

}

jimport( 'joomla.application.component.controller' );

class JControllerV extends JControllerLegacy {

	var $output = null;
	var $m 		= null;
	var $uri 	= null;
	var $ajax	= false;
	/**
	 * @var JInput
	 */
	var $input  = null;

	function __construct($d, $load_model = false){
		parent::__construct($d);
		$this->uri = 'index.php?option=com_bauplan&section='.$this->name;
		if ($load_model)
			$this->m = $this->getModel();
		$this->input = JApplicationCms::getInstance()->input;
	}

	function setTitle( $title = '', $use_postfix = true ){
		$document = JFactory::getDocument();
		if ($use_postfix) $title .= ' – '.JFactory::getConfig()->get('sitename');
		$document->setTitle($title);
	}

	function view($tmpl = "default", $vars = array(), $output = true) {
		$tmpl = JPATH_COMPONENT.DS.$this->getName().DS."view_".$tmpl.".php";
		if (file_exists($tmpl)) {
			extract($vars);
			ob_start();
			$me = $this;
			include $tmpl;
			$this->output = ob_get_contents();
			ob_end_clean();
		}
		else {
			$this->output = JError::raiseError( 500, '"' . $tmpl . '" not found' );
		}

		if ($output) {
			echo $this->output; return;
		} else {
			return $this->output;
		}
	}

	function addPathway($title, $url = null){
		$app = JFactory::getApplication();
		$pathway = $app->getPathway();
		$pathway->addItem($title, $url);
	}

	static function getURI(){
		return JFactory::getURI()->current().'?'.JFactory::getURI()->getQuery();
	}

	static function getToolbar(){
		$section = JRequest::getVar('section', 'object');
		$task = JRequest::getVar('task', '');
		include CORE_PATH.DS.'toolbar.bauplan.php';
	}

}



?>