-- Adminer 4.2.5 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `#__bp_build`;
CREATE TABLE `#__bp_build` (
  `build_id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `coord` tinytext NOT NULL,
  `icon` tinytext NOT NULL,
  `text` text NOT NULL,
  `public` tinyint(1) NOT NULL,
  `articles` tinytext NOT NULL,
  `params` text NOT NULL,
  `cdate` date NOT NULL,
  `mdate` date NOT NULL,
  `published` tinyint(1) NOT NULL,
  `ordering` smallint(6) NOT NULL,
  PRIMARY KEY (`build_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `#__bp_category`;
CREATE TABLE `#__bp_category` (
  `category_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(6) NOT NULL,
  `title` tinytext,
  `alias` tinytext NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `selector` tinytext NOT NULL,
  `ordering` tinyint(4) DEFAULT NULL,
  `published` tinyint(4) DEFAULT NULL,
  `use` tinytext NOT NULL,
  `material` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `price_from` float NOT NULL,
  `price_till` float NOT NULL,
  `area_from` float NOT NULL,
  `area_till` float NOT NULL,
  `sort_column` smallint(6) NOT NULL,
  `sort_order` smallint(6) NOT NULL,
  `limit` smallint(6) NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `#__bp_plan`;
CREATE TABLE `#__bp_plan` (
  `plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` tinytext NOT NULL,
  `alias` tinytext NOT NULL,
  `sku` tinytext NOT NULL,
  `description` text NOT NULL,
  `type` smallint(6) NOT NULL DEFAULT '0',
  `material` smallint(6) NOT NULL DEFAULT '0',
  `area` float DEFAULT '0',
  `area_live` float DEFAULT '0',
  `size_w` float DEFAULT '0',
  `size_l` float DEFAULT '0',
  `price_min` float NOT NULL DEFAULT '0',
  `prices` text NOT NULL,
  `params` text NOT NULL,
  `image_path` tinytext NOT NULL,
  `image_main` tinytext,
  `rate` tinytext NOT NULL,
  `cdate` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `mdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` smallint(6) NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `category_tab` smallint(6) NOT NULL DEFAULT '0',
  `meta_title` text NOT NULL,
  `meta_keys` text NOT NULL,
  `meta_desc` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`plan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `#__bp_price`;
CREATE TABLE `#__bp_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `value` float NOT NULL,
  `ext` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;


-- 2017-03-24 08:29:47