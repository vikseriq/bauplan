<?php
JFormHelper::loadFieldClass('list');

class JFormFieldbpcat extends JFormFieldList
{
	protected $type = 'bpcat';

	protected function getOptions()
	{
		$db    = JFactory::getDBO();
		$db->setQuery("SELECT category_id, title FROM #__bp_category WHERE parent_id = 0");
		$messages = $db->loadObjectList();
		$options  = array();

		if ($messages)
		{
			foreach ($messages as $message)
			{
				$options[] = JHtml::_('select.option', $message->category_id, $message->title);
			}
		}

		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}
}