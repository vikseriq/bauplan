<?
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('list');

class JFormFieldBauplanCat extends JFormFieldList {

	protected $type = 'BauplanCat';

	protected function getOptions() {
		$db = JFactory::getDBO();
		$q = $db->getQuery(true);
		$q->select('category_id, name');
		$q->from('#__bp_category');
		$db->setQuery((string)$q);
		$items = $db->loadObjectList();
		$options = array();

		if ($items) {
			foreach ($items as $item) {
				$options[] = JHtml::_('select.option', $item->category_id, $item->name);
			}
		}

		$options = array_merge(parent::getOptions(), $options);
print_r($options);
		return $options;
	}
}