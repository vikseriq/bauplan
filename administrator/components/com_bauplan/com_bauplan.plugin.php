<?php
/**
 * JComments plugin for com_bauplan component
 *
 * by vikseriq 		// viseriq @ gmail.com // Oct 2011
 **/

class jc_com_bauplan extends JCommentsPlugin
{
	function getTitles($ids)
	{
		$db = & JCommentsFactory::getDBO();
		$db->setQuery( 'SELECT plan_id, title FROM #__bp_paln WHERE plan_id IN (' . implode(',', $ids) . ')' );
		return $db->loadObjectList('plan_id');
	}

	function getObjectTitle($id)
	{
		$db = & JCommentsFactory::getDBO();
		$db->setQuery( 'SELECT title, plan_id FROM #__bp_plan WHERE plan_id = ' . $id );
		return $db->loadResult();
	}

	function getObjectLink($id)
	{
		$_Itemid = JCommentsPlugin::getItemid( 'com_bauplan' );
		if (JCOMMENTS_JVERSION >= '1.5') {
			$link = 'index.php?option=com_bauplan&c="object&pid='. $id;
			$link .= ($_Itemid > 0) ? ('&Itemid=' . $_Itemid) : '';
			$link = JRoute::_($link);
		}
		return $link;
	}
}
?>