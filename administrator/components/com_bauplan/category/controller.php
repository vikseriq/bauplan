<?php
	// bauPlan
	// управление категориями
	// vikseriq / 2011-08-24 / J!3 2015-02-06
class categoryController extends JControllerV {

	function __construct( $d = array() ) {
		parent::__construct( $d, true );
		$this->setTitle("Категории");
	}

	function show_list($cachable = false, $urlparams = array()) {
		$this->m->getList();
		$this->view('list', array('rows' => $this->m->data, 'page' => $this->m->getPagination()));
	}

	function edit() {
		$cid = JRequest::getVar('cid', null);
		if (isset($cid[0])) {
			$data = $this->m->getItem($cid[0]);
			if ($data) {
				$this->view('item', array('row' => $data, 'isNew' => false));
				return;
			}
		}
		$this->setRedirect(JRoute::_($this->uri, 0), "Запрошенная категория отсутствует в списке");
	}

	function add() {
		$data = new stdClass();
		$data->category_id = $data->hits = $data->published = 0;
		$data->ordering = 100;
		$data->description = "";
		$data->title = "Новая категория";
		$data->alias = "";

		$data->type = $data->material = $data->use = '';
		$data->sort_column = $data->sort_order = 0;
		$data->area_from = $data->price_from = 0;
		$data->area_till = $data->price_till = 1000;
		$data->limit = 15;
		
		$data->selector = $data->image = '';

		$this->view('item', array('row' => $data, 'isNew' => true));
	}

	function save( $apply = false ) {
		$data = new stdClass();
		$id = 0;
		gv($id, 'id');	$data->category_id = $id;

		gv($data->description,	'description', '', 'post', 'string', JREQUEST_ALLOWHTML);
		gv($data->title, 		'title', '', 'post', 'string');
		gv($data->alias, 		'alias', '', 'post', 'string');
		if (!$data->alias) $data->alias = $data->title;
		$data->alias = JFilterOutput::stringURLSafe(BauCore::rus2lat($data->alias));
		//gv($data->parent_id, 	'parent');
		$data->parent_id = 0;

		$tmp = null;
		gv($tmp,	'use',		array(), 'post', 'array');
		$data->use 		= join(',', $tmp);
		gv($tmp,	'type',		array(), 'post', 'array');
		$data->type 	= join(',', $tmp);
		gv($tmp,	'material',	array(), 'post', 'array');
		$data->material = join(',', $tmp);
		gv($tmp,	'sort_column', array(0), 'post');
		$data->sort_column = $tmp[0];
		gv($tmp,	'sort_order', array(0), 'post');
		$data->sort_order = $tmp[0];

		gv($data->published, 	'published');
		gv($data->ordering,		'ordering');
		gv($data->area_from,	'area_from', 	0, 'post');
		gv($data->area_till,	'area_till', 1000, 'post');
		gv($data->price_from,	'price_from', 	0, 'post');
		gv($data->price_till,	'price_till',1000*1000, 'post');
		gv($data->limit,		'limit', 30, 'post');
		
		gv($data->selector,		'selector', '', 'post', 'string');
		gv($data->image,		'image', '', 'post', 'string');

		$reset_hits = 0;
		gv($reset_hits, 'reset_hits', 0, 'post');
		if ($reset_hits) $data->hits = 0;

		//pre($data); die;

		if ($data->category_id > 0) {
			$this->getModel()->update($data);
		} else if ($data->category_id == 0) {
			$this->getModel()->insert($data);
		}
		if (!$apply)
			$this->setRedirect(JRoute::_($this->uri, 0), "Категория успешно сохранена");
		else
			$this->setRedirect(JRoute::_($this->uri.'&task=edit&cid[0]='.$data->category_id, 0), "Категория успешно обновлена");

	}

	function apply() {
		$this->save( true );
	}

	function cancel() {
		$this->setRedirect(JRoute::_($this->uri, 0));
	}

	function saveOrder() {
		$o = 0;
		$cid = 0;
		gv($o,		'order', 0, 'post', 'array');
		gv($cid,	'hcid',  0, 'post', 'array');
		if (!$o || !$cid) return;
		JArrayHelper::toInteger($cid);
		JArrayHelper::toInteger($o);
		$this->m->updateOrder($cid, $o);
		$this->setRedirect(JRoute::_($this->uri, 0), 'Порядок изменен');
	}

	function publish() {
		$cid = JRequest::getVar('cid', array());
		foreach($cid as $i) {
			$this->getModel()->published($i, true);
		}
		$this->show_list();
	}

	function unpublish() {
		$cid = JRequest::getVar('cid', array());
		foreach($cid as $i) {
			$this->getModel()->published($i, false);
		}
		$this->show_list();
	}

	function remove() {
		$cid = JRequest::getVar('cid', array());
		foreach($cid as $i) {
			$this->getModel()->remove($i);
		}
		$this->show_list();
	}


	// фигня всякая
	function parentSelector($row) {
		$db =& JFactory::getDBO();

		$where = "";
		if ( $row->category_id )
			$where = ' WHERE category_id != '.(int) $row->category_id;

		if (!$row->parent_id) {
			$row->parent_id = 0;
		}

		$query = 'SELECT * FROM '.$this->getModel()->table.' '.$where.' ORDER BY parent_id, category_id';
		$db->setQuery( $query );
		$mitems = $db->loadObjectList();
		$wut 	= array();
		$wut[] 	= JHTML::_('select.option',  '0', JText::_( 'Top' ) );
		foreach ($mitems as $i) {
			$wut[] = JHTML::_('select.option',  $i->category_id, '&nbsp;&nbsp;&nbsp;'. $i->title);
		}
		$output = JHTML::_('select.genericlist',   $wut, 'parent', 'class="inputbox" size="10"', 'value', 'text', $row->parent_id );

		return $output;
	}

	function parentOrder(&$rows) {
		function repeat_title($n = 0, $postfix = ""){
			$n = ($n-1)*12;
			if (!$n) return $postfix;
			$s = "";
			while ($n--) $s .= "&nbsp";
			$s .= '<sup>|_</sup>&nbsp;';
			return $s.$postfix;
		}
		function in_array_id($i, &$a) {
			foreach($a as $aa)
				if ($aa->category_id == $i) return true;
			return false;
		}
		if (!count($rows)) return;
		$l = array();
		$s = array();
		array_push($s, 0);
		while (count($s)) {
			$parent = $s[count($s)-1];

			foreach($rows as $i) {
				if ($i->parent_id == $parent and !in_array_id($i->category_id, $l)) {
					$i->title = repeat_title(count($s), $i->title);
					$l[] = $i;
					array_push($s, $i->category_id);
					break;
				}
			}

			if ($parent == $s[count($s)-1])
				array_pop($s);
		}
		/*
		while (count($s)) {
			$p = $s[count($s)-1];
			for($i = 0; $i < count($rows); $i++) {
				if ($rows[$i]->parent_id == $p) {
					array_push($s, $rows[$i]->category_id);
					$rows[$i]->title = repeat(count($s)).$rows[$i]->title;
					$l[] = $rows[$i];
				}
			}
			pre($s); pre($l); break;
			//if ($p == $s[count($s)-1])
			array_pop($s);
			array_pop($s);
		}
		*/
		$rows = $l;
	}

}

?>