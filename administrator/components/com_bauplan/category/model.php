<?php

jimport('joomla.application.component.model');

class categoryModelcategory extends JModelLegacy {

	var $data			= null;
	var $table			= null;
	var $table_xref		= null;
	var $total 			= null;
	var $_pagination	= null;
	var $limit			= null;
	var $limitstart		= null;

	function __construct () {
		parent::__construct();

		//$app = JApplication::getInstance();
		//$context = '';

		$this->table 		= "#__bp_category";
		$this->table_xref 	= "#__bp_plan_category_xref";
		
		$this->limit		= 0;//$app->getUserStateFromRequest( $context.'limit', 'limit', 200, 0 );
		$this->limitstart	= 0;//$app->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0 );
		if ($this->limit == 0) { $this->limitstart = 0; $this->limit = 200; }
	}

	function getList() {
		$q = "SELECT * FROM ".$this->table." WHERE parent_id = 0 ORDER BY ordering, category_id";
		
		$this->data = $this->_getList($q, $this->limitstart, $this->limit);
		$this->total = $this->_getListCount($q);
		
		return $this->data; 
	}

	function getPagination() {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->total, $this->limitstart, $this->limit );
		}
		return $this->_pagination;
	}
	
	function published( $cid, $state = true) {
		$state = ($state == true)? 1 : 0;
		$q = "UPDATE ".$this->table." SET published = ".$state." WHERE category_id = ".$cid." LIMIT 1";
		$this->_db->setQuery($q);
		$this->_db->query();
	}
	
	function getItem( $cid ) {
		$q = "SELECT * FROM ".$this->table." WHERE category_id = ".$cid." LIMIT 1";
		$a = $this->_getList($q);
		return (count($a)) ? $a[0] : null;
	}
	
	function update($data) {
		$this->_db->updateObject($this->table, $data, "category_id");
	}
	
	function insert($data) {
		unset($data->category_id);
		$this->_db->insertObject($this->table, $data, "category_id");
	}
	
	function remove($id) {
		$this->_db->setQuery("DELETE FROM ".$this->table." WHERE category_id = $id LIMIT 1;");
		$this->_db->query();
	}
	
	function updateOrder( $ids, $pos ) {
		foreach ($ids as $k => $i) {
			$this->_db->setQuery("UPDATE ".$this->table." SET ordering = ".$pos[$k]." WHERE category_id = ".$i);
			$this->_db->query();
		}
	}
	
	function getSortColumns(){
		$x = array(
			0 => 'по индексу',
			'по площади',
			'по названию',
			'по минимальной цене',
			'по дате создания',
			'по дате изменения',
			'по рейтингу',
			'по кол-ву просмотров',
			'по пьяни'
		);
		return $x;
	}
	
	function getSortOrders(){
		$x = array(
			0 => 'по убыванию',
			1 => 'по возрастанию'
		);
		return $x;
	}

}

?>