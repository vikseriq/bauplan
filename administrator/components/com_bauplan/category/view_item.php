<?php

function selector ( $name, $selected = 0, $values = array(), $multiple = false, $sbox = false ) {
	if ($name == 'type')
		$values = BauCore::getObjectTypes();
	else if ($name == 'material')
		$values = BauCore::getObjectMaterials();
	if (!count($values)) return '';
	
	$name.= '[]';
	$html = '';
	
	if (!$sbox){		// SELECT BOX
		if ($multiple) 
			$html = '<select multiple size="'.count($values).'" name="'.$name.'[]">';
		else
			$html = '<select name="'.$name.'">';
		foreach($values as $i => $v) {
			if (!$multiple && $i == $selected)
				$html.= '<option value="'.$i.'" selected="yes">'.$v.'</option>';
			else
				$html.= '<option value="'.$i.'">'.$v.'</option>';
		}
		$html.= '</select>';
	} else {		//  [CHECK | RADIO] BUTTONS
		unset($values[0]);
		if ($multiple) {
			$s = explode(',', $selected);
			foreach($values as $i => $v) {
				$x = (in_array($i, $s))? 'checked="checked"' : '';
				$html.= '<span><input type="checkbox" name="'.$name.'" value='.$i.' '.$x.'>'.$v.'</span>';
			}
		} else {
			foreach($values as $i => $v) {
				$x = ($i==$selected)? 'checked="checked"' : '';
				$html.= '<span><input type="radio" name="'.$name.'" value='.$i.' '.$x.'>'.$v.'</span>';
			}		
		}
	}
	return $html;
}

function input_text ( $key, $value, $etc = ' maxlength="300"') {
	return sprintf('<input class="text_in" type="text" name="%s" id="%s" value="%s" %s />', $key, $key, $value, $etc);
}

	$uri = JControllerV::getURI();
	JRequest::setVar( 'hidemainmenu', 1 );

	$editor = JFactory::getEditor();

	JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES, 'description' );

	JControllerV::getToolbar();

	JToolBarHelper::title( $row->title, 'bp-folder' );
	
	$query = 'SELECT ordering AS value, title AS text FROM '.$this->m->table;

	?>
	<script language="javascript" type="text/javascript">
	function submitbutton(pressbutton, section) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			Joomla.submitform( pressbutton );
			return;
		}

		if ( pressbutton == 'menulink' ) {
			if ( form.menuselect.value == "" ) {
				alert( "<?php echo JText::_( 'Please select a Menu', true ); ?>" );
				return;
			} else if ( form.link_type.value == "" ) {
				alert( "<?php echo JText::_( 'Please select a menu type', true ); ?>" );
				return;
			} else if ( form.link_name.value == "" ) {
				alert( "<?php echo JText::_( 'Please enter a Name for this menu item', true ); ?>" );
				return;
			}
		}

		if ( form.title.value == "" ) {
			alert("<?php echo JText::_( 'Category must have a title', true ); ?>");
		} else {
			<?php
			echo $editor->save( 'description' ) ; ?>
			Joomla.submitform(pressbutton);
		}
	}
	</script>

	<form action="<?php echo $uri ?>" method="post" name="adminForm" id="adminForm">
	<table style="width:100%"><tr><td style="width:45%" valign="top">
		<fieldset class="adminform">
			<legend><?php echo JText::_( 'Основное' ); ?></legend>
			<table class="admintable">
			<tr>
				<td class="key"><label for="title" width="100"><?php echo JText::_( 'Заголовок' ); ?>:</label></td>
				<td>
					<input class="text_area" type="text" name="title" id="title" value="<?php echo $row->title; ?>" 
						size="100" maxlength="100" title="" />
				</td>
			</tr>
			<tr>
				<td class="key"><label for="title" width="100"><?php echo JText::_( 'Псевдоним' ); ?>:</label></td>
				<td>
					<input class="text_area" type="text" name="alias" id="alias" value="<?php echo $row->alias; ?>" 
						size="50" maxlength="50" title="Псевдоним, который используется в URL-адресе" />
				</td>
			</tr>
			<tr>
				<td class="key">
					<?php echo JText::_( 'Опубликовано' ); ?>:
				</td>
				<td>
					<?php echo JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $row->published ); ?>
				</td>
			</tr>
			<tr>
				<td class="key">Сортировка</td>
				<td><input class="text_area" type="text" name="ordering" value="<?php echo $row->ordering; ?>" size="4"/></td>
			</tr>
			<tr>
				<td class="key"><label for="viewed"><?php echo JText::_( 'Просмотров' ); ?>:</label></td>
				<td><?php echo $row->hits; ?> <span><input type="checkbox" name="reset_hits" value="1">сброс</span></td>
			</tr>
			<tr>
				<td class="key">Изображение категории:<br><small>ID объекта или путь</small></td>
				<td><input class="text_area" type="text" name="image" id="image" value="<?php echo $row->image; ?>"
						size="10" title="<?php echo JText::_( 'Основное изображение для категории. Введите id объекта - его изображение будет использоваться. Либо полный путь к изображению' ); ?>" />
				</td>
			</tr>
			<tr>
				<td class="key">Список площадей:</td>
				<td><textarea class="text_area" rows="6" cols="30" name="selector"
					title="<?php echo JText::_( 'Список выбора площади, на главной. Формат: <имя>=<от>-<до>; ' ); ?>"><?php 
						echo trim($row->selector); 
					?></textarea>
				</td>
			</tr>
			</table>
		</fieldset>
	</td><td style="width:50%" valign="top">
	<fieldset class="adminform"><legend>Фильтры</legend>
		<p id="desc">Тут вы можете задать порядок обработки данных категорий: 
		условия, по которым выбираются объекты, а также способ их вывода.<br>
		Для применения того или иного параметра поставьте галочку около его названия
		</p>
		<!-- Query filters -->
		<h3>Выбирать объекты, где:</h3>
<?php
function div_where( $title = '', $content = '', $name = '', $used = ''){
	$html = '<div class="where">';
	if (strlen($name)) {
		$html.= '<input type="checkbox" name="use[]" value="'.$name.'" ';
		if(strpos($used,$name)!==false)	$html.='checked="checked"';
		$html.= ' />';
		$html.= $title.': <br/>'.$content;
	} else {
		$html.= $title.' '.$content;
	}
	$html.= "</div>\n";
	echo $html;
}
?>
		<?php
			div_where('Тип постройки равен', 
				selector('type', $row->type, 0, 1, 1), "type", $row->use);
				
			div_where('Вид материала равен', 
				selector('material', $row->material, 0, 1, 1), "material", $row->use);
				
			div_where('Общая площадь постройки', 
				'от '.input_text('area_from', $row->area_from).' до '.input_text('area_till', $row->area_till).' метров квадратных', 
				"area", $row->use);
				
			div_where('Минимальная цена', 
				'от '.input_text('price_from', $row->price_from).' до '.input_text('price_till', $row->price_till).' рублей', 
				"price", $row->use);			
		?>
		
		
		<h3>Выводить результаты следующим образом:</h3>
		<?php
			div_where('', 'Сортировка '.selector('sort_column', $row->sort_column, $me->m->getSortColumns()).', '.
					'направление '.selector('sort_order', $row->sort_order, $me->m->getSortOrders()).' ');
			div_where('', 'Выводить по '.input_text('limit', $row->limit).' элементов на страницу');
		?>
		<!-- ~ Query filters -->
	</fieldset>
	</td></tr>
	<tr><td colspan="2">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'Описание' ); ?></legend>
		<table class="admintable"><tr><td valign="top">
		<?php
		// parameters : areaname, content, width, height, cols, rows, show xtd buttons
		echo $editor->display( 'description',  htmlspecialchars($row->description, ENT_QUOTES),
		'800', '400', '120', '40', array('pagebreak', 'readmore') ) ;
		?>
		</td></tr></table>
	</fieldset>
	</td></tr>
	</table>

	<div class="clr"></div>

	<input type="hidden" name="option" value="com_bauplan" />
	<input type="hidden" name="section" value="category" />
	<input type="hidden" name="id" value="<?php echo $row->category_id; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
	</form>
<style>

	#desc {
		color: lightSlateGray;
		font-family: Verdana;
		font-size: 12px;
		font-style: italic;
	}
	.where {
		border-bottom: 1px dashed #AAAAAA;
		display: block;
		float: left;
		font-size: 14px;
		padding: 4px 2px;
		position: relative;
		width: 100%;
	}
	.where span {
		display: block;
		float: left;
		padding: 6px;
	}
	.where input {
		margin: 3px !important;
		width: 15px !important;
	}
	.where select {
		margin: 3px !important;
		font-size: 14px !important;
	}
	.text_in {
		display: inline !important;
		font-size: 14px !important;
		width: 55px !important;
	}
</style>