<?php 
	JHTML::_('behavior.tooltip');
	$uri = JControllerV::getURI();
	//$this->parentOrder($rows);
	JControllerV::getToolbar();
?>
<form action="<?php echo $uri ?>" method="post" name="adminForm" id="adminForm">
<? if (count($rows)): ?>
	<table class="table table-striped" id="articleList">
	<thead>
		<tr>
			<th width="1%" nowrap="nowrap"><input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" /></th>
			<th width="1%" nowrap="nowrap"><?php echo JText::_( 'ID' ); 		?></th>
			<th							  ><?php echo JText::_('Название');?></th>
			<th width="1%" nowrap="nowrap"><?php echo JText::_('Опубликовано'); 	?></th>
			<th width="1%" nowrap="nowrap"><?php echo JText::_('Просмотров'); 		?></th>
		</tr>
	</thead>
	<tfoot>
		<tr><td colspan="7"><?php echo $page->getListFooter(); ?></td></tr>
	</tfoot>
	<tbody>
	<?php
	for ($i = 0, $n = count($rows); $i < $n; $i++) {
		$row = &$rows[$i];
		
		JFilterOutput::objectHtmlSafe($row, ENT_QUOTES, "title");
		$row->checked_out = false;
		$row->id 	= $row->category_id;
		$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$published 	= JHTML::_('grid.published', 	$row, $i );
		?>
		<tr class="<?php echo "row".($i % 2); ?>">
			<td>
				<?php echo $checked; ?>
			</td>
			<td>
				<?php echo $row->id; ?>
			</td>
			<td>
				<span class="editlinktip hasTip" title="<?php echo JText::_( 'Title' )."::".$row->title."<br>Alias: ".$row->alias; ?>">
				<?php
					$link = $uri."&task=edit&cid[0]=".$row->id;
					printf('<a href="%s">%s</a>', JRoute::_($link), $row->title);
				?>
				</span>
			</td>
			<td align="center">
				<?php echo $published;?>
			</td>
			<td>
				<?php echo $row->hits; ?>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
	</table>
<? else: ?>
	<div class="alert alert-no-items">
		<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
	</div>
<? endif ?>
	<input type="hidden" name="option" value="com_bauplan" />
	<input type="hidden" name="section" value="category" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="chosen" value="" />
	<input type="hidden" name="act" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
	