<?php
	// bauPlan
	// управление постройками на карте
	// vikseriq / 2013-08-11 / J!3 2015-02-06

class buildController extends JControllerV {

	function __construct( $d = array() ) {
		parent::__construct( $d );
		$this->setTitle("Постройки");
		$this->m = $this->getModel();
		$this->cp = JComponentHelper::getParams('com_bauplan');
	}

	var $cp;

		// вывод списка объектов
	function show_list() {
		gv($this->lists['order'], 		'filter_order',		'plan_id');
		gv($this->lists['order_Dir'],	'filter_order_Dir', 'desc');
		$this->m->getList($this->lists);

		$this->view('list', array('rows' => $this->m->data, 'page' => $this->m->getPagination()));
	}

		// редактирование существующего объекта
	function edit( $build_id = 0 ) {
			// если build_id не передан, то берем из запроса
		if ($build_id == 0) {
			$build_id = JRequest::getVar('cid', null);
			if(isset($build_id[0])) $build_id = $build_id[0];
		}
		$data = $this->getModel()->getItem($build_id);
		if (!$data){
			$this->setRedirect(JRoute::_('index.php?option=com_bauplan&section=build', 0), "Объект с таким индексом не найден");
			return;
		}
		$data->p = BauCore::str2params($data->params, BauCore::getBuildParamKeys());
		$this->setTitle($data->title);
		$this->view('item', array(
			'row' => $data,
			'plans' => $this->getPlans(),
			'isNew' => false)
		);
	}

		// добавление нового объекта - обнуляем переменные
	function add() {
		$data = new stdClass();
		$data->build_id = $data->plan_id = $data->published = $data->public = 0;
		$data->text = "";
		$data->icon = "islands#brownStretchyIcon";
		$data->articles = "";
		$data->coord = "55.992013,37.213803";
		$data->title = "Новое строение";
		$data->p = BauCore::str2params("", BauCore::getBuildParamKeys());
		$this->view('item', array(
			'row' => $data,
			'plans' => $this->getPlans(),
			'isNew' => true)
		);
	
	}

		// сохранение объекта
	function save( $show_again = false ) {
			// обрабатываем переданные параметры формы
		$id = 0;
		gv($id, 'id');
		$data = new stdClass();
		$data->build_id	= $id;
		gv($data->plan_id, 		'plan_id');
		gv($data->public, 		'public');
		gv($data->text,			'text', '', 'post', 'string', JREQUEST_ALLOWHTML);
		gv($data->title, 		'title', '', 'post', 'string');
		gv($data->articles, 	'articles', '', 'post', 'string');
		gv($data->coord, 		'coord', '', 'post', 'string');
		gv($data->icon, 		'icon', '', 'post', 'string');
		gv($data->published, 	'published');
		$params = null;
		gv($params,				'params', array(), 'post', 'array');

		$data->icon = str_replace(array('\'', '"'), '', $data->icon);
		$data->params = BauCore::params2str($params, BauCore::getBuildParamKeys());
		$model = $this->getModel();

		if ($id > 0) {								// обновление существующих + установка даты
			$data->mdate	=	date("Y-m-d H:i:s", time()+11);
			$model->update($data);
		} else if ($id == 0) {						// или добавление новых с получением индекса
			$data->cdate	=	date("Y-m-d H:i:s", time()+11);
			$data->mdate	=	"0000-00-00 00:00:00";
			$id = $model->insert($data);
		}
		
			// обработка "сохранить\применить"
		if (!$show_again)
			$link = JRoute::_('index.php?option=com_bauplan&section=build', 0);
		else
			$link = JRoute::_('index.php?option=com_bauplan&section=build&task=edit&cid[0]='.$id, 0);

		$msg = 'Объект успешно сохранен';
		$this->setRedirect($link, $msg);
		$this->redirect();
	}
		// обрабатываем так же, как и сохранение, и возвращаемся на страницу объекта
	function apply() {
		$this->save(true);
	}
	
	function cancel(){
		$this->setRedirect(JRoute::_('index.php?option=com_bauplan&section=build', 0));
		$this->redirect();
	}

		// изменение статуса - публикация
	function publish() {
		$cid = JRequest::getVar('cid', array());
		foreach($cid as $i) {
			$this->getModel()->published($i, true);
		}
		$this->show_list();
	}
		// изменение статуса - скрытие
	function unpublish() {
		$cid = JRequest::getVar('cid', array());
		foreach($cid as $i) {
			$this->getModel()->published($i, false);
		}
		$this->show_list();
	}
		// удаление объекта
	function remove() {
		$cid = JRequest::getVar('cid', array());
		foreach($cid as $i) {
			$this->getModel()->remove($i);
		}
		$this->show_list();
	}

	function getPlans(){
		include_once __DIR__.'/../object/model.php';
		$model = new objectModelobject();
		$model->getList(['order' => 'plan_id', 'order_Dir' => 'desc']);
		$data = [
			0 => '- не выбран -'
		];
		foreach($model->data as $item){
			$data[$item->plan_id] = $item->title;
		}
		return $data;
	}
}

?>