<?php

jimport('joomla.application.component.model');

class buildModelbuild extends JModelLegacy {

	var $data			= null;
	var $table			= null;
	var $table_xref		= null;
	var $total 			= null;
	var $_pagination	= null;
	var $limit			= null;
	var $limitstart		= null;

	function __construct () {
		//global $mainframe, $context;

		parent::__construct();

		$this->table = "#__bp_build";
		
		$this->limit		= 0;//$mainframe->getUserStateFromRequest( $context.'limit', 'limit', $mainframe->getCfg('list_limit'), 0 );
		$this->limitstart	= 0;//$mainframe->getUserStateFromRequest( $context.'limitstart', 'limitstart', 0 );
		if ($this->limit == 0) $this->limitstart = 0;
	}

	function getList( $order ) {
		$q = "SELECT * FROM ".$this->table." ";
		$f_order = $order['order']; $f_order_dir = $order['order_Dir'];
		if ($f_order)
			$q.= BauCore::esc(' ORDER BY '.$f_order.' '.$f_order_dir.' ');
		
		$this->data = $this->_getList($q, $this->limitstart, $this->limit);
		
		$this->total = $this->_getListCount($q);
	}
	
	function getPagination() {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->total, $this->limitstart, $this->limit );
		}
		return $this->_pagination;
	}
	
	function published( $cid, $state = true) {
		$state = ($state == true)? 1 : 0;
		$q = "UPDATE ".$this->table." SET published = ".$state." WHERE build_id = ".$cid." LIMIT 1";
		$this->_db->setQuery($q);
		$this->_db->query();
	}
	
	function getItem( $cid ) {
		$this->_db->setQuery("SELECT * FROM ".$this->table." WHERE build_id = ".$cid." LIMIT 1;");
		return $this->_db->loadObject();
	}
	
	function update($data) {
		$this->_db->updateObject($this->table, $data, "build_id");
	}
	
	function insert($data) {
		$this->_db->insertObject($this->table, $data, "build_id");
		return $data->build_id;
	}
	
	function remove($id) {
		$this->_db->setQuery("DELETE FROM ".$this->table." WHERE build_id = $id LIMIT 1;");
		$this->_db->query();
	}
	
	// обновление полей, переданных в массиве
	function updateFields ($id, $info) {
		if (!count($info)) return;
		$q = "UPDATE ".$this->table." SET ";
		foreach($info as $field => $value) {
			$q.= "`$field` = '$value',";
		}
		$q[strlen($q)-1]= " ";	// последнюю запятую на точку с запятой
		$q.= " WHERE build_id = $id LIMIT 1;";
		$this->_db->setQuery($q);
		echo $q;
		$this->_db->query();
	}
	
}
?>