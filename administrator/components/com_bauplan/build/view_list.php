<?php
JControllerV::getToolbar();
JHTML::_('behavior.tooltip');
$uri = JControllerV::getURI();

$types = BauCore::getObjectTypes();
$_ord = &$this->lists['order_Dir'];
$_orr = &$this->lists['order'];
?>
<form action="<?php echo $uri ?>" method="post" name="adminForm" id="adminForm">
	<? if (count($rows)): ?>
		<table class="adminlist table table-striped">
			<thead>
			<tr>
				<th width="1%" nowrap="nowrap">
					<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);"/>
				</th>
				<th width="3%" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'ID', 'build_id', $_ord, $_orr); ?></th>
				<th><?php echo JHTML::_('grid.sort', 'Название', 'title', $_ord, $_orr); ?></th>
				<th width="1%" nowrap="nowrap"><?php echo JText::_('Опубликовано'); ?></th>
				<th width="1%" nowrap="nowrap">Доступность</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="5"><?php echo $page->getListFooter(); ?></td>
			</tr>
			</tfoot>
			<tbody>
			<?php
			for ($i = 0, $n = count($rows); $i < $n; $i++):
				$row = &$rows[$i];

				JFilterOutput::objectHtmlSafe($row, ENT_QUOTES);
				$row->checked_out = false;
				$row->ordering = true;
				$row->id = $row->build_id;
				$checked = JHTML::_('grid.checkedout', $row, $i);
				$published = JHTML::_('grid.published', $row, $i);
				?>
				<tr class="<?php echo "row".($i % 2); ?>">
					<td>
						<?php echo $checked; ?>
					</td>
					<td>
						<?php echo $row->id; ?>
					</td>
					<td>
				<span class="editlinktip hasTip" title="<?php echo JText::_('Название')."::".$row->title; ?>">
				<?php
				$link = "index.php?option=com_bauplan&section=build&task=edit&cid[0]=".$row->id;
				printf('<a href="%s">%s</a>', JRoute::_($link), $row->title);
				?>
				</span>
					</td>
					<td align="center">
						<?php echo $published; ?>
					</td>
					<td align="center">
						<?
						echo ($row->public) ? 'Общий доступ' : 'Приватный';
						$p = BauCore::str2params($row->params, BauCore::getBuildParamKeys());
						$access = array();
						if ($p['a_title']) $access[] = 'название';
						if ($p['a_articles']) $access[] = 'статьи';
						if ($p['a_text']) $access[] = 'описание';
						if ($access){
							echo '<br/>Поля: '.implode(', ', $access);
						}
						?>
					</td>
				</tr>
			<? endfor ?>
			</tbody>
		</table>
	<? else: ?>
		<div class="alert alert-no-items">
			<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
		</div>
	<? endif ?>

	<input type="hidden" name="option" value="com_bauplan"/>
	<input type="hidden" name="section" value="build"/>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>"/>
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>"/>
	<?php echo JHTML::_('form.token'); ?>
</form>
