<?php // bauplan .11 - .15 // vik

$d = JFactory::getDocument();
$d->addScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU");

function input_text($key, $value, $etc = ' maxlength="300"'){
	return sprintf('<input class="text_in" type="text" name="%s" id="%s" value="%s" %s />', $key, $key, $value, $etc);
}

function check_param($array, $key, $title = null){
	$a = '';
	if ($title)
		$a .= '<label>';
	$a .= sprintf('<input type="checkbox" class="checkbox" name="params[%s]" id="%s" value="1" %s />',
		$key, $key, (isset($array[$key]) && $array[$key]) ? 'checked' : '');
	if ($title)
		$a .= ' '.$title.'</label>';
	return $a;
}

// Вывод строки таблицы. Заголовок, значения, описание
function tt($title, $field, $desc = ''){
	$title = JText::_($title);
	if (strlen($desc))
		$title = '<abbr title="'.$desc.'">'.$title.'</abbr>';

	echo '
	<div class="control-group">
		<div class="control-label key"><label>'.$title.'</label></div>
		<div class="controls">'.$field.'</div>
	</div>';
}

function selector($name, $selected = 0, $values = array()){
	$html = '<select name="'.$name.'">';
	foreach ($values as $i => $v){
		if ($i == $selected)
			$html .= '<option value="'.$i.'" selected="yes">'.$v.'</option>';
		else
			$html .= '<option value="'.$i.'">'.$v.'</option>';
	}
	$html .= '</select>';
	return $html;
}

JControllerV::getToolbar();
JToolBarHelper::title($row->title, 'bp-buildings');

$uri = JControllerV::getURI();
$editor = JFactory::getEditor();
JFilterOutput::objectHTMLSafe($row, ENT_QUOTES, 'text');

define('SITE_URL', str_replace('/administrator/', '', JUri::base()));

$icons = array(
	'islands#darkGreenStretchyIcon' => 'зеленая',
	'islands#blueStretchyIcon' => 'синяя',
	'islands#darkOrangeStretchyIcon' => 'оранжевая',
	'islands#redStretchyIcon' => 'красная',
	'islands#yellowStretchyIcon' => 'желтая'
);

?>
<style type="text/css">
	table.admintable td.key {
		font-size: 12px;
	}

	.text_in {
		font-size: 16px;
	}

	input#title {
		width: 350px;
	}

	.params .text_in {
		width: 120px;
	}

	select {
		font-size: 12px;
	}
</style>

<form action="<?php echo $uri ?>" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
	<fieldset class="adminform row-fluid form-horizontal">
		<div class="span6">
			<legend>Параметры постройки</legend>
			<?php

			tt('Номер постройки', $row->build_id, "Порядковый номер в базе данных");
			tt('Название', input_text('title', $row->title));
			tt('Опубликовано', JHTML::_('select.booleanlist', 'published', 'class="inputbox"', $row->published));
			tt('Объект', selector('plan_id', $row->plan_id, $plans), "");
			//tt('Объект', input_text('plan_id', $row->plan_id, 'style="width: 200px;"'), "");
			tt('Галерея строительства', input_text('articles', $row->articles, 'style="width: 200px;"'), "Ссылка на галерею");
			/*tt('<a target="_blank" href="https://tech.yandex.ru/maps/doc/jsapi/2.1/ref/reference/option.presetStorage-docpage/">Метка</a> на карте',
				input_text('icon', $row->icon, 'style="width: 200px;"'), "");*/
			tt('Цвет метки на карте', selector('icon', $row->icon, $icons), "");

			tt('Доступно всем', JHTML::_('select.booleanlist', 'public', 'class="inputbox"', $row->public), "Управление доступом: полная информация всем или только менеджерам");
			tt('Общедоступные поля',
				check_param($row->p, 'a_title', 'Заголовок')
				.check_param($row->p, 'a_articles', 'Ссылки на статьи')
				.check_param($row->p, 'a_text', 'Описание')
			);
			?>
		</div>
		<div class="span6">
			<legend>Координаты</legend>
			<?php
			tt('GPS-координаты', input_text('coord', $row->coord), "Координаты на карте: long,lat");
			tt('Карта', '<div id="map" style="width: 350px; height: 350px"></div>');
			?>
		</div>
	</fieldset>
	<fieldset class="adminform row-fluid form-horizontal">
		<div class="span12">
			<legend>Описание</legend>
			<?php
			tt('Описание', $editor->display(
				'text', htmlspecialchars($row->text, ENT_QUOTES),
				'400', '400', '80', '40', array('pagebreak', 'readmore'))
			);
			?>
		</div>
	</fieldset>

	<input type="hidden" name="option" value="com_bauplan"/>
	<input type="hidden" name="id" value="<?php echo $row->build_id; ?>"/>
	<input type="hidden" name="task" value=""/>
	<?php echo JHTML::_('form.token'); ?>
</form>
<script>
	var map;
	var point;

	ymaps.ready(function(){
		map = new ymaps.Map("map", {center: [<?=$row->coord?>], zoom: 11});
		map.events.add('click', function(e){
			var coords = e.get('coords');
			point.geometry.setCoordinates(coords);
			document.getElementById('coord').value = [coords[0].toPrecision(6), coords[1].toPrecision(6)].join(',');
		});

		point = new ymaps.Placemark([<?=$row->coord?>], {iconContent: '<?=$row->title?>'}, {preset: '<?=$row->icon?>'});
		map.geoObjects.add(point);

	});
</script>