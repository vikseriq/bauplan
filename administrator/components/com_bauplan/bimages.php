<?php
	// BauPlan
	// Класс для работы с изображениями

function real_unix_path( $path ) {
	return str_replace(array("\\", "/"), "/", realpath($path));
}

	// размеры изображений
define ('MAX_IMAGE_WIDTH',	1280);
define ('MAX_IMAGE_HEIGHT',	1024);
define ('IMAGE_QUALITY',	90);
	
	// пути
define ('MEDIA_DIR', "/images/projects/");
define ('ROOT_PATH',  real_unix_path(str_replace("administrator", "", JPATH_BASE)));
if (!is_dir(ROOT_PATH.MEDIA_DIR))
	{ mkdir(ROOT_PATH.MEDIA_DIR); echo "New image path created: ".ROOT_PATH.MEDIA_DIR; }	// в случае измнения настроек
define ('MEDIA_PATH', real_unix_path(ROOT_PATH.MEDIA_DIR)."/");

class bImages {

		// возвращает список разрешенных форматов
	function allowed_exts() {
		return array('jpg', 'png', 'gif');
	}

		// обработка загруженного изображения
		// id, экстра: 0 - только обработка загрузки, 1 - изображение объекта, 2 - план объекта, 3 - изображение категории
		// возвращает в успешном случае: path - папку изображения и image - путь к изображению
	function process_image ( $id, $extra = 1, $field = 'image_upload' ) {
		$info = array();

		if (!isset($_FILES[$field])) return false;

		$path = MEDIA_PATH.'pr'.$id.'/';

		if (!is_dir($path)) {		// если существует
			if (mkdir($path)) {		// то создаем
				chmod ($path, 0766);// и ставим права
			} else {
				$path = MEDIA_PATH;	// или выдаем корневую
			}
		}
		$path = str_replace("\\", "/", $path);
		$info['path'] = str_replace(array(ROOT_PATH, '//'), "/", $path);
		
		$tmp = $_FILES[$field]['tmp_name'];		
		if (is_uploaded_file($tmp)) {	// если загружен файл
			$inj = '';
			if ($extra[0] == 2) $inj .= "plan_".$extra.'_'; 	// для планировок - префикс

			$path = $path.$inj.$_FILES[$field]['name'];

			$a = move_uploaded_file($tmp, $path);
			if ($a == true) {
				$a = '';
				$a = bImages::resize_image($path, MAX_IMAGE_WIDTH, MAX_IMAGE_HEIGHT, true, true);
				$info['image'] = (strlen($a)) ? $a : $path;
			}
		}

		$info['image'] = str_replace(array(ROOT_PATH, '//'), "/", $info['image']);
		return $info;
	}
	
		// изменение размера изображения
		// @params относительный путь к изображению, ширина, высота, сохранение пропорций, заменять оригинал, принудительное изменение
		// @return путь к итоговому изображению
		// при сохранении пропорций размер подгоняется под большее из переданных значений
	function resize_image ( $rpath, $rx = 0, $ry = 0, $save_prop = true, $rewrite = false, $must = false ) {
		$path = str_replace(ROOT_PATH, "", $rpath);
		$path = ROOT_PATH.$path;	// приводим к относительному пути
		
		//die($path);
		if (!file_exists($path)) 
			return $rpath;

		$size = getimagesize($path);
		if ($size === false)
			return $rpath;

		if (($size[0] > $rx || $size[1] > $ry) || $must) {	// обрабатываем бОльшие размеры и случай принудительного увеличения
		
			$ext = strtolower(substr($path, -3));
			if ($ext == 'peg') $ext = 'jpg';
			if (!in_array($ext, bImages::allowed_exts()))
				return $rpath;
		
			$icfunc = "imagecreatefrom".$ext;
			if (!function_exists($icfunc))	
				return $rpath;			
			
			$new_x = $rx;	$new_y = $ry;
			if ($save_prop) {	// сохраняем пропорции по бОльшей стороне
				$k_x = $rx / $size[0];
				$k_y = $ry / $size[1];
				if ($k_x <= $k_y) {
					$new_x = $rx;
					$new_y = floor($size[1] * $k_x);
				} else {
					$new_x = floor($size[0] * $k_y);
					$new_y = $ry;				
				}
			}			
			//printf("Now: %d %d\nNeed: %d %d\nUse: %d %d\n",$size[0], $size[1], $rx, $ry, $new_x, $new_y); die;

			$isrc = $icfunc($path);
			$idest = imagecreatetruecolor($new_x, $new_y);
			imagecopyresampled($idest, $isrc, 0, 0, 0, 0, $new_x, $new_y, $size[0], $size[1]);

			if ($rewrite == true)
				 $dest = $path;
			else $dest = str_replace($ext, $new_x.'_'.$new_y.'.jpg', $path);
			imagejpeg($idest, $dest, IMAGE_QUALITY);

			imagedestroy($isrc);
			imagedestroy($idest);
			return str_replace(array(ROOT_PATH, '//'), "/", $dest);
		} else
			return $rpath;
	}
	
		// возвращает список изображений в указанной директории
	static function images_in_dir ( $dir ) {
		if (!strlen($dir)) return;
		$dir2 = ROOT_PATH.$dir;
		if (!is_dir($dir2)) {
			if (!is_dir($dir))
				return;
		} else $dir = $dir2;
		
		$x = scandir($dir);
		unset($x[1], $x[0]);
		//pre($x);
		//array_shift($x); array_shift($x);
		return $x;
		/*	// Альтернатовно - с фильтром
		$r = array();
		$exts = bImages::allowed_exts();
		foreach($x as $i) {
			if (strlen($i) <= 4) continue;
			if (in_array(strtolower(substr($i, -3)), $exts))
				$r[] = $i;
		}
		return $r;
		*/
		
	}

}

?>