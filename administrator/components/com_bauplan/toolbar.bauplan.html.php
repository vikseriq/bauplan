<?php

class TOOLBAR_bau
{
	static function categories()
	{
		JToolBarHelper::title(JText::_('Категории'), 'bp-folder');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		//JToolBarHelper::apply("saveOrder", "Save Order");
		JToolBarHelper::spacer();
		JToolBarHelper::addNew();
		JToolBarHelper::editList();
		JToolBarHelper::deleteList();
	}

	static function element($is_new)
	{
		JToolBarHelper::apply();
		JToolBarHelper::save();
		if ($is_new) {
			JToolBarHelper::cancel();
		} else {
			JToolBarHelper::cancel('cancel', 'Закрыть');
		}
	}

	static function objects()
	{
		JToolBarHelper::title(JText::_('Объекты'), 'bp-card');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		//JToolBarHelper::apply("saveOrder", "Save Order");
		JToolBarHelper::spacer();
		JToolBarHelper::addNew();
		JToolBarHelper::editList();
		JToolBarHelper::deleteList();
		JToolBarHelper::preferences('com_bauplan', 600, 500);
	}

	static function prices()
	{
		JToolBarHelper::title(JText::_('Ценовые таблицы'), 'bp-calculator');
		JToolBarHelper::addNew();
		JToolBarHelper::editList();
		JToolBarHelper::deleteList();
		JToolBarHelper::preferences('com_bauplan', 600, 500);
	}

}