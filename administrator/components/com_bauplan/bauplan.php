<?php
if (!defined('DS')) define('DS', '/');
require_once (JPATH_COMPONENT.DS.'core.php');

$section = JRequest::getVar('section', 'object');

/* Hierarhy:
	component
		section1
			controller
			model
			view_1
			view_2
		section2
			controller
			model
	etc.
*/
require_once (JPATH_COMPONENT.DS.$section.DS.'controller.php');
require_once (JPATH_COMPONENT.DS.$section.DS.'model.php');

JHTML::stylesheet('administrator/components/com_bauplan/assets/com_bauplan.css');

$controller = $section.'Controller';
$controller = new $controller( array('default_task' => 'show_list') );

$controller->execute( JRequest::getVar('task' ));
$controller->redirect();

?>
