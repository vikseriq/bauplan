<?php // bauplan .11 - .15 // vik
	JControllerV::getToolbar();
	JToolBarHelper::title( JText::_( 'Ценовая таблица' ). " :: ".$row->title, 'bp-calculator' );
	$uri = JControllerV::getURI();
?>

<script language="javascript" type="text/javascript">
function submitbutton(pressbutton, section) {
	var form = document.adminForm;
	if (pressbutton == 'cancel') {
		Joomla.submitform( pressbutton );
		return;
	}
	if ( form.title.value == "" ) {
		alert("Объект должен иметь название!");
	} else {
		Joomla.submitform(pressbutton);
	}
}
</script>
<style>
.xdiv {
	padding: 5px 40px;
	font-size: 14px;
	font-family: serif;
	color: #333333;
}
.xdiv input {
	color: #333333;
	font-size: 130%;
	width: 300px;
	margin: 2px 6px;
}
.xt th {
	border-bottom: 1px solid brown;
	font-weight: bold;
	width:200px;
}
</style>
<form action="<?php echo $uri ?>" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
	<fieldset class="adminform">
	<legend>Данные таблицы</legend>
	<div class="xdiv">
		<label>Заголовок</label>
		<input type="text" name="title" id="title" value="<?php echo $row->title ?>" />
		<small>Минимальная стоимость: <?php echo $row->value ?></small>
	</div>
	<div class="xdiv">
		<table class="xt">
		<thead><tr><th>Наименование</th><th>Стоимость</th></tr></thead>
		<?php
		for ($i = 0; $i < ROWS_LIMIT; $i++) {
			$r = new stdClass();
			if (isset($row->table[$i])){
				$r = &$row->table[$i];
			} else {
				$r->title = $r->value = ""; $r->id = 0;
			}
			echo '<tr><td>';
				echo '<input type="text" 	name="t['.$i.']" value="'.$r->title.'" />';
			echo '</td><td>';
				echo '<input type="text" 	name="v['.$i.']" value="'.$r->value.'" />';
				echo '<input type="hidden" 	name="x['.$i.']" value="'.$r->id.'" />';
			echo '</td></tr>';
			
		}
		?>
		</table>
	</div>	
	</fieldset>

	<div class="clr"></div>

	<input type="hidden" name="option" value="com_bauplan" />
	<input type="hidden" name="section" value="price" />
	<input type="hidden" name="id" value="<?php echo $row->id; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>