<?php
	// bauPlan
	// управление ценами
	// vikseriq / 2011-08-18 / J!3 2015-02-06

class priceController extends JControllerV {

	function __construct( $d = array() ) {
		parent::__construct( $d );
		$this->m = $this->getModel();
		define('ROWS_LIMIT', 20);	// количество строк в таблице
		$this->setTitle("Ценовые таблицы");
	}

		// вывод списка расчетных таблиц
	function show_list() {
	
		// выхов функцции пересчета минимальных цен
		if (isset($_POST['recalc'])){
			$this->m->do_recalc();
			echo "<p>Цены успешно обновлены</p>";
		}
		
		$rows = $this->m->getParents();
		for ($i = 0; $i < count($rows); $i++){
			$rows[$i]->table = $this->m->getChildren($rows[$i]->id);
		}

		$this->view('list', array('rows' => $rows));
	}
	
	
		// редактирование существующего объекта
	function edit( $id = 0 ) {
		if ($id == 0) {
			$id = JRequest::getVar('cid', null);
			if(isset($id[0])) $id = $id[0];
		}

		if ($id != 0) {
			$data = $this->m->getParent($id);
			$data->table = $this->m->getChildren($id);
			$this->view('item', array('row' => $data, 'isNew' => false));
		} else {
			$this->show_list();
		}
	}
		// добавление нового объекта
	function add() {
		$data = new stdClass();
		$data->title = "Новая таблица";
		$data->value = $data->ext = $data->id = 0;
		$data->table = array();
		$this->view('item', array('row' => $data, 'isNew' => true));
	}
	
		// сохранение таблицы
	function save( $show_again = false ) {
		$data = new stdClass();
		gv($id, 'id', 0, 'post', 'int');
		gv($data->title, 'title', '', 'post', 'string');
		gv($t, 't', array(), 'post', 'array');	// заголовки
		gv($v, 'v', array(), 'post', 'array');	// значения
		gv($x, 'x', array(), 'post', 'array');	// индексы

		if(strlen($data->title)){
			if ($id)
				$this->m->updateRow($id, $data->title, 1);	 // обновляем название
			else
				$id = $this->m->insertRow(0, $data->title, 1);// или создаем новую табличку
		}
		//die($id);
		$min = 0; // минимальная цена
		do {
			if (count($x)+count($v)+count($t) != ROWS_LIMIT*3){
				$msg = "Ошибка: данные повреждены"; break;
			}
			for ($i = 0; $i < ROWS_LIMIT; $i++){
				if (!strlen(trim($t[$i]))){			// заголовок пуст
					if ($x[$i] > 0)					// но задан индеск
						$this->m->deleteRow($x[$i]);// то удаляем этот ряд
				} else {
					$val = (float)$v[$i];
					if ($val >= 0){// если задан заголовок и значение не отрицательно
						if ($val < $min || !$min) $min = $val;
						if ($x[$i] > 0){			// и задан индекс
							$this->m->updateRow((int)$x[$i],trim($t[$i]), $val);	// обновляем
						} else {
							$this->m->insertRow($id, trim($t[$i]), $val);	// добавляем
						}
					}
				}
			}		
			if ($min) {		// и обновляем значение минимальной цены таблицы
				$this->m->updateRow($id, $data->title, $min);
			}
			if (!$id){
				$msg = 'Данные сохранены';
			} else {
				$msg = 'Данные обновлены';
			}
		} while(false);
		
		if (!$show_again){		// обработка "сохранить\применить"
			$link = JRoute::_('index.php?option=com_bauplan&section=price', 0);
		} else {
			$link = JRoute::_('index.php?option=com_bauplan&section=price&task=edit&cid[0]='.$id, 0);
		}
		$this->setRedirect($link, $msg);
		$this->redirect();
	}
	
		// обрабатываем так же, как и сохранение, и возвращаемся на страницу объекта
	function apply() {
		$this->save(true);
	}
	
		// удаление таблицы
	function remove() {
		$id = JRequest::getVar('cid', array());
		$j = 0;
		foreach($id as $i) {
			$this->m->deleteRow($i);
			$j++;
		}
		$this->setRedirect(JRoute::_('index.php?option=com_bauplan&section=price', 0), "Удалено объектов: ".$j);
		$this->redirect();
	}

}

?>