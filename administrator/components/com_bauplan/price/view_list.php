<?php
	JHTML::_('behavior.tooltip');
	JControllerV::getToolbar();
	$uri = JControllerV::getURI();
?>
<form action="<?php echo $uri ?>" method="post" name="adminForm" id="adminForm">
	<div align="right"><input type="submit" name="recalc" class="btn btn-info" value="Пересчитать минимальные цены для проектов" /></div>
	<table class="adminlist table">
	<thead>
		<tr>
			<th width="10" align="left"><?php echo JText::_( '#' ); ?></th>
			<th width="25%" nowrap="nowrap"><?php echo 'Название расчетной таблицы' ?></th>
			<th><?php echo "Содержимое таблицы"; ?></th>
		</tr>
	</thead>
	<tbody>
<?php
	for ($i = 0, $n = count($rows); $i < $n; $i++) {
		$row = &$rows[$i];
		JFilterOutput::objectHtmlSafe($row, ENT_QUOTES);

		$row->checked_out = false;
		$link = "index.php?option=com_bauplan&section=price&task=edit&cid[0]=".$row->id;
		echo '<tr>';

		td(JHTML::_('grid.checkedout', $row, $i));
		printf('<td><a href="%s">%s</a></td>', $link, $row->title);

		echo '<td><table class="adminlist">';
			echo '<thead><tr><th>Наименование</th><th>Стоимость</th></tr></thead>';
			foreach($row->table as $r){
				printf('<tr><td>%s</td><td>%s</td></tr>',
					$r->title, BauCore::rur($r->value)
				);
			}
		echo '</table></td>';

		echo '</tr>';
	}
	if (!$n){
		echo '<tr><td colspan="3">'. JText::_('Таблица еще пуста') .'</td></tr>';
	}
?>
	</tbody>
	</table>

	<input type="hidden" name="option" value="com_bauplan" />
	<input type="hidden" name="section" value="price" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="chosen" value="" />
	<input type="hidden" name="act" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
