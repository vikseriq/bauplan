<?php

jimport('joomla.application.component.model');

class priceModelprice extends JModelLegacy {

	var $data			= null;
	var $table			= null;

	function __construct () {
		//global $mainframe, $context;
		parent::__construct();

		$this->table = "#__bp_price";
	}

		// возвращает предков - названия таблиц
	function getParents(){
		$q = 'SELECT * FROM '.$this->table.' WHERE ext = 0';
		$this->data = $this->_getList($q);
		return $this->data;
	}
		// возвращает потомков - строки таблицы
	function getChildren( $id = 0 ){
		$q = 'SELECT * FROM '.$this->table.' WHERE ext = '.$id;
		$q.= ' ORDER BY value ASC ';
		$this->data = $this->_getList($q);
		return $this->data;
	}
		// возвращает одного предка
	function getParent( $id ) {
		$q = "SELECT * FROM ".$this->table." WHERE id = ".$id." AND ext = 0 LIMIT 1";
		$a = $this->_getList($q);
		return (count($a)) ? $a[0] : null;
	}
		// удаляет ряд и связанных потомков
	function deleteRow($id) {
		$this->_db->setQuery('DELETE FROM '.$this->table.' WHERE id = '.$id.' LIMIT 1');
		$this->_db->query();	// предок
		$this->_db->setQuery('DELETE FROM '.$this->table.' WHERE ext= '.$id.' LIMIT 1');
		$this->_db->query();	// потомки
	}
		// обновляет данные ряда
	function updateRow( $id, $title = "", $value = 0 ) {
		$this->_db->setQuery("UPDATE {$this->table} SET title = '$title', value = ".(float)$value."  WHERE id = $id LIMIT 1");
		$this->_db->query();
	}
		// добавляет ряд
	function insertRow( $ext, $title = "", $value = 0 ) {
		$this->_db->setQuery('INSERT INTO '.$this->table."(title, value, ext) ".
			"VALUES( '".$title."',".(float)$value.", ".(int)$ext.") ");
		$this->_db->query();
		return $this->_db->insertid();
	}
	
		// пересчет минимальных цен
	function do_recalc(){
		include(dirname(__FILE__).'/../object/model.php');
		$x = new objectModelobject();
		$x->price_update();
	}
	
}

?>