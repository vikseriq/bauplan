<?php

class BauToolbar
{
	static function element($is_new)
	{
		JToolBarHelper::apply();
		JToolBarHelper::save();
		if ($is_new) {
			JToolBarHelper::cancel();
		} else {
			JToolBarHelper::cancel('cancel', 'Закрыть');
		}
	}

	static function categories()
	{
		JToolBarHelper::title(JText::_('Категории'), 'bp-folder');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		//JToolBarHelper::apply("saveOrder", "Save Order");
		JToolBarHelper::spacer();
		JToolBarHelper::addNew();
		JToolBarHelper::editList();
		JToolBarHelper::deleteList();
	}

	static function objects()
	{
		JToolBarHelper::title(JText::_('Объекты'), 'bp-card');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		//JToolBarHelper::apply("saveOrder", "Save Order");
		JToolBarHelper::spacer();
		JToolBarHelper::addNew();
		JToolBarHelper::editList();
		JToolBarHelper::deleteList();
	}

	static function prices()
	{
		JToolBarHelper::title(JText::_('Ценовые таблицы'), 'bp-calculator');
		JToolBarHelper::addNew();
		JToolBarHelper::editList();
		JToolBarHelper::deleteList();
	}

	static function builds(){
		JToolBarHelper::title( JText::_( 'Завершенные проекты' ), 'bp-buildings' );
		JToolBarHelper::addNew();
		JToolBarHelper::editList();
		JToolBarHelper::deleteList();
	}
}

switch($task){
	case 'add':
	case 'edit':
	case 'apply':
		BauToolbar::element($task == 'add'); break;
	default:
		switch($section){
			case 'category':
				BauToolbar::categories(); break;
			case 'object':
				BauToolbar::objects(); break;
			case 'price':
				BauToolbar::prices(); break;
			case 'build':
				BauToolbar::builds(); break;
		}
		JToolBarHelper::preferences('com_bauplan', 600, 500);
		break;
}