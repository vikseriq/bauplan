<?php

jimport('joomla.application.component.model');

class objectModelobject extends JModelLegacy {

	var $data			= null;
	var $table			= null;
	var $table_xref		= null;
	var $total 			= null;
	var $_pagination	= null;
	var $limit			= null;
	var $limitstart		= null;

	function __construct () {
		parent::__construct();
		$app = JApplicationCms::getInstance();

		$this->table = "#__bp_plan";
		$this->table_xref = "#__bp_plan_category_xref";
		
		$this->limit		= $app->getUserStateFromRequest('limit', 'limit', 25, 0 );
		$this->limitstart	= $app->getUserStateFromRequest('limitstart', 'limitstart', 0 );
		if ($this->limit == 0) $this->limitstart = 0;
	}

	function getList( $order ) {
		$q = "SELECT * FROM ".$this->table." ";
		$f_order = $order['order']; $f_order_dir = $order['order_Dir'];
		if ($f_order)
			$q.= BauCore::esc(' ORDER BY '.$f_order.' '.$f_order_dir.' ');
		
		$this->data = $this->_getList($q, $this->limitstart, $this->limit);
		
		$this->total = $this->_getListCount($q);
	}
	
	// get categories
	function getCategories() {
		$table = "#__bp_category";
		$q = "SELECT * FROM ".$table." WHERE 1 ORDER BY parent_id, category_id";
		return $this->_getList($q);	
	}	
	// ~ get categories

	function getPagination() {
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination( $this->total, $this->limitstart, $this->limit );
		}
		return $this->_pagination;
	}
	
	function published( $cid, $state = true) {
		$state = ($state == true)? 1 : 0;
		$q = "UPDATE ".$this->table." SET published = ".$state." WHERE plan_id = ".$cid." LIMIT 1";
		$this->_db->setQuery($q);
		$this->_db->query();
	}
	
	function getItem( $cid ) {
		$q = "SELECT * FROM ".$this->table." WHERE plan_id = ".$cid." LIMIT 1";
		$a = $this->_getList($q);
		if (count($a)) {
			$a[0]->p = BauCore::str2params($a[0]->params);
			$a[0]->price_tables = BauCore::str2array($a[0]->prices, 3);
			return $a[0];
		} else return null;
	}
	
	function update($data) {
		$this->_db->updateObject($this->table, $data, "plan_id");
	}
	
	function insert($data) {
		unset($data->category_id);
		$this->_db->insertObject($this->table, $data, "plan_id");
		return $data->plan_id;
	}
	
	function remove($id) {
		$this->_db->setQuery("DELETE FROM ".$this->table." WHERE plan_id = $id LIMIT 1;");
		$this->_db->query();
		$this->_db->setQuery("DELETE FROM ".$this->table_xref." WHERE plan_id = $id LIMIT 1;");	
		$this->_db->query();		// удаляем старые связи
	}
	
	// установка категории
	function setCategory ($plan_id, $category_id) {
		$this->_db->setQuery("DELETE FROM ".$this->table_xref." WHERE plan_id = $plan_id LIMIT 1;");	
		$this->_db->query();		// удаляем старое
		$this->_db->setQuery("INSERT INTO ".$this->table_xref." ( xid, plan_id, category_id) VALUES ('', $plan_id, $category_id);");
		$this->_db->query();		// добавляем новое	
	}
	// получение категории
	function getCategory ($plan_id) {
		$this->_db->setQuery("SELECT category_id FROM ".$this->table_xref." WHERE plan_id = $plan_id ORDER BY xid LIMIT 1;");
		$this->_db->query();
		return $this->_db->loadResult();
	}
	
	// обновление полей, переданных в массиве
	function updateFields ($id, $info) {
		if (!count($info)) return;
		$q = "UPDATE ".$this->table." SET ";
		foreach($info as $field => $value) {
			$q.= "`$field` = '$value',";
		}
		$q[strlen($q)-1]= " ";	// последнюю запятую на точку с запятой
		$q.= " WHERE plan_id = $id LIMIT 1;";
		$this->_db->setQuery($q);
		echo $q;
		$this->_db->query();
	}
	
	
	// обновление минимальных цен
	function price_update( $list = null ){
		$q = "SELECT plan_id, area, price_min, params FROM ".$this->table.' ';
		if ($list)
			$q.= " WHERE plan_id IN ( ".$list." ) ";
		
		$l = $this->_getList($q);

		$db = JFactory::getDBO();
		foreach ($l as $x){
			$x->p = BauCore::str2params($x->params);
			if ($x->p['price_sale'] > 10.00){
				continue;	// не изменяем, если цена задана вручную
			}
			if (strlen($x->p['tables'])){
				$q = "SELECT MIN(value) FROM #__bp_price WHERE id IN ( ".$x->p['tables']." )";
				$db->setQuery($q);
				$min = (int)$db->loadResult();
				if ($min > 0 && $x->area) {
					$k = BauCore::price_mul($x->p['multiplier'], $x->p['floors']);
					$price = $min * $x->area * $k;
					//var_dump($min, $x->area, $k, $price); die;
					echo 'Проект '.$x->plan_id.' новая цена: '.$price.', коэффициент '.$k.'<br/>';
					$db->setQuery('UPDATE '.$this->table." SET price_min = '".$price."' WHERE plan_id = ".$x->plan_id);
					$db->query();
				}
			}
		}
	}
	
	// кросс-связь похожих проектов
	function xlink_related($plan, $plans){
		if (is_array($plans)) $plans = implode(',', $plans);
		$q = "SELECT plan_id, params FROM ".$this->table." WHERE plan_id IN (".$plans.")";
		$db = JFactory::getDBO();
		$db->setQuery($q);
		$l = $db->loadObjectList();
		foreach($l as $x){
			$p = BauCore::str2params($x->params);
			$related = explode(',', $p['related']);
			if (in_array($plan, $related)) continue;
			if($related[0] == 0)
				unset($related[0]);
			if ($k = array_search($x->plan_id, $related))
				unset($related[$k]);
			$related[] = $plan;
			$related = array_unique($related);
			$p['related'] = implode(',', $related);
			$x->params = BauCore::params2str($p);
			$db->updateObject($this->table, $x, "plan_id");
		}
	}
	
}

?>