<?php // bauplan .11 // vik

// форматирует вывод дробей
function af ( $number ) {
	return number_format($number, 3, '.', ' ');
}
function afp ( $number ) {
	return number_format($number, 2, '.', '');
}
function input_text ( $key, $value, $etc = ' maxlength="300"') {
	return sprintf('<input class="text_in" type="text" name="%s" id="%s" value="%s" %s />', $key, $key, $value, $etc);
}
function check_param( $array, $key) {
	return sprintf('<input type="checkbox" class="checkbox" name="params[%s]" id="%s" value="1" %s />', 
			$key, $key, (isset($array[$key])&&$array[$key])?'checked':'');
}
	// Вывод строки управления. Заголовок, значения, описание
function tt( $title, $field, $desc = '' ) {
	$title = JText::_($title);
	if (strlen($desc))
		$title = '<abbr title="'.$desc.'">'.$title.'</abbr>';

	echo '
	<div class="control-group">
		<div class="control-label key"><label>'.$title.'</label></div>
		<div class="controls">'.$field.'</div>
	</div>';
}
function selector ( $name, $selected = 0, $values = array() ) {
	if ($name == 'type')
		$values = BauCore::getObjectTypes();
	else if ($name == 'material')
		$values = BauCore::getObjectMaterials();

	$html = '<select name="'.$name.'">';
	foreach($values as $i => $v) {
		if ($i == $selected)
			$html.= '<option value="'.$i.'" selected="yes">'.$v.'</option>';
		else
			$html.= '<option value="'.$i.'">'.$v.'</option>';
	}
	$html.= '</select>';
	return $html;
}

function multiselect ( $id, $tables, $selected = "" ){
	if (!count($tables)) return 'Нет таблиц';
	//$selected = explode(',', $selected);
	$html = '<select multiple name="'.$id.'" id="'.$id.'" >';
	
	foreach($tables as $t){
		if (strpos($selected, $t->id)===false)
			$s = ''; else $s = 'selected';
		$html.= '<option value="'.$t->id.'" '.$s.'>'.$t->title.'</option>';
	}
	
	$html.= '</select>';
	return $html;
}

	JControllerV::getToolbar();
	JToolBarHelper::title( $row->title, 'card.png' );

	$uri = JControllerV::getURI();
	$editor = JFactory::getEditor();
	JRequest::setVar( 'hidemainmenu', 1 );
	JFilterOutput::objectHTMLSafe( $row, ENT_QUOTES, 'description' );
	$cats = $this->getModel()->getCategories();
	$cat_list = array(0 => '(автоматически)');
	foreach($cats as $cat){
		$cat_list[$cat->category_id] = $cat->title;
	}
	define( 'SITE_URL', str_replace('/administrator/', '', JUri::base()));
	if ($row->plan_id > 0) {
		$images = bImages::images_in_dir($row->image_path);
	}
	
?>
<style type="text/css">
table.admintable {
	font-size: 14px;
}
table.admintable td.key {
	font-size: 12px;
}
.text_in {
	font-size: 16px;
}
.params .text_in {
	width: 120px;
}
select {
	font-size: 12px;
}
.add_new_row {
	border-bottom: 1px dotted;
	color: #0B55C4;
	cursor: pointer;
	float: right;
}
.control-move {
	float: right;
}
</style>
<script language="javascript" type="text/javascript">
function submitbutton(pressbutton, section) {
		// исправляем запятую на точку для дробных значений
	var ffix = ['area', 'area_live', 'width', 'price_min'/*, 'params[price_sale]'*/];
	var i = ffix.length;
	while (--i)
		document.getElementById(ffix[i]).value = document.getElementById(ffix[i]).value.replace(",", ".");

	var form = document.adminForm;
	if (pressbutton == 'cancel') {
		Joomla.submitform( pressbutton );
		return;
	}

	if ( form.title.value == "" ) {
		alert("Объект должен иметь название!");
	} else {
		<?php echo $editor->save( 'description' ); ?>
		Joomla.submitform(pressbutton);
	}
}
function reset (id, val) {
	document.getElementById(id).value = val;
}
function lock_images() {
	var locker = ['image_type1', 'image_type0', 'image_upload', 'image_path', 'image_main'];
	var i = locker.length; while (--i) document.getElementById(locker[i]).disabled = true;
}

// автоподсчет площади
function auto_area(){
	var w = parseFloat(jQuery('#width').val());
	var h = parseFloat(jQuery('#length').val());
	w = w*h;
	if(w > 0) jQuery('#auto_area').html('Площадь: '+(w.toFixed(3))+' m<sup>2</sup>');
}

function add_new_row(){
	var rows = jQuery('#prices_table');
	rows.append(jQuery('#new_row')[0].innerHTML);
}

function move_row(element, direction){
	var e = jQuery(element).closest('.control-group');
	if (direction < 0)
		e.after(e.prev());
	else
		e.before(e.next());
}

jQuery(document).ready(function(){
	jQuery('#width').on('keyup', auto_area);
	jQuery('#length').on('keyup', auto_area);

	jQuery('.sortable div.control-group').each(function(){
		jQuery(this).append('<div class="control-move">' +
			'<a href="#" onclick="move_row(this, -1);return false;">&#8593;</a> ' +
			'<a href="#" onclick="move_row(this, 1);return false;">&#8595;</a>' +
			'</div>');
	});
});
</script>

<form action="<?php echo $uri ?>" method="post" enctype="multipart/form-data" name="adminForm" id="adminForm">
	<table class="admintable" style="width:100%">
		<tr><td>
		<fieldset class="adminform row-fluid form-horizontal">
			<div class="span6">
				<legend>Параметры объекта</legend>
				<?php

				tt('Номер плана', 	$row->plan_id,				"Порядковый номер плана в базе данных");
				tt('Название', 		input_text('title', $row->title));
				tt('Артикул', 		input_text('sku', $row->sku));
				tt('Опубликован', 	JHTML::_('select.booleanlist',  'published', 'class="inputbox"', $row->published ));

				tt('ЧПУ: Псевдоним', input_text('alias', $row->alias),
					"Название проекта для использовании в ссылках, например `dom-iz-brevna-dm123`");
				tt('ЧПУ: Катерогия ', selector('category', $row->category_tab, $cat_list),
					"При формировании ссылки будет использован символьный код указанной категории");
				
				tt('Тип постройки',	selector('type', $row->type),			"Дома, бани, беседки, колодцы...");
				tt('Материал',		selector('material', $row->material),	"Основной материал строения");

				tt('Ценовые таблицы<br><small>выделение: ctrl+клик</small>', 
						multiselect('params[tables][]', $tables, $row->p['tables']),
						"Выбор таблиц, по которым будет производиться расчет стоимости силовой части");

				tt('Похожие объекты', input_text('params[related]', $row->p['related'], 'style="width: 200px;"'), "ID проектов через запятую. Выводятся справа на странице товара.");
				
				if ($row->plan_id > 0) {	// только для созданных

				tt('Объект добавлен',		$row->cdate);
				tt('Последнее обновление',	$row->mdate);
				tt('Просмотров', 	$row->hits.' <span><input type="checkbox" name="reset_hits" value="1"> сброс</span>');
				tt(' ', '<a href="'.JRoute::_('/index.php?option=com_bauplan&view=object&pid='.$row->plan_id).'" target="_blank">Открыть страницу на сайте</a>');
				}
				
				?>
			</div>
			<div class="span6">
				<legend>Изображения</legend>
				<?php
			if ($row->plan_id == 0) {
				tt('! Важно !', 		'Для работы с изображениями необходимо сохранить объект!');
			} else {
				tt('Директория', 			input_text('image_path', $row->image_path, 'style="font-size:10px;width:100%;" disabled="disabled"'));
				tt('Основное изображение', 	input_text('image_main', $row->image_main, 'style="font-size:10px;width:100%;"'));
	// ~~~ Блок изображений
				echo '<div style="width: 100%;"><div id="ic">';

				echo '<input type="hidden" name="v_to_del" id="v_to_del" value="" />';
				echo '<div id="delete_alert">Файлов к удалению: <span id="delete_count"></span><br/>Для удаления нажмите "Применить"/"Сохранить"</div>';
				
				$path = $row->image_path;
				$i = 0; if (count($images)) foreach ($images as $img) {
					printf('<div %s class="iprev" rel="%s">
								<span class="m open"></span><span class="m def"></span><span class="m del"></span>
						<img src="%s?078078" alt="%s"/><span>%s</span>
						</div>',
						($path.$img==$row->image_main)?'id="mi" ':'', $path.$img,
						SITE_URL.$path.$img, $path.$img,
						BauCore::planTitle($img)
					);
					if (++$i % 4 == 0)	echo '<br>';
				}
				echo '</div></div><br style="clear: both;" />';
	// ~~~ Конец блока
				tt('', 		'Загрузить изображение');
				tt('Файл изображения', '<input type="file" id="image_upload" name="image_upload" size="30"> <small><a onclick="reset(\'image_upload\', \'\')" style="cursor:pointer;">сброс</a></small>');
				tt('Тип изображения',  '
							<label><input type="radio" name="image_type" value="1" class="inputbox" checked="checked" /> Фотография</label>
							<label><input type="radio" name="image_type" value="2" class="inputbox" checked="checked" /> Планировка</label><br/>
							<label><input type="radio" name="image_type" value="20" class="inputbox" checked="checked" /> Цокольный</label>
							<label><input type="radio" name="image_type" value="21" class="inputbox" checked="checked" /> Первый</label>
							<label><input type="radio" name="image_type" value="22" class="inputbox" checked="checked" /> Второй</label>
							<label><input type="radio" name="image_type" value="23" class="inputbox" checked="checked" /> Третий</label>');
			}
			/*{
				$_db = JFactory::getDBO();
				$ob = $_db->setQuery('SELECT id, name from #__bwg_gallery WHERE published = 1');
				$ar = $ob->loadAssocList();
				if (count($ar)){
					$galleries = array( 0 => '- не выбрано -');
					foreach($ar as $a){
						$galleries[$a['id']] = $a['name'];
					}
					tt('', 'Дополнительная галерея');
					tt('<a href="/administrator/index.php?option=com_gallery_wd&view=galleries" target="_blank">WD Gallery</a>', selector('params[gallery_id]', $row->p['gallery_id'], $galleries));
				}
			}*/
				?>
			</div>
		</fieldset>
		</td></tr>
		<tr><td>
			<fieldset class="adminform row-fluid form-horizontal">
				<legend>Параметры</legend>
			<div class="span6">
				<?php
				tt('Общая площадь', input_text('area', 		af($row->area))		.' m<sup>2</sup>');
				tt('Жилая площадь', input_text('area_live', af($row->area_live)).' m<sup>2</sup>');
				tt('Ширина', 		input_text('width', 	af($row->size_w))	.' m');
				tt('Длина', 		input_text('length', 	af($row->size_l))	.' m<br><div id="auto_area"></div>');
				tt('Рекомендуемый<br>проект', check_param($row->p, 'featured'), 'Отображать проект в списке рекомендуемых проектов');
				?>
			</div>
			<div class="span6">
				<?php
				$floors = $row->p['floors'];
				tt('Дополнительные<br>характеристики', 	
					check_param($row->p, 'floor_0').' Цокольный этаж '.
					'<br/><br/>'.
					'Количество этажей: <select name="params[floors]">
						<option value="0" '.(($floors==0)?' selected ':'').'>не указано</option>
						<option value="1" '.(($floors==1)?' selected ':'').'>Один этаж</option>
						<option value="2" '.(($floors==2)?' selected ':'').'>Два этажа</option>
						<option value="3" '.(($floors==3)?' selected ':'').'>Три этажа</option>
					</select>'.
					/*check_param($row->p, 'floor_2').' Второй этаж '.
					check_param($row->p, 'floor_3').' Третий этаж '.*/
					'<br/><br/>'.
					check_param($row->p, 'car').' Гараж '.
					check_param($row->p, 'swim').' Сауна '.
					'<br/><br/>'.
					check_param($row->p, 'floor_b').' Балкон '.
					check_param($row->p, 'floor_out').' Терраса '.
					' '
				);
				?>
			</div>
			</fieldset>	
		</td></tr>
		<tr><td>
			<fieldset class="adminform row-fluid form-horizontal">
				<div class="span6">
					<legend>Управление ценами</legend>
					<?php
					if ($this->cp->get('enable_rating', 1)) {
						$a_rate = explode(';', $row->rate);
						if (isset($a_rate[1]))
							tt('Рейтинг<br><br>',
								sprintf('Оценка: %.2f из 5.00 | Голосов: %d <br/><small><input type="checkbox" name="reset_rate" value="1" />сброс</small>', $a_rate[0], $a_rate[1]),
								'Внутренний рейтинг данного объекта, выставленный пользователями'
							);
					} else {
						tt('Рейтинг',	    input_text('rate', $row->rate), "Целое число [0..5]. Может использоваться как показатель класса (средний/высокий/люкс)");
					}
					tt('Минимальная цена',	input_text('price_min', afp($row->price_min)).' RUR', "Расчитанная минимальная цена из ценовых таблиц. Сбрасывается при обновлении цен из таблиц");
					tt('Своя цена', 		input_text('params[price_sale]', afp($row->p['price_sale'])).' RUR', "В случае акций/скидок можно задать свою новую цену для этого объекта. Значение заменит МИНИМАЛЬНУЮ ЦЕНУ и не будет изменяться при пересчете ценовых таблиц!");
					tt('Коэфф. цены', 		input_text('params[multiplier]', afp($row->p['multiplier'])).' ', "Ценовой коэффициент для объекта. На него будут умножаться все ценовые таблицы. Для одноэтажных строений этот коэффициент имеет приоритет. 1.00 = оригинальная цена");
					tt('Цена проекта', 		input_text('params[price_plan]', afp($row->p['price_plan'])).' RUR', "Цена проектной документации");
					tt('Описание цены', 	'<textarea class="text_in" rows="6" name="params[containment]">'.($row->p['containment'] ? $row->p['containment'] : '').'</textarea>', "Детальное описание материалов, работ и услуг, включенных в цену проекта");
					?>
				</div>
				<div class="span6">
					<legend>Детальные расценки</legend>
					<div class="sortable" id="prices_table">
						<div id="new_row" style="display:none;">
							<div class="control-group">
								<div class="control-label key"><label>
										<? echo input_text('prices_key[]', '', 'maxlength="300" placeholder="Новое свойство"');
										echo '<br/>'.input_text('prices_group[]', '', 'maxlength="30" placeholder="Группа"'); ?>
									</label></div>
								<div class="controls"><? echo input_text('prices_val[]', afp(0)).' RUR'; ?></div>
							</div>
						</div>
					<?
						foreach($row->price_tables as $ptr){
							$plus = ($ptr[1][0] == '+') ? '+' : '';
							tt(
								input_text('prices_key[]', $ptr[0]).'<br/>'.input_text('prices_group[]', $ptr[2]),
								input_text('prices_val[]', $plus.afp($ptr[1])).' RUR'
							);
						}
					?>
					</div>
					<a class="add_new_row" onclick="add_new_row();return false;">+ добавить свойство</a>
				</div>
			</fieldset>
		</td></tr>
		<tr><td>
			<fieldset class="adminform row-fluid form-horizontal">
				<legend>Описание</legend>
				<div class="span12">
					<?php
					tt('Заголовок страницы', input_text('meta_title', $row->meta_title));

					tt('Описание', $editor->display(
						'description', htmlspecialchars($row->description, ENT_QUOTES),
						'800', '500', '80', '40', array('pagebreak', 'readmore'))
					);
					tt('Meta: ключевые слова', input_text('meta_keys', $row->meta_keys));
					tt('Meta: описание', input_text('meta_desc', $row->meta_desc));
					?>
				</div>
			</fieldset>
		</td></tr>
	</table>

	<div class="clr"></div>

	<input type="hidden" name="option" value="com_bauplan" />
	<input type="hidden" name="id" value="<?php echo $row->plan_id; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>

<style>
	#ic {
		float: left;
		display: block;
		padding: 4px;
		border: 1px solid #CCC;
		position: relative;
	}
	.iprev {
		background-repeat: no-repeat;
		background-color: white;
		border: 1px solid gray;
		height: 141px;
		margin: 2px;
		padding: 2px;
		text-align: center;
		width: 125px;
		display:block;
		position: relative;
		float: left;
	}
	#mi {
		background-color: #BBFFBB;
	}
	.iprev img {
		max-height: 120px;
		max-width: 120px;
		min-height: 20px;
		min-width: 20px;
		margin-top: 20px;
	}
	#ic .m {
		display: block;
		float: left;
		height: 16px;
		width: 16px;
		padding: 1px;
		position: absolute;
		top: 2px;
		background-color: white;
		background-repeat: no-repeat;
		background-position: 1px 1px;
		border: 1px solid gray;
		cursor: pointer;
	}
	#ic .def { background-image: url('templates/hathor/images/menu/icon-16-default.png'); left: 2px; }
	#ic .open{ background-image: url('templates/hathor/images/menu/icon-16-content.png'); left: 28px; }
	#ic .del { background-image: url('templates/hathor/images/menu/icon-16-logout.png'); right: 2px; }
	#delete_alert {
		color: Red;
		display: none;
		font-size: 13px;
		font-style: italic;
	}
</style>
<script>
	var my, delete_count = 0;
	jQuery(document).on('ready', function(){
		var $ = jQuery;
		$('.iprev .def').on('click', function(){
			my = $(this).parent().attr('rel');
			$('#image_main').val(my);
			$('.iprev').each(function(){
				if ($(this).attr('rel') != my)
					$(this).attr('id', '');
				else
					$(this).attr('id', 'mi');
		});
		});
		$('.iprev .open').on('click', function(){
			my = $(this).parent().attr('rel');
			window.open('http://'+window.location.hostname+my);
		});
		$('.iprev .del').on('click', function(){
			my = $(this).parent().attr('rel');
			var del_list = $('#v_to_del');
			del_list.val(del_list.val() + my + ',');
			$('#delete_alert').show();
			$('#delete_count').html(++delete_count);
			$(this).parent().css('display', 'none');
		});

		// и на случай, если нет изображения по умолчанию - отмечаем первое
		if (!$('#mi').length){
			$('.iprev .def').first().click();
		}
	});
</script>