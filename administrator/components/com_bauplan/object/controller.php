<?php
	// bauPlan
	// управление объектами
	// vikseriq / 2011-07-11 - 08-24 / J!3 2015-02-06

class objectController extends JControllerV {

	function __construct( $d = array() ) {
		parent::__construct( $d );
		$this->setTitle("Объекты");
		$this->m = $this->getModel();
		$this->cp = JComponentHelper::getParams('com_bauplan');
	}

	var $cp;

		// вывод списка объектов
	function show_list() {
		if (isset($_POST['price_update'])){
			$msg = null;
			if (isset($_POST['price_update_all'])){
				$this->m->price_update();
				$msg = "Минимальные цены успешно обновлены согласно таблицам для всех объектов";
			} else {
				$ch = (array)JRequest::getVar('cid', array(), 'post');
				if (count($ch)) {
					$list = implode(', ', $ch);
					$this->m->price_update( $list );
					$msg= "Минимальные цены обновлены для отмеченных объектов";
				}
			}
			if ($msg) {
				$this->setRedirect(JRoute::_('index.php?option=com_bauplan&section=object', 0), $msg);
				$this->redirect();
			}
		}
		gv($this->lists['order'], 		'filter_order',		'plan_id');
		gv($this->lists['order_Dir'],	'filter_order_Dir', 'desc');
		$this->m->getList($this->lists);

		$this->view('list', array('rows' => $this->m->data, 'page' => $this->m->getPagination()));
	}

		// редактирование существующего объекта
	function edit( $plan_id = 0 ) {
			// если plan_id не передан, то берем из запроса
		if ($plan_id == 0) {
			$plan_id = JRequest::getVar('cid', null);
			if(isset($plan_id[0])) $plan_id = $plan_id[0];
		}
		do{
			if ($plan_id != 0) {
				$data = $this->getModel()->getItem($plan_id);
				if (!$data) break;
				//$data->categories = $this->getModel()->getCategory($data->plan_id);
				$this->setTitle($data->title);
				$this->view('item', array(
					'row' => $data,
					'tables' => BauCore::getPriceTables(),
					'isNew' => false)
				);
				return;
			} else break;
		} while(false);
		$this->setRedirect(JRoute::_('index.php?option=com_bauplan&section=object', 0), "Объект с таким индексом не найден");
	}

		// добавление нового объекта - обнуляем переменные
		// лучше всё делать вручную, так как "глушить" через @ - моветон и источник ошибок
	function add() {
		$data = new stdClass();
		$data->plan_id = $data->hits = $data->published = $data->ordering = 0;
		$data->area = $data->area_live = $data->size_w = $data->size_l = 0;
		$data->price_min = 0;
		$data->image_path = $data->image_main = "";
		$data->p = BauCore::str2params("");
		$data->p['multiplier'] = 1;
		$data->p['related'] = "";
		$data->description = "";
		$data->prices = "";
		$data->price_tables = array();
		$data->rate = "";
		$data->type = $data->material = 0;
		$data->title = "Новый объект";
		$data->sku = "";
		$data->alias = "";
		$data->category_tab = 0;
		$data->meta_title = $data->meta_keys = $data->meta_desc = "";
		$this->view('item', array(
			'row' => $data,
			'tables' => BauCore::getPriceTables(),
			'isNew' => true)
		);
	}

		// сохранение объекта
	function save( $show_again = false ) {
			// обрабатываем переданные параметры формы
		$id = 0; $data = new stdClass();
		gv($id, 'id');
		$data->plan_id	= $id;
		gv($data->description,	'description', '', 'post', 'string', JREQUEST_ALLOWHTML);
		gv($data->title, 		'title', '', 'post', 'string');
		gv($data->sku, 			'sku', '', 'post', 'string');
		gv($data->alias, 		'alias', '', 'post', 'string');
		if (!$data->alias)
			$data->alias = $data->title;
		$data->alias = JFilterOutput::stringURLSafe(BauCore::rus2lat($data->alias));

		gv($data->category_tab, 'category', '', 'post', 'int');
		gv($data->meta_title, 	'meta_title', '', 'post', 'string');
		gv($data->meta_keys, 	'meta_keys', '', 'post', 'string');
		gv($data->meta_desc, 	'meta_desc', '', 'post', 'string');
		gv($data->published, 	'published');
		gv($data->area, 		'area');
		gv($data->area_live, 	'area_live');
		gv($data->size_w, 		'width');
		gv($data->size_l, 		'length');
		gv($data->price_min, 	'price_min');
		gv($data->type,			'type');
		gv($data->material,		'material');
		//gv($data->image_path,	'image_path');
		gv($data->image_main,	'image_main');
		$params = '';
		gv($params, 			'params', array(), 'post', 'array');
		$params['multiplier'] = floatval($params['multiplier']);
		if ($params['multiplier'] <= 0)
			$params['multiplier'] = 1;

		if (isset($params['tables'])) {
			$params['tables'] = join(',', $params['tables']);
			// теперь выбираем минимальную цену (указана в value заголовочной записи)
			$q = "SELECT MIN( value ) FROM #__bp_price WHERE id IN ( ".$params['tables']." )";
			$db = JFactory::getDBO();
			$db->setQuery($q);
			$min = (int)$db->loadResult(); $db = null;
			if ($min > 0 && $data->area) {
				// мин = цена*площадь*коэф
				$data->price_min = $min * $data->area *
					BauCore::price_mul( $params['multiplier'], $params['floors']);
			}
		}
		if (isset($params['price_sale'])) {
			$params['price_sale'] = (float)$params['price_sale'];
			/*
			if ($params['price_sale'] > 10.00) {
				$data->price_min = $params['price_sale'];
			} else {
				$params['price_sale'] = 0.00;
			}*/
		}
		if (isset($params['related']) && strlen($params['related'])){
			// кросс-связь "похожих проектов"
			$this->m->xlink_related($data->plan_id, explode(',', $params['related']));
		}
		$data->params = BauCore::params2str($params);

		$data->prices = '';
		// расчет расценок
		foreach($_POST['prices_key'] as $i => $key){
			if (trim($key)){
				$plus = ($_POST['prices_val'][$i][0] == '+') ? '+' : '';
				$price = floatval($_POST['prices_val'][$i]);
				$line = $key.';'.$plus.$price.';'.trim($_POST['prices_group'][$i])."\n";
				$data->prices.= $line;
			}
		}
		$msg = '';

			// сброс рейтинга/хитов
		$_tmp = 0;
		gv($_tmp, 'reset_hits', 0);
			if ($_tmp == 1) $data->hits = 0;
		gv($_tmp, 'reset_rate', 0);
			if ($_tmp == 1) $data->rate = '0;0;0;';
		if ($this->cp->get('enable_rating', 0) == 0){
			// рейтинг задается вручную
			gv($data->rate, 'rate', 0, 'post', 'int');
		}

		$model = $this->getModel();

		if ($id > 0) {								// обновление существующих + установка даты
			$data->mdate	=	date("Y-m-d H:i:s", time()+11);
			$model->update($data);
		} else if ($id == 0) {						// или добавление новых с получением индекса
			$data->cdate	=	date("Y-m-d H:i:s", time()+11);
			$data->mdate	=	"0000-00-00 00:00:00";
			$id = $model->insert($data);
		}
			// обработка удалений
		$image_to_del = '';
		gv($image_to_del,'v_to_del', '');
		$i_to_del = explode(',', $image_to_del);
		foreach($i_to_del as $x){
			$x = trim($x);
			if (!$x) continue;
			//echo '['.$x.']';
			@unlink(ROOT_PATH.$x);
			$msg.=" Файл [$x] удален. \n";
		}

			// обработка изображения
		$image_path = $image_main = $image_type = '';
		gv($image_path,	'image_path');
		gv($image_main,	'image_main');
		gv($image_type,	'image_type', 1);
		if (strlen($_FILES['image_upload']['name']) && $info = bImages::process_image($id, $image_type)) {
			if (isset($info['image']) && strlen($info['image']))
				$tmp = array('image_path' => $info['path']);
					// для изображений объекта - устанавливаем по умолчанию
				if($image_type == 1) $tmp['image_main'] = $info['image'];

				$model->updateFields($id, $tmp);
		}

			// обработка "сохранить\применить"
		if (!$show_again)
			$link = JRoute::_('index.php?option=com_bauplan&section=object', 0);
		else
			$link = JRoute::_('index.php?option=com_bauplan&section=object&task=edit&cid[0]='.$id, 0);

		$msg.= 'Объект успешно сохранен';
		$this->setRedirect($link, $msg);
		$this->redirect();
	}
		// обрабатываем так же, как и сохранение, и возвращаемся на страницу объекта
	function apply() {
		$this->save(true);
	}

	function cancel(){
		$this->setRedirect(JRoute::_('index.php?option=com_bauplan&section=object', 0));
		$this->redirect();
	}

		// изменение статуса - публикация
	function publish() {
		$cid = JRequest::getVar('cid');
		foreach($cid as $i) {
			$this->getModel()->published($i, true);
		}
		$this->show_list();
	}
		// изменение статуса - скрытие
	function unpublish() {
		$cid = JRequest::getVar('cid');
		foreach($cid as $i) {
			$this->getModel()->published($i, false);
		}
		$this->show_list();
	}
		// удаление объекта
	function remove() {
		$cid = JRequest::getVar('cid');
		foreach($cid as $i) {
			$this->getModel()->remove($i);
		}
		$this->show_list();
	}
	
		// обновление связей "похожие проекты" для всех - разово
	function cross_related(){
		$db = $this->m->_db;
		$db->setQuery('SELECT plan_id, params FROM #__bp_plan');
		$res = $db->loadObjectList();
		foreach($res as $x){
			$p = BauCore::str2params($x->params);
			if ($p['related']){
				$this->m->xlink_related($x->plan_id, $p['related']);
				echo $x->plan_id.' ';
			}
		}
	}

}

?>