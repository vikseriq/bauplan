<?php
function af ( $number ) {
	return number_format($number, 3, '.', ' ');
}
	JHTML::_('behavior.tooltip');
	$uri = JControllerV::getURI();
	JControllerV::getToolbar();
	
	$cats = $this->getModel()->getCategories();
	$types = BauCore::getObjectTypes();
	$_ord = &$this->lists['order_Dir'];
	$_orr = &$this->lists['order'];
?>
<form action="<?php echo $uri ?>" method="post" name="adminForm" id="adminForm">
<? if(count($rows)): ?>
	<table class="adminlist table table-striped artileList">
	<thead>
		<tr>
			<th width="1%"	nowrap="nowrap">
				<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
			</th>
			<th width="3%"	nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'ID', 'plan_id', $_ord, $_orr); ?></th>
			<th><?php echo JHTML::_('grid.sort', 'Название планировки', 'title', $_ord, $_orr); ?></th>
			<th width="10%"	nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Тип постройки', 'type', $_ord, $_orr); ?></th>
			<th width="5%"	nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Общая площадь', 'area', $_ord, $_orr); ?></th>
			<th width="5%"	nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Жилая площадь', 'area_live', $_ord, $_orr); ?></th>
			<th width="10%" nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Минимальная цена', 'price_min', $_ord, $_orr); ?></th>
			<th width="5%"	nowrap="nowrap"><?php echo JText::_('Изображение'); ?></th>
			<th width="1%"	nowrap="nowrap"><?php echo JText::_('Опубликовано'); ?></th>
			<!--<th width="1%" nowrap="nowrap"><?php echo JText::_('Сортировка'); ?></th>-->
			<th width="1%"	nowrap="nowrap"><?php echo JHTML::_('grid.sort', 'Просмотров', 'hits', $_ord, $_orr); ?></th>
		</tr>
	</thead>
	<tbody>
	<?php
	for ($i = 0, $n = count($rows); $i < $n; $i++) {
		$row = &$rows[$i];
			
		JFilterOutput::objectHtmlSafe($row, ENT_QUOTES);
		$row->checked_out = false;
		$row->ordering = true;
		$row->id = $row->plan_id;
		$checked 	= JHTML::_('grid.checkedout',   $row, $i );
		$published 	= JHTML::_('grid.published', 	$row, $i );
		$published  = JHtml::_('jgrid.published', $row->published, $i, '', true, 'cb', '', '');

		?>
		<tr class="<?php echo "row".($i % 2); ?>">
			<td>
				<?php echo $checked; ?>
			</td>
			<td>
				<?php echo $row->id; ?>
			</td>
			<td>
				<span class="editlinktip hasTip" title="Название::<?php echo $row->title; ?>">
				<?php
					$link = "index.php?option=com_bauplan&section=object&task=edit&cid[0]=".$row->id;
					printf('<a href="%s">%s</a>', JRoute::_($link), $row->title);

					if ($row->alias)
						echo '<br/><span class="small">Алиас: '.$row->alias.'</span>';
				?>
				</span>
			</td>
			<?php
			td($types[$row->type]);
			td(af($row->area));
			td(af($row->area_live));
			td(number_format($row->price_min, 2, ',', ' ')." RUR");
			echo '<td>';
			$im = bImages::images_in_dir($row->image_path);
			if (strlen($row->image_main) && count($im)){
				printf('<a href="%s" target="_pop" title="Всего: %d">'.
					'<img src="%s" style="max-height:120px;max:width:120px" /></a>',
					$row->image_main, count($im), BauCore::img($row->image_main, 120, 120));
			} else echo " - ";
			echo '</td>';
			?>
			<td align="center">
				<?php echo $published;?>
			</td>
			<!--<td class="order">
				<span><?php echo $page->orderUpIcon( $i, 1, 'orderup', 'Move Up', 1); ?></span>
				<span><?php echo $page->orderDownIcon( $i, $n, 1, 'orderdown', 'Move Down', 1 ); ?></span>
				<input type="text" name="order[]" size="5" value="<?php echo $row->ordering; ?>" class="text_area" style="text-align: center" />
			</td>-->
			<td>
				<?php echo $row->hits; ?>
			</td>
		</tr>
		<?php
	}
	?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="8"><?php echo $page->getListFooter(); ?></td>
			<td colspan="2">На страницу: <?php echo $page->getLimitBox(); ?></td>
		</tr>
	</tfoot>
	</table>
<? else: ?>
	<div class="alert alert-no-items">
		<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
	</div>
<? endif ?>
	<div align="right">
		<input type="submit" value="Обновить минимальные цены" name="price_update" class="btn btn-info" />
		<span><input type="checkbox" value="1" name="price_update_all" /> для всех объектов</span>
	</div>
	
	<input type="hidden" name="option" value="com_bauplan" />
	<input type="hidden" name="section" value="object" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
	