<?php

require_once (JPATH_COMPONENT.DS.'core.php');

$section = JRequest::getVar('section', 'category');

/* Hierarhy:
	component
		section1
			controller
			model
			view_1
			view_2
		section2
			controller
			model
	etc.
*/
require_once (JPATH_COMPONENT.DS.$section.DS.'controller.php');
require_once (JPATH_COMPONENT.DS.$section.DS.'model.php');

JHTML::stylesheet("com_bauplan.css", 'administrator/components/com_bauplan/assets/');

$controller = $section.'Controller';
$controller = new $controller( array('default_task' => 'display') );

$controller->execute( JRequest::getVar('task' ));
$controller->redirect();

?>
