<?php defined('_JEXEC') or die('Hello, hacker!');
// mod_bp_calculator	by vikseriq		2011-08-24
// works with BauPlan data tables

$db = JFactory::getDBO();
$db->setQuery('SELECT title, value FROM #__bp_price WHERE value > 0 AND ext > 0 ORDER BY ext ASC, title ASC ');
$out = $db->loadAssocList();
// php вариант
$opts = '';
if ($out) foreach ($out as $x){
	$opts .= '<option value='.$x['value'].'>'.$x['title'].'</option>';
}
?>
<script>
  function cal_c(){
    document.getElementById('calc_out').innerHTML = '';
    var x = parseFloat(document.getElementById('calc_type').value.replace(',', '.'));
    var y = parseFloat(document.getElementById('calc_square').value.replace(',', '.').replace(' ', ''));
    if (x >= 1 && y >= 1){
      x = x * y;
      var money = x.toFixed(0)
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + ' ');

      document.getElementById('calc_out').innerHTML = ' ' + money + ' руб.';
    }
  }
</script>

<form method="post" action="#">
	<ul class="b-lhlp">
		<li class="b-item b-item-inp">
			<div class="b-inp"><input type="text" id="calc_square" name="cal_square" placeholder="Площадь, м2"></div>
		</li>
		<li class="b-item">
			<div class="b-ss">
				<select class="ss" id="calc_type" name="cal_type">
					<option value="0" selected="selected">Тип массива</option>
					<?php echo $opts ?>
				</select>
			</div>
		</li>
	</ul>
	<div class="b-calc-value" id="calc_out"></div>
	<div class="b-zero">*данные являются рассчетными, для более точной цены сделайте запрос
		предварительной сметы (бесплатно).
	</div>
	<button class="b-btn-send" onclick="cal_c();return false;">Посчитать</button>
</form>