<?php defined('_JEXEC') or die;

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$rows = [];

$model_file = JPATH_BASE . '/components/com_bauplan/category/model.php';
if (!file_exists($model_file))
	return;

include_once $model_file;
$model = new categoryModelcategory();

$items = $model->getCategories();
foreach ($items as $item) {
	if (strlen($item->image) > 5)
		$rows[] = $item;
}

if ($rows)
	require JModuleHelper::getLayoutPath('mod_bp_category', $params->get('layout', 'default'));