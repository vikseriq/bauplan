<ul class="b-typecat-list<?=$moduleclass_sfx?>">
	<?php foreach ($rows as $row): ?>
	<li class="b-item">
		<div class="b-pic"><a href=""><img src="<?=$row->image?>" width="161" height="129" alt="<?=$row->title?>"></a></div>
		<div class="b-head">
			<div class="b-head-inner">
				<a href="<?=$row->url?>"><?=$row->title?></a>
			</div>
		</div>
		<? // перебираем варианты площадей
		if ($row->selector && ($a = explode(';', $row->selector)) && count($a) && strlen(trim($a[0]))):
			?>
            <div class="b-selsq-ss">
                <div class="b-label">Выбрать площадь</div>
                <div class="b-pop-list">
                    <ul class="b-list">
	                    <? foreach ($a as $sq){
		                    $_val = explode('=', trim($sq));
		                    if (count($_val) > 1 && strlen(trim($_val[0]))){
			                    $_sub = explode('-', $_val[1]);
			                    printf('<li><a href="%s">%s</a></li>',
				                    JRoute::_($row->url.'?from='.$_sub[0].'&till='.$_sub[1]),
				                    $_val[0]);
		                    }
	                    }?>
                    </ul>
                </div>
            </div>
		<? endif ?>

	</li>
	<?php endforeach; ?>
</ul>
